#!/bin/bash

#  This script creates the binding site library.
#
# START WITH
#
#         run.sh ... create a new version of probisdb or update
#
# FOR CLUSTER MODE YOU MUST INCREASE THE FOLLOWING LIMITS ON THE MAIN NODE:
#
#   Settings in /etc/security/limits.conf
#	probis           soft    nproc           1000000
#	probis           hard    nproc           1000000
#	probis           soft    nofile          65535
#	probis           hard    nofile          65535
#
#   Settings in /etc/systemd/user.conf
#	DefaultLimitNOFILE=65535
#	DefaultLimitNPROC=1000000
#
#   Settings in /etc/systemd/system.conf
#	DefaultLimitNOFILE=65535:524288
#	DefaultLimitNPROC=1000000
#
source src/defs.sh
source src/func.sh

# in case of an update, save old data directory
[[ -d "${DATA}" ]] && {
    confirm "Are you sure you want to update data in ${DATA}?" || exit
}

mkdir -p $TMP

echo "Starting Binding Site Library Creation (seq_id = $SEQID)"

# echo "Update all databases from www"
# 01_update_databases.sh

echo "Get fasta sequences of downloaded PDB and AlphaFold proteins"
02_convert_to_fasta.sh

# echo "Prepare biological assemblies"
# 03_prepare_biounits.sh

# echo "Convert entity_id based clusters into chain_id based ones"
# 04_convert_clusters.sh

# echo "Pick a representative protein for each sequence cluster from PDB"
# 05_getclust.sh >$TMP/clusters$SEQID.txt

# echo "Annotate PDB files: Human, Cancer, Kinases ..."
# 10_annotate_structures.sh

# echo "Get binding site chain id(s) of specific ligands"
# 20_neighb.sh

# echo "Make protein surfaces"
# 30_make_protein_surfaces.sh list_pdb_surface $TMP/clusters$SEQID.txt
# 30_make_protein_surfaces.sh list_alphafold_surface

# echo "Remove PDBDIR protein chains that produce defect SRF file"
# 40_remove_calpha.sh $TMP/clusters$SEQID.txt $TMP/clusters_pdb_$SEQID.txt $TMP/clusters_bio_$SEQID.txt

# echo "Prepare bio files"
# 80_bio.sh $TMP/clusters_bio_$SEQID.txt

# echo "For PDB, take only representative proteins"
# 85_representative_srfs.sh $TMP/clusters_bio_$SEQID.txt

# echo "Generate bslib files (for ProBiS -surfdb) and Json metadata"
# 90_bslib.sh 

# echo "Compare PDB protein representatives ($SEQID%) against bslib using ProBiS algorithm"
# 100_run_probis.sh list_representative_pdb_proteins $TMP/clusters_pdb_$SEQID.txt
# 100_run_probis.sh list_member_pdb_proteins $TMP/clusters_pdb_$SEQID.txt

# # echo "Compare AlphaFold models against bslib using ProBiS algorithm"
# 100_run_probis.sh list_alphafold_human_proteins

# echo "Predict binding sites: grids, centroids, ligands... for all PDB proteins"
# 120_predict_bsites.sh

# echo "Generate new ligands with binding affinity and sequence identity information"
# 130_add_affinity_seqid.sh

# echo "Generate docking ready binding sites across whole PDB or AlphaFold DB"
# 140_probisdockdb.sh

# echo "Annotate binding sites (ProBiS-ligands + ProBIS-Dock DB)..."
# 150_annotate_bsites.sh

# echo "Map sequence variants to PDB (or AlphaFold) structures"
# 160_genprobis.sh

# echo "Generate Structural Protein Interaction Network (SPIN) dataset"
# 170_spindb.sh --genprobis_json $JSON2  --probisdb_ligands $LIGS --install_dependencies yes --probisdb_bresi $BRESI --reduce_ligands yes

# echo "Generate SPIN homology networks annotated datasets"
# 180_homonets.sh

echo "Done."
