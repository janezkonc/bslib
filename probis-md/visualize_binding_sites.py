from pymol import cmd
import json
import os
import glob
import gzip

# number of snapshots to analyze
N = 1001

# specify the directory path and what files to read
dir_path = 'results'

# loop through each snapshot directory in the results directory
subdirs = os.listdir(dir_path)
subdirs_sorted = sorted(subdirs,  key=lambda x: int(os.path.basename(x)[3:]))
# print(subdirs_sorted)

idx = 1
for subdir in subdirs_sorted:
    if os.path.isdir(os.path.join(dir_path, subdir)):
        try:
            # read snapshot of a protein
            snapshot_file = glob.glob(
                os.path.join(dir_path, subdir, 'xx*.pdb'))[0]
            print("snapshot", snapshot_file)
            cmd.load(snapshot_file, object="protein", state=idx)

            # read in JSON file with coordinates and radii
            full_dir_path = os.path.join(dir_path, subdir, "probisligandsdb")
            file_path = glob.glob(os.path.join(full_dir_path, 'centro*'))[0]
            print(file_path)
            bsite = []
            with gzip.open(file_path, 'rt') as f:
                obj = json.load(f)
                # this should select the first binding site (bs_id = 1), but a more elaborate
                # selection might be needed for other proteins with e.g. multiple binding sites
                # print(obj[0]["data"])
                bsite = obj[0]["data"]
            # loop through coordinates and radiuses for each binding site
            for xyz, radius in zip(bsite[0], bsite[1]):
                # print("coordinate=", xyz, " radius=", radius)
                cmd.pseudoatom("bsite", pos=xyz, vdw=radius, state=idx)
        except:
            pass
    idx += 1
    if idx > N:
        break

# adjust visualization settings
cmd.hide("everything", "bsite")
cmd.show("mesh", "bsite")
# cmd.set("sphere_scale", 1.0, "bsite")
# cmd.set("sphere_transparency", 0.5, "bsite")
# cmd.set("sphere_color", "white", "bsite")

cmd.hide("everything", "protein")
cmd.show("ribbon", "protein")
# cmd.color("red", "elem C", "protein")
cmd.set("ribbon_color", "blue", "protein")

# highlight residues from Fig. 3 in 3kq0 paper (https://www.sciencedirect.com/science/article/pii/S0022283608011546?via%3Dihub)
cmd.create("gate", "resi 64+68+90")
cmd.show("licorice", "gate")

cmd.zoom("protein")
