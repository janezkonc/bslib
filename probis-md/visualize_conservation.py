from pymol import cmd
import json
import os
import glob
import gzip

# on frame 36, Trp25 lights up (from the paper on 3kq0: "residue Trp25 (which is conserved throughout the lipocalin family)")
# as well as Glu64, which is also important

# number of snapshots to analyze
N = 1001

# specify the directory path and what files to read
dir_path = 'results'

# loop through each snapshot directory in the results directory
subdirs = os.listdir(dir_path)
subdirs_sorted = sorted(subdirs,  key=lambda x: int(os.path.basename(x)[3:]))
# print(subdirs_sorted)
idx = 1
for subdir in subdirs_sorted:
    if os.path.isdir(os.path.join(dir_path, subdir)):
        try:
            # read a snapshot protein file
            snapshot_file = glob.glob(
                os.path.join(dir_path, subdir, 'xx*.pdb'))[0]
            print("snapshot", snapshot_file)
            cmd.load(snapshot_file, object="tmp", state=1)
            full_dir_path = os.path.join(dir_path, subdir, "probisligandsdb")
            # read in JSON file with evolutionary conservation
            file_path = glob.glob(os.path.join(full_dir_path, 'evo*'))[0]
            print(file_path)
            with gzip.open(file_path, 'rt') as f:
                conservation_list = json.load(f)
                # Iterate over all residues in the protein structure
                print("idx", idx)
                for val in conservation_list:
                    # set the color of the residue based on the color value
                    cmd.alter(
                        f"tmp and resi {val['resi']}", f"b={val['cons']}")
                cmd.alter("tmp", f"resi=str(int(resi) + 200 * {idx - 1})")
                cmd.create("protein", "tmp", 1, -1)
        except:
            pass
        cmd.delete("tmp")
    idx += 1
    if idx > N:
        break

# display the protein structure
# cmd.show("cartoon")
cmd.show("spheres")
# in newer input files "HSD" is changed to "HIS" (see fix_to_conform... script)
cmd.hide("everything", "resn HSD")
# show conservation as cyan (not conserved) to red (most conserved)
cmd.spectrum("b", selection="protein", palette="cyan_white_red",
             minimum=0, maximum=9)
