#!/bin/bash -x

# command logging is turned on through -x flag

export JOB_ID=$1
export JOB_NAME=$2
export SNAPSHOT=$3
export CHAIN_ID=$4
export BSLIB_FILE=$5

export RESULTS_DIR=results/$JOB_ID

# the results for a snapshot will be here
mkdir -p $RESULTS_DIR

{
  export UPLOADS_DIR=inputs
  export BIN_DIR=../bin
  export BSLIB_DIR=../data/bslib
  export LIGDIR=../data/lig
  export PROBISEXE=$RESULTS_DIR/probis.$JOB_ID
  export PROBISLIGANDSEXE=$RESULTS_DIR/probisligands.$JOB_ID
  export JOBS=32
  export PATH=$PATH:.
  export BSLIB_SRCDIR=../src
  export SEQDIR=../data/sequences
  export ANNODIR=../data/annotations

  # make a backup copy of parameters
  echo "$JOB_ID $JOB_NAME $SNAPSHOT $CHAIN_ID $BSLIB_FILE" | gzip -c >$RESULTS_DIR/params.txt.gz

  # intialize variables
  pdb_file1=$RESULTS_DIR/$(basename $SNAPSHOT)
  chain_id1=$CHAIN_ID
  id1=$(basename $pdb_file1 .pdb)$chain_id1
  export bslib_file=$BSLIB_DIR/$BSLIB_FILE

  export PROBISDB=$RESULTS_DIR/probisdb
  mkdir -p $PROBISDB
  export TMP=$RESULTS_DIR

  export PROBISLIGANDSDB=$RESULTS_DIR/probisligandsdb
  mkdir -p $PROBISLIGANDSDB

  nosql_file=$PROBISDB/$id1.nosql
  json_file=$PROBISDB/align_$id1.json.gz

  # we expect that 'bslib' project is installed
  source $BSLIB_SRCDIR/func.sh

  # add JOB_ID to executable filenames
  cp $BIN_DIR/probis $PROBISEXE
  cp $BIN_DIR/probisligands $PROBISLIGANDSEXE

  # copy uploaded PDB files into results directory
  cp $UPLOADS_DIR/$(basename $pdb_file1) $RESULTS_DIR

  # run progress monitor
  progress $nosql_file $(wc -l $bslib_file | cut -f1 -d" ") &
  MYSELF=$!

  # run ProBiS algorithm to calculate similar proteins
  probisit $pdb_file1 $chain_id1 $bslib_file $id1 $JOBS $motif_sele

  kill $MYSELF >/dev/null 2>&1

  cp $PROBISDB/$id1.json.gz $json_file

  # run ProBiS-ligands to calculate ligands, binding sites...
  probisligandsit $pdb_file1 $chain_id1 $json_file $id1

} 2>&1 | tee $RESULTS_DIR/log.txt
