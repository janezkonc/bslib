#!/bin/bash

for f in $(find inputs/*.pdb); do
    # insert Chain ID(s) so that each restarting of residue numbers gives another letter A, B, C, ...
    # we assume that a protein is the first in file so it is given chain A
    tmp_file=$(mktemp)
    python3 insert_chain_id_smart.py <$f >$tmp_file && mv $tmp_file $f
done
