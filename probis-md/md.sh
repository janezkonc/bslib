#!/bin/bash

{

    for f in $(find inputs/*.pdb); do
        i=$(basename $f .pdb)
        i=${i#xx}
        ./run_probis.sh job$i $i $f A bslib.txt
    done

} 2>&1 | tee log.txt
