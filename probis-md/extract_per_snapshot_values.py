import json
import os
import glob
import gzip
from statistics import mean

# number of snapshots to analyze
N = 1001

# specify the directory path and what files to read
dir_path = 'results'

# loop through each snapshot directory in the results directory
subdirs = os.listdir(dir_path)
subdirs_sorted = sorted(subdirs,  key=lambda x: int(os.path.basename(x)[3:]))
# print(subdirs_sorted)

similar_proteins_list = []
residues_list = []
ligands_list = []
centroids_list = []
idx = 1
for subdir in subdirs_sorted:
    if not os.path.isdir(os.path.join(dir_path, subdir)):
        continue
    try:
        # read in JSON file with evolutionary conservation
        probis_dir_path = os.path.join(dir_path, subdir, "probisdb")
        probisligands_dir_path = os.path.join(
            dir_path, subdir, "probisligandsdb")

        # specify various JSON files to read
        align_file_path = glob.glob(os.path.join(probis_dir_path, 'align*'))[0]
        bresi_file_path = glob.glob(os.path.join(
            probisligands_dir_path, 'bresi*'))[0]
        centro_file_path = glob.glob(os.path.join(
            probisligands_dir_path, 'centro*'))[0]
        ligands_file_path = glob.glob(os.path.join(
            probisligands_dir_path, 'ligands*'))[0]

        similar_proteins_list.append(
            json.load(gzip.open(align_file_path, 'rt')))
        residues_list.append(
            json.load(gzip.open(bresi_file_path, 'rt')))
        ligands_list.append(
            json.load(gzip.open(ligands_file_path, 'rt')))
        centroids_list.append(
            json.load(gzip.open(centro_file_path, 'rt')))

    except:
        pass
    idx += 1
    if idx > N:
        break

# loop over the data for all snapshots and output the summary of it as a matrix
idx = 1
threshold_z_score = 2.0

# most relevant binding site: highest ranked small molecule binding site


def most_relevant(b): return b["bsite_rest"] == "small" and b["bs_id"] == 1


for similars_for_snapshot, residues_for_snapshot, ligands_for_snapshot, centroids_for_snapshot in zip(similar_proteins_list, residues_list, ligands_list, centroids_list):
    # number of similar proteins
    count_z_score = len(
        [elem for elem in similars_for_snapshot if elem["alignment"][0]["scores"]["z_score"] > threshold_z_score])
    # average Z-score of similar proteins
    mean_z_score = mean(elem["alignment"][0]["scores"]["z_score"]
                        for elem in similars_for_snapshot if elem["alignment"][0]["scores"]["z_score"] > threshold_z_score)
    # number of binding site residues for the most relevant binding site
    count_residues = len([residue for residues in residues_for_snapshot if most_relevant(
        residues) for residue in residues["data"]])
    # list of binding site residue ids/names
    resi_list = [residue["resi"] for residues in residues_for_snapshot if most_relevant(
        residues) for residue in residues["data"]]
    resi_list_comma = ",".join(str(resi) for resi in resi_list)
    # total number of binding sites predicted (let's see if something else gets predicted, e.g., ion, otherwise sanity check - should be two)
    tot_bsites = len([residues for residues in residues_for_snapshot])
    # number of predicted ligands for the most relevant binding site
    count_ligands = len([ligand for ligands in ligands_for_snapshot if most_relevant(
        ligands) for ligand in ligands["data"]])
    # list of ligand names
    ligand_resn_list = [ligand["comments"]["resn"] for ligands in ligands_for_snapshot if most_relevant(
        ligands) for ligand in ligands["data"]]
    ligand_resn_list_comma = ",".join(str(resn) for resn in ligand_resn_list)
    # number of centroids (rough estimate for size of binding site) for the most relevant binding site
    count_centroids = len([centroid for centroids in centroids_for_snapshot if most_relevant(
        centroids) for centroid in centroids["data"][0]])

    # output matrix
    print(f"{idx} {count_z_score} {mean_z_score:.2f} {count_residues} {tot_bsites} {count_ligands} {count_centroids} {resi_list_comma} {ligand_resn_list_comma}")
    idx += 1
