import sys

prev_number = -999999
counts = 0

for line in sys.stdin:
    line = line.rstrip('\n')

    if line.startswith(('ATOM', 'HETATM')):
        # Extract the number from positions 23-26
        number = line[22:26]

        # Update the count for the number
        if int(number) < prev_number:
            counts += 1
        prev_number = int(number)

        # Determine the letter to insert based on the count
        letters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
                   'K', 'L', 'M', 'N', 'O', 'P', 'R', 'S', 'T', 'U',
                   'V', 'W', 'X', 'Y', 'Z']
        letter = letters[counts] if counts < len(letters) else '?'

        # Insert the letter at position 21 in the line
        line = line[:21] + letter + line[22:]

    # Print the (un)modified line
    print(line)
