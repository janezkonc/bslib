#!/bin/bash

for f in $(find inputs/*.pdb); do
    # rename CD -> CD1 for ILE
    # remove OT1, OT2 (applies to last GLU)
    # rename HSD -> HIS
    # add Chain ID (A) - this is required by ProBiS algorithm!
    sed -ri -e 's/ATOM(.{9})CD  ILE/ATOM\1CD1 ILE/' -e '/OT1|OT2/d' -e 's/HSD/HIS/' -e 's/(^.{21}).(.*$)/\1A\2/' $f
done
