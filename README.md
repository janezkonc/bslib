# This is Bslib, a set of tools that generate all ProBiS Databases

**The scripts within this repository update:**
* ProBiS server and ProBiS-ligands 
* GenProBiS
* ProBiS-Dock Database

**Usage:**
Start by looking at run.sh.

Depends on the following C++ programs (included binaries): 
* probis 
* probisligands
* probisdockdatabase
* step20
* moltype
* pick_representative
* convert_format
* gsup
* genexdb
* genmapping

