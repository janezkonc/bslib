#!/bin/bash

CLUS_FILE=$1

confirm "Are you sure you want to delete $SRFDIR?" && rm -Rf $SRFDIR
mkdir -p $SRFDIR
rm -f $TMP/dbg30.txt

# create transient cluster (share base directory to all nodes -- sshfs must be installed on all nodes)
clustermode.bash ${BASE} ${WORKNODE_LIST} up
trap disable_cluster INT

confirm "Do you want to continue to run step 30 on the transient (sshfs) cluster?" || disable_cluster

$@ | ${BASE}/bin/parallel/parallel --progress --colsep " " --env PATH --env TMP \
    --timeout 7200 --joblog $TMP/par30.log --retries 3 --sshloginfile $WORKNODE_LIST "30_parallel.sh {}"

# umount transient cluster
confirm "Parallel jobs have finished. Do you wish to disable cluster?" && disable_cluster
