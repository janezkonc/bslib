#!/bin/bash

{
    id=$(basename $1 .json.gz)
    id=${id#aff_ligands_}
    len=${#id}
    (($len == 5)) && {
        # PDB structure
        pdb_id=${id:0:4}
        receptor_file=$PDBDIR/all/pdb/pdb$pdb_id.ent.gz
    } || {
        # AlphaFold structure
        receptor_file=$ALPHAFOLD/$id.pdb.gz
    }

    # prepare ProBiS-Dock Database files (binding sites ready for docking)
    probisdockdatabaseit $receptor_file $id

} &>>$TMP/dbg140.txt
