from sys import argv
import os
import gzip

CLUSTER_FILE = argv[1]
PDBDIR = os.path.join(argv[2], "all/pdb")


pdb_to_entity_to_chain = {}


def get_entity_to_chain_mapping(pdb_dir, pdb_id):
    if pdb_id in pdb_to_entity_to_chain:
        return pdb_to_entity_to_chain[pdb_id]
    with gzip.open(os.path.join(pdb_dir, 'pdb' + pdb_id + '.ent.gz'), 'rt') as f:
        entity_to_chain = {}
        entity_id = 0
        chain_ids = []
        visited = False
        for line in f:
            #line = line.decode()
            if not line.startswith('COMPND'):
                if visited:
                    break
                continue
            visited = True
            shorter = line[10:].lstrip()
            line = ''.join(line.strip().split())  # remove endline, spaces...
            if shorter.startswith('MOL_ID:'):
                entity_id = line.split(':')[1].strip(';')
                entity_to_chain[entity_id] = []
            elif shorter.startswith('CHAIN:'):
                # print(line)
                chain_ids = line.split(':')[1].strip(';').split(',')
                # print(chain_ids)
                # print("before", entity_to_chain[entity_id])
                entity_to_chain[entity_id].extend(chain_ids)
                # print("after", entity_to_chain[entity_id])
        pdb_to_entity_to_chain[pdb_id] = entity_to_chain
    return pdb_to_entity_to_chain[pdb_id]


with open(CLUSTER_FILE, 'rt') as f:
    for line in f:
        line_with_chain_ids = []
        for pdb_item in line.strip().split(' '):
            try:
                # print(pdb_item)
                pdb_id, entity_id = pdb_item.split('_')
                entity_to_chain = get_entity_to_chain_mapping(
                    PDBDIR, pdb_id.lower())
                # print(entity_to_chain)
                for chain_id in entity_to_chain[entity_id]:
                    line_with_chain_ids.append(pdb_id + "_" + chain_id)
            except:
                pass
        if not line_with_chain_ids:
            # print(line)
            continue
        result = (' '.join(line_with_chain_ids)).rstrip()
        print(result)
