#!/bin/bash

confirm "Are you sure you want to delete $BSLIBDIR?" && rm -Rf $BSLIBDIR
mkdir -p $BSLIBDIR

# generate bslib file for PDB
find $SRFDIR/pdb/rep -type f | xargs realpath --relative-to=$BASE | sed -r "s,(.*)(.{4})(.{1})\.srf$,\1\2\3.srf \3," >$BSLIBDIR/bslib.txt

# generate bslib files for AlphaFold
for species_dir in $(find $SRFDIR/alphafold -mindepth 1 -maxdepth 1 -type d | xargs realpath --relative-to=$BASE); do
    species=$(basename $species_dir)
    find $species_dir -type f | sed -r 's/(.*)/\1 A/' >$BSLIBDIR/bslib_$species.txt
done
