#!/bin/bash

cnt=$1
line=$2

line=$(echo "$line" | tr ' ' '\n' | egrep "^.{4}_.$" | tr '\n' ' ' | tr '_' ':') # remove multi-character chain_ids eg. HHH
protein_files=$(echo $line | tr '[:upper:]' '[:lower:]' | sed -r "s,(\S+):\S+,$PDBDIR/all/pdb/pdb\1.ent.gz,g")
chain_ids=$(echo $line | sed -r "s,\S+:(\S+),\1,g")
rep=$(pick_representative --protein_files ${protein_files} --chain_ids ${chain_ids} 2>>$TMP/dbg05.txt)
echo $cnt $rep >>$TMP/dbg05.txt
[ -z "$rep" ] && exit 1
rep=$(echo "$rep" | cut -f1,2 -d" " | sed -r "s/.*pdb(.{4}).ent.gz (.)/\U\1\E:\2/")
echo -e "$cnt\t1\t${rep}"                                                                   # print representative
echo "$line" | tr ' ' '\n' | sed '/^$/d' | grep -v ${rep} | nl -v2 -nln | sed "s/^/$cnt\t/" # print members
