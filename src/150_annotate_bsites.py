import os
from sys import argv
import gzip
import json
import pyfunc

# this script annotates pdb codes with taxonomical groups, cancer-relatedness, kinase activity...

PROBISLIGANDSDB = argv[1]
PROBISDOCKDB = argv[2]  # the resulting annotation file will be here
ANNODIR = argv[3]

# convert to json to get double quotes
extended = pyfunc.extend_annotations(PROBISLIGANDSDB, PROBISDOCKDB, ANNODIR)

# output annotated binding sites
with gzip.open(os.path.join(PROBISDOCKDB, 'probisdockdb_annotated.json.gz'), 'w') as fout:
    fout.write(json.dumps(extended).encode('utf-8'))
