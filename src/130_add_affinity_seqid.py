import pyfunc
from sys import argv
import glob
import os

DOWNLOAD = argv[1]
PROBISLIGANDSDB = argv[2]
SEQDIR = argv[3]

pdb_bind_data = pyfunc.get_pdb_bind_data(DOWNLOAD)  # globalno
bindingDB_data = pyfunc.get_bindingDB_data(DOWNLOAD)  # globalno
protein_data = pyfunc.get_pdb_bind_PROTEIN_data(DOWNLOAD)  # globalno


def wrap_add_to_ligand(ligands_file):
    pyfunc.add_to_ligand(ligands_file, pdb_bind_data,
                         bindingDB_data, protein_data, PROBISLIGANDSDB, SEQDIR, SEQDIR)


if __name__ == '__main__':
    pyfunc.go_over(glob.glob(os.path.join(PROBISLIGANDSDB,
                   "ligands_*.json.gz")), wrap_add_to_ligand)
