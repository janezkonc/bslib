from sys import argv
import pyfunc

# this script annotates pdb codes with taxonomical groups, cancer-relatedness, kinase activity...

PDBDIR = argv[1]
ALPHAFOLD = argv[2]
DOWNLOAD = argv[3]
ANNODIR = argv[4]

annotated = pyfunc.annotate_pdbs(DOWNLOAD, PDBDIR, ALPHAFOLD, ANNODIR)
