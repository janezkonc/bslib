#!/bin/bash

python3 src/04_convert_clusters.py $DOWNLOAD/clusters-by-entity-$SEQID.txt $PDBDIR |
    awk '{ print length, $0 }' |
    sort -r -n -s |
    cut -d" " -f2- >$TMP/bc-$SEQID.out
