#!/bin/bash

{
    pdb_file=$1
    chain_id=$2
    srf_file=$3
    compress_srf=${4:-""}

    echo "cmd = probis -extract -f1 $pdb_file -c1 $chain_id -srffile $srf_file"

    # probis creates surface ONLY from the first model..
    probis -extract -f1 $pdb_file -c1 $chain_id -srffile $srf_file

    # gzip the surface file
    [[ $compress_srf == "compress_srf" ]] && gzip $srf_file

    rm -f $pdb_file

} &>>$TMP/dbg30.txt
