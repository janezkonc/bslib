#!/bin/bash

{

	pdb_file=$1
	pdb_id=${pdb_file:0:4}

	step20 --bio_file $BIODIR/${pdb_file} --pdb_id ${pdb_id} --dist 5.0 --ligands_file $LIGDIR/pdb/${pdb_id}.json.gz

} &>>$TMP/dbg20.txt
