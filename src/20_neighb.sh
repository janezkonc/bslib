#!/bin/bash

# cleanup and prepare new dirs
rm -f $TMP/dbg20.txt
confirm "Are you sure you want to delete $LIGDIR/pdb?" && rm -Rf $LIGDIR/pdb
mkdir -p $LIGDIR/pdb

# create transient cluster (share base directory to all nodes -- sshfs must be installed on all nodes)
clustermode.bash ${BASE} ${WORKNODE_LIST} up
trap disable_cluster INT

confirm "Do you want to continue to run step 20 on the transient (sshfs) cluster?" || disable_cluster

ls $BIODIR | ${BASE}/bin/parallel/parallel --progress --env PATH --env TMP --env BIODIR --env LIGDIR --env BASE \
	--timeout 7200 --retries 3 --memfree 1G --joblog $TMP/par20.log --sshloginfile $WORKNODE_LIST "20_parallel.sh {}"

# umount transient cluster
confirm "Parallel jobs have finished. Do you wish to disable cluster?" && disable_cluster
