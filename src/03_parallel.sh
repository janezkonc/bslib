#!/bin/bash

{

    pdb_file=$1
    pdb_id=${pdb_file:3:4}

    # compute molecule types, i.e., glycan, cofactor etc. for pdb file
    moltype --hydrogens --bio_ass first --models first -i $PDBDIR/all/pdb/$pdb_file -o $BIODIR/${pdb_id}.json.gz

} &>>$TMP/dbg03.txt
