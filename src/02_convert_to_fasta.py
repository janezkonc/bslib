from sys import argv
import glob
import os
import pyfunc

STRUCDIR = argv[1]
SEQDIR = argv[2]


def wrap_convert_to_fasta(pdb_file):
    pyfunc.convert_to_fasta(pdb_file, SEQDIR)


if __name__ == '__main__':
    pyfunc.go_over(glob.glob(os.path.join(STRUCDIR, "*.ent.gz")) +
            glob.glob(os.path.join(STRUCDIR, "**", "*.pdb.gz")), wrap_convert_to_fasta)
