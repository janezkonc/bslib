#!/bin/bash

YESALL=y # set this to y if you wish to disable asking for every download

mkdir -p ${DOWNLOAD}
echo $(timestamp) >$DOWNLOAD/creation_date.txt

confirm "Update PDB?" ${YESALL} && {
    mkdir -p $PDBDIR/all/pdb
    mkdir -p $PDBDIR/divided/pdb
    retry "rsync -q -rlpt -z --delete --port=33444 rsync.rcsb.org::ftp_data/structures/all/pdb/ $PDBDIR/all/pdb" &&
        echo "Successfully downloaded PDB (all directory)" || {
        echo "[WHOOPS] PDB failed to download"
        exit 1
    }
    retry "rsync -q -rlpt -z --delete --port=33444 rsync.rcsb.org::ftp_data/structures/divided/pdb/ $PDBDIR/divided/pdb" &&
        echo "Successfully downloaded PDB (divided directory)" || {
        echo "[WHOOPS] PDB failed to download"
        exit 1
    }
}

confirm "Update sequence clusters?" ${YESALL} && {
    download $DOWNLOAD/clusters-by-entity-30.txt https://cdn.rcsb.org/resources/sequence/clusters/clusters-by-entity-30.txt
    download $DOWNLOAD/clusters-by-entity-40.txt https://cdn.rcsb.org/resources/sequence/clusters/clusters-by-entity-40.txt
    download $DOWNLOAD/clusters-by-entity-50.txt https://cdn.rcsb.org/resources/sequence/clusters/clusters-by-entity-50.txt
    download $DOWNLOAD/clusters-by-entity-70.txt https://cdn.rcsb.org/resources/sequence/clusters/clusters-by-entity-70.txt
    download $DOWNLOAD/clusters-by-entity-90.txt https://cdn.rcsb.org/resources/sequence/clusters/clusters-by-entity-90.txt
    download $DOWNLOAD/clusters-by-entity-95.txt https://cdn.rcsb.org/resources/sequence/clusters/clusters-by-entity-95.txt
    download $DOWNLOAD/clusters-by-entity-100.txt https://cdn.rcsb.org/resources/sequence/clusters/clusters-by-entity-100.txt
}

confirm "Update PDBbind?" ${YESALL} && {
    download $DOWNLOAD/PDBbind.tar.gz http://www.pdbbind.org.cn/download/PDBbind_v2020_plain_text_index.tar.gz
}

confirm "Update BindingDB?" ${YESALL} && {
    download $DOWNLOAD/BindingDB.tsv.zip https://www.bindingdb.org/bind/downloads/BindingDB_All_202309_tsv.zip
}

confirm "Update Taxonomy from NCBI?" ${YESALL} && {
    download $DOWNLOAD/taxdump.tar.gz http://ftp.ncbi.nih.gov/pub/taxonomy/taxdump.tar.gz
}

confirm "Update SIFTS?" ${YESALL} && {
    download $DOWNLOAD/pdb_chain_enzyme.tsv.gz http://ftp.ebi.ac.uk/pub/databases/msd/sifts/flatfiles/tsv/pdb_chain_enzyme.tsv.gz
    download $DOWNLOAD/pdb_chain_ensembl.tsv.gz http://ftp.ebi.ac.uk/pub/databases/msd/sifts/flatfiles/tsv/pdb_chain_ensembl.tsv.gz
    download $DOWNLOAD/pdb_chain_taxonomy.tsv.gz http://ftp.ebi.ac.uk/pub/databases/msd/sifts/flatfiles/tsv/pdb_chain_taxonomy.tsv.gz
    download $DOWNLOAD/pdb_chain_uniprot.tsv.gz http://ftp.ebi.ac.uk/pub/databases/msd/sifts/flatfiles/tsv/pdb_chain_uniprot.tsv.gz
}

confirm "Update cancer-related from Protein Atlas?" ${YESALL} && {
    download $DOWNLOAD/cancer_related.tsv.gz "https://www.proteinatlas.org/search/protein_class%3ACOSMIC+somatic+mutations+in+cancer+genes?format=tsv&compress=yes"
}

confirm "Update SIFTS XML (GenProBiS)?" ${YESALL} && {
    retry "rsync -q -rlpt -z --delete ftp.ebi.ac.uk::pub/databases/msd/sifts/split_xml/ $SIFTS" &&
        echo "Successfully downloaded SIFTS" || {
        echo "[WHOOPS] SIFTS failed to download"
        exit 1
    }
}

confirm "Update UniProt variants and extra information (GenProBiS)?" ${YESALL} && {
    mkdir -p $UNIPROT/variants
    download $UNIPROT/variants ftp://ftp.uniprot.org/pub/databases/uniprot/current_release/knowledgebase/variants/ "-q -r -N -nd -P"
}

confirm "Update ClinVar (GenProBiS)?" ${YESALL} && {
    mkdir -p $CLINVAR
    download $CLINVAR/variant_summary.txt.gz ftp://ftp.ncbi.nlm.nih.gov/pub/clinvar/tab_delimited/variant_summary.txt.gz
    download $CLINVAR/submission_summary.txt.gz ftp://ftp.ncbi.nlm.nih.gov/pub/clinvar/tab_delimited/submission_summary.txt.gz
}

confirm "Update PharmGKB (GenProBiS)?" ${YESALL} && {
    mkdir -p $PHARMGKB
    download $PHARMGKB/pharmgkb.zip https://api.pharmgkb.org/v1/download/file/data/rsid.zip
    unzip -o $PHARMGKB/pharmgkb.zip -d $PHARMGKB
}

confirm "Update BLAST database for PDB (GenProBiS)?" ${YESALL} && {
    download $DOWNLOAD/pdbase.fa ftp://ftp.rcsb.org/pub/pdb/derived_data/pdb_seqres.txt
}

confirm "Update AlphaFold Database?" ${YESALL} && {
    mkdir -p $ALPHAFOLD
    download $DOWNLOAD/alphafold_download_metadata.json https://ftp.ebi.ac.uk/pub/databases/alphafold/download_metadata.json
    metadata_j="$(cat $DOWNLOAD/alphafold_download_metadata.json)"
    for item in $(parse_json "archive_name, species" "$metadata_j" | tr " " "_"); do
        archive_name=$(echo $item | cut -f1 -d@)
        species=$(echo $item | cut -f2 -d@)
        [[ $species == "undefined" ]] && continue
        mkdir -p $ALPHAFOLD/$species
        download $DOWNLOAD/$archive_name https://ftp.ebi.ac.uk/pub/databases/alphafold/latest/$archive_name
        tar xf $DOWNLOAD/$archive_name --directory $ALPHAFOLD/$species
        rm -f $DOWNLOAD/$archive_name
    done
}

echo "[NOTE] All downloads finished SUCCESSFULLY!"
