#!/bin/bash

confirm "Do you wish to generate binding site annotations for ProBiS-ligands and ProBiS-Dock DB binding sites?" || exit

python3 src/150_annotate_bsites.py $PROBISLIGANDSDB $PROBISDOCKDB $ANNODIR &>$TMP/dbg150.txt
