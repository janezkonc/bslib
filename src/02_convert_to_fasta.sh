#!/bin/bash

confirm "Are you sure you want to delete $SEQDIR?" && rm -Rf $SEQDIR $TMP/dbg02.txt
mkdir -p $SEQDIR

python3 src/02_convert_to_fasta.py $PDBDIR/all/pdb $SEQDIR &>$TMP/dbg02.txt
python3 src/02_convert_to_fasta.py $ALPHAFOLD $SEQDIR &>>$TMP/dbg02.txt
