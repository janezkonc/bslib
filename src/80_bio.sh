#!/bin/bash

#
# Naredimo .bu.pdb in .lig datoteke!
#
#
#
#

export CLUSTER_FILE=$1

# Obnovimo bazo ligandov - superponiramo vse v vsakem spremenjenem klastru na njihovega predstojnika :)
confirm "Are you sure you want to delete $LIGDIR/{bio,nosql,json}?" &&
	rm -Rf $LIGDIR/{bio,nosql,json}

mkdir -p $LIGDIR/{bio,nosql,json}
rm -f $TMP/dbg80.txt

# go over all representatives in cluster file in a parallel job
sort -nusk1,1 $CLUSTER_FILE | tr -d " " | tr "\t" "_" | ${BASE}/bin/parallel/parallel --env TMP \
	--env PATH --env CLUSTER_FILE --env SRFDIR --env LIGDIR --env BIODIR --env BASE --progress \
	--retries 3 --joblog $TMP/par80_1.log --timeout 7200 "80_parallel_1.sh"

# naredimo biounite in jih prilegamo na ustrezen predstavnikov chain, del biounita
find $LIGDIR/json -type f| grep "\.json.gz$" | ${BASE}/bin/parallel/parallel --env LIGDIR --env TMP \
	--env PATH --progress --retries 3 --joblog $TMP/par80_2.log --timeout 7200 "80_parallel_2.sh"
