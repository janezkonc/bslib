#!/bin/bash

export SEQID=100;
export BASE=$(pwd);
export DATA="$BASE/data";
export TMP="$DATA/tmp";
export DOWNLOAD="$DATA/download";

export PROBISEXE="probis";
export PROBISLIGANDSEXE="probisligands";
export PROBISDOCKDBEXE="probisdockdatabase";

export SEQDIR="$DATA/sequences";
export ANNODIR="$DATA/annotations";

export SRFDIR="$DATA/srf";
export LIGDIR="$DATA/lig";
export BIODIR="$DATA/biounits";
export BSLIBDIR="$DATA/bslib";

export PDBDIR="$DOWNLOAD/pdb";
export ALPHAFOLD="$DOWNLOAD/alphafold";

export SIFTS="$DOWNLOAD/sifts";
export UNIPROT="$DOWNLOAD/uniprot";
export CLINVAR="$DOWNLOAD/clinvar";
export PHARMGKB="$DOWNLOAD/pharmgkb";

export PROBISDB="$DATA/probisdb";
export PROBISLIGANDSDB="$DATA/probisligandsdb";
export PROBISDOCKDB="$DATA/probisdockdb";

export GENEXDB="$DATA/genexdb";
export GENPROBISDB="$DATA/genprobisdb";

export SPINDB="$DATA/spin";
export SPINSRC="$BASE/spin_src";

export WORKNODE_LIST="worknodes.txt";

export PATH="$PATH:$(pwd):$(pwd)/src:$(pwd)/bin:$(pwd)/spin_src";

export NUMPROBIS=$(grep ^processor /proc/cpuinfo|wc -l);
export LC_ALL="en_US.utf-8";
