#!/bin/bash

confirm "Do you wish to generate annotations for the PDB and AlphaFold?" || exit

mkdir -p $ANNODIR

python3 src/10_annotate_structures.py $PDBDIR/all/pdb $ALPHAFOLD $DOWNLOAD $ANNODIR &>$TMP/dbg10.txt
