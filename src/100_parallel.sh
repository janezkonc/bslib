#!/bin/bash

{
    pdb_file=$1
    chain_id=${2:-A} # defaults are for AlphaFold proteins
    bslib_file=${3:-$BSLIBDIR/bslib.txt}

    echo "Running ProBiS for protein $pdb_file chain $chain_id comparing it to proteins in $bslib_file and saving results in $PROBISDB"

    cd $BASE

    id=$(basename $pdb_file .pdb.gz)
    id=$(basename $id .ent.gz)
    id=${id#pdb}
    [[ ${#id} == 4 ]] && id=${id}${chain_id}

    probisit $pdb_file $chain_id $bslib_file $id

} &>>$TMP/dbg100.txt
