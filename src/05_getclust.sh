#!/bin/bash

# Reorganize sequence clusters so, that a representative structure is first (format of old clusters95.txt).
# A representative within each cluster is chosen as the one with highest structural coverage
# and with lowest resolution. There is a tolerance (5%) for coverage, so sometimes a structure
# within this percent is chosen as representative. Also, X-RAY strutures are generally preffered
# over NMR models.

rm -f $TMP/dbg05.txt

# naredimo v format "starega" clusters95.txt
cat $TMP/bc-$SEQID.out | ${BASE}/bin/parallel/parallel --progress --jobs 100% --keep-order \
    --joblog $TMP/par05.log --retries 3 "05_parallel.sh {#} {}"
