#!/bin/bash

# cleanup and prepare new dirs
rm -f $TMP/dbg120.txt
confirm "Are you sure you want to delete $PROBISLIGANDSDB?" && rm -Rf $PROBISLIGANDSDB

mkdir -p $TMP $PROBISLIGANDSDB

# create transient cluster (share base directory to all nodes -- sshfs must be installed on all nodes)
clustermode.bash ${BASE} ${WORKNODE_LIST} up
trap disable_cluster INT

confirm "Do you want to continue to run step 120 on the transient (sshfs) cluster?" || disable_cluster

# prepare a filter file from bslib.txt (same data but different format)
cat $BSLIBDIR/bslib.txt | tr " " "@" | xargs -n1 basename | tr "@" " " | sed s/.srf// >$TMP/filter_probis_json.txt

## remove --resume flag if you wish to start the process from zero
find $PROBISDB -name *.json.gz | ${BASE}/bin/parallel/parallel --env probisligandsit --env LIGDIR \
	--env PROBISLIGANDSEXE --env PROBISLIGANDSDB --env ALPHAFOLD --env PATH --env PDBDIR --env TMP --progress --jobs 100% \
	--delay 0.1 --joblog $TMP/par120.log --retries 3 --memfree 1G --timeout 36000 \
	--sshloginfile $WORKNODE_LIST '120_parallel.sh {}'

# umount transient cluster
confirm "Parallel jobs have finished. Do you wish to disable cluster?" && disable_cluster
