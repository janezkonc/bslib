#!/bin/bash

#
# Expects a function that lists proteins to compare as argument $1. This function can
# also accept its own arguments.
#

# cleanup and prepare new dirs
rm -f $TMP/dbg100.txt
mkdir -p $PROBISDB

# create transient cluster (share base directory to all nodes -- sshfs must be installed on all nodes)
clustermode.bash ${BASE} ${WORKNODE_LIST} up
trap disable_cluster INT

confirm "Do you want to run step 100 on the transient (sshfs) cluster?" || disable_cluster

## add or remove '--resume' option if you wish to restart job or start it from zero
$@ | ${BASE}/bin/parallel/parallel --colsep " " --env PATH --env BASE --env TMP --env BSLIBDIR \
    --env PROBISDB --env PROBISEXE --env probisit --progress --jobs 100% --delay 0.1 --retries 3 \
    --joblog $TMP/par100.log --memfree 1G --sshloginfile $WORKNODE_LIST '100_parallel.sh {}'

# make a backup copy
r=$(basename $(mktemp))
cp $TMP/par100.log $TMP/par100.${r}.BKP

# umount transient cluster
confirm "Parallel jobs have finished. Do you wish to disable cluster?" && disable_cluster
