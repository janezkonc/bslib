#!/bin/bash

rm -f $TMP/dbg160.txt

# run genprobis to create mapping between uniprot, pdb, snp, and interactions
echo "Running GenProBiS ..."
genexdb \
    --submission_summary_file $CLINVAR/submission_summary.txt.gz \
    --variant_summary_file $CLINVAR/variant_summary.txt.gz \
    --pharmgkb_file $PHARMGKB/rsid.tsv \
    --uniprot_file $UNIPROT/extra/uniprot.tab \
    --uniprot_variants_dir $UNIPROT/variants \
    --clinvar_json_file $GENEXDB/clinvar.json.gz \
    --pharmgkb_json_file $GENEXDB/pharmgkb.json.gz \
    --uniprot_json_file $GENEXDB/uniprot.json.gz \
    --out_dir $GENEXDB/uniprot_variants &>$TMP/dbg160.txt

find $ALPHAFOLD $PDBDIR/divided/pdb -type f | egrep "\-F1\-model_v2.pdb.gz|*.ent.gz" |
    ${BASE}/bin/parallel/parallel --progress --load 100% --joblog $TMP/par160.log \
        --retries 3 --memfree 1G --timeout 36000 "160_parallel.sh {}"

##### below for testing
# find $PDBDIR/divided/pdb -type f | egrep "\-F1\-model_v2.pdb.gz|*.ent.gz" |
#     xargs -n1 "160_parallel.sh"
