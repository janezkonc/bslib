#!/bin/bash

{

    line=$1

    pdb_id=$(echo "$line" | cut -f3 -d"_" | cut -f1 -d":" | tr "[:upper:]" "[:lower:]")
    chain_id=$(echo "$line" | cut -f3 -d"_" | cut -f2 -d":")

    pdbchain=${pdb_id}${chain_id}

    # remove defect chains from all PDB chains (requires extracting the srf)
    gunzip -c $PDBDIR/all/pdb/pdb$pdb_id.ent.gz >$TMP/$pdbchain.pdb
    probis -extract -f1 $TMP/$pdbchain.pdb -c1 $chain_id -srffile $TMP/$pdbchain.srf

    if [[ $(grep -m1 '^P>' $TMP/$pdbchain.srf 2>/dev/null) != "" ]]; then
        echo "$(echo $line | tr '_' '\t')"
    fi >>$CLUS_FILE_MOD_PDB

    rm -f $TMP/$pdbchain.{pdb,srf}

    # remove defect chains from all biounit SRFs
    if [[ $(zgrep -m1 '^P>' $SRFDIR/pdb/all/$pdbchain.srf.gz 2>/dev/null) != "" ]]; then
        echo "$(echo $line | tr '_' '\t')"
    fi >>$CLUS_FILE_MOD_BIO

} &>>$TMP/dbg40.txt
