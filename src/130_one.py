import pyfunc
from sys import argv
from collections import defaultdict

DOWNLOAD = argv[1]
PROBISLIGANDSDB = argv[2]
ligands_file = argv[3]
SEQDIR_REP= argv[4]
SEQDIR_LIG = argv[5]

pdb_bind_data = pyfunc.get_pdb_bind_data(DOWNLOAD)  # globalno
protein_data = pyfunc.get_pdb_bind_PROTEIN_data(DOWNLOAD)  # globalno

if __name__ == '__main__':
    pyfunc.add_to_ligand(ligands_file, pdb_bind_data,
                         defaultdict(lambda: defaultdict(float)), protein_data, 
                         PROBISLIGANDSDB, SEQDIR_REP, SEQDIR_LIG)
