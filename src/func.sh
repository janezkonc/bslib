#!/bin/bash

##
# Parses a Json argument that can be an object or array and prints requested properties. If Json is array,
# then each element of this array is printed on its own line.
#
# Example1: parse_json "prop1, prop2" '{"prop1":"a b", "prop2": "c"}' "@"
# Output1:
# a b@c
#
# Example2: parse_json "prop1, prop2" '[{"prop1":"a b", "prop2": "c"}, {"prop1":"d e", "prop2": "f g"}]' "@"
# Output2:
# a b@c
# d e@f g
#
function parse_json() {
    set +x # turn off logging
    props=$1
    json=$2
    delimiter=${3:-@}                                 # default delimiter
    props2=$(echo $1 | sed -r "s/,/,'$delimiter',/g") # add a delimiter
    echo -e "$(node -e "const p = JSON.parse(process.argv[1]); const o = Array.isArray(p) ? p : [p]; for(const {$props} of o) console.log($props2);" "$json" | sed -r s/" "$delimiter" "/$delimiter/g)"
}


##
# Returns the progress of job in percentage.
#
function progress() {
  set +x # turn off logging
  while true; do
    job_done=$(wc -l $1 | cut -f1 -d" ")
    all_done=$2
    echo PROGRESS\($(echo "100 * $job_done / $all_done" | bc)\)
    sleep 5
  done
}

function retry {
    local cmd=$1
    local n=${2:-10} # retry 10-times by default
    cnt=1
    (echo >&2 "Executing $cmd (maximum $n retries)")
    while [[ $(
        $cmd &>/dev/null
        echo $?
    ) != "0" ]]; do
        cnt=$((cnt + 1))
        if (($cnt > $n)); then
            (echo >&2 "Failed to execute $cmd...")
            return 1
        fi
        (echo >&2 "Retrying to execute $cmd...")
        sleep 1
    done
    (echo >&2 "Successfully executed $cmd")
    return 0
}

function all_worknodes {
    cmd=$1
    prt=$2
    worknodes=$3
    servers=""
    while read line; do
        servers="$servers $(echo "$line" | cut -d "/" -f 2)"
    done <$worknodes

    for server in $servers; do
        realcmd=$(echo "$cmd" | sed "s/SUBST/$server/g")
        echo $server ":" $prt " executing cmd =" $realcmd
        eval $realcmd
    done
}

function confirm() {
    local msg=${1:-Are you sure? [y/N]}
    local yn=${2} # set this to y if you want to confirm without asking
    while true; do
        [[ $yn == "y" ]] || read -p "${msg}" yn
        case $yn in
        [Yy]*)
            echo "Answer is YES" >&2
            return 0
            ;;
        [Nn]*)
            echo "Answer is NO" >&2
            return 1
            ;;
        *)
            echo "Please answer yes or no." >&2
            ;;
        esac
    done
    return 1
}

function timestamp() {
    date +'%m-%d-%Y'
}

function download {
    filename=$1
    url=$2
    opts=${3:--q -O}
    retry "wget ${opts} $filename $url"
    [ -s $filename ] && echo "Success" || {
        echo "[WHOOPS] Some files failed to download..."
        exit 1
    }
}

## run probis
function probisit {
    pdb_file=$1
    chain_id=$2
    bslib_file=$3
    id=$4
    jobs=${5:-1}
    motif_sele=${4:-""}

    echo "probisit: pdb_file=$pdb_file chain_id=$chain_id bslib_file=$bslib_file id=$id tmp=$TMP probisdb=$PROBISDB"

    # compare each protein chain against surfaces of all binding sites
    zcat -f $pdb_file >$TMP/$id.pdb # pdb_file can be .gz or not!
    gunzip ${PROBISDB}/$id.nosql.gz 2>/dev/null

    $PROBISEXE -extract $motif_sele -f1 $TMP/$id.pdb -c1 $chain_id -srffile $TMP/$id.srf
    $PROBISEXE -ncpu $jobs -surfdb -local -longnames -sfile $bslib_file -out $PROBISDB -nosql $id.nosql -f1 $TMP/$id.srf -c1 $chain_id
    $PROBISEXE -nofp -longnames -results -f1 $TMP/$id.pdb -c1 $chain_id -nosql $PROBISDB/$id.nosql -out ${PROBISDB} -json $id.json

    gzip -f ${PROBISDB}/$id.nosql ${PROBISDB}/$id.json
    rm -f ${PROBISDB}/{*.cons.pdb,info.json,query.json} $TMP/$id.pdb $TMP/$id.srf

    # # remove only if it's a member's bslib file (if it's in a $TMP directory)!
    # [[ $($bslib_file) == *"$(basename $TMP)"* ]] && rm -f $bslib_file
}

## run probis-ligands
function probisligandsit {
    receptor_file=$1
    chain_id=$2
    json_file=$3
    id=$4
    filter_file=${5:-""}

    echo "probisligandsit: receptor_file=$receptor_file chain_id=$chain_id json_file=$json_file id=$id filter_file=$filter_file"

    $PROBISLIGANDSEXE \
        --noprobis \
        --receptor_file $receptor_file \
        --receptor_chain_id $chain_id \
        --json_file $json_file \
        --filter_file "$filter_file" \
        --bio_dir $LIGDIR/bio \
        --evo_file $PROBISLIGANDSDB/evo_$id.json.gz \
        --msa_file $PROBISLIGANDSDB/msa_$id.json.gz \
        --inte_file $PROBISLIGANDSDB/inte_$id.json.gz \
        --centro_out_file $PROBISLIGANDSDB/centro_$id.json.gz \
        --lig_clus_file $PROBISLIGANDSDB/ligands_$id.json.gz \
        --bresi_file $PROBISLIGANDSDB/bresi_$id.json.gz
        # --min_z_score 2.0 3.0 3.0 2.0 2.5 2.5 2.5 2.0 2.5
}

## run probis-dock database
function probisdockdatabaseit {
    receptor_file=$1
    id=$2

    echo "probisdockdatabaseit: receptor_file=$receptor_file id=$id"

    $PROBISDOCKDBEXE \
        --receptor_file $receptor_file \
        --lig_clus_file $PROBISLIGANDSDB/aff_ligands_$id.json.gz \
        --bio_dir $LIGDIR/bio \
        --predicted_ligands_file $PROBISDOCKDB/ligands_$id.json.gz \
        --centroids_file $PROBISDOCKDB/centro_$id.json.gz \
        --bresi_file $PROBISDOCKDB/bresi_$id.json.gz \
        --vina_file $PROBISDOCKDB/vina_$id.json.gz \
        --equivalent_file $PROBISDOCKDB/equ_$id.json.gz \
        --accessory_file $PROBISDOCKDB/accessory_$id.json.gz
}

function list_alphafold_human_proteins() {
    find $ALPHAFOLD/Homo_sapiens -name *.pdb.gz
}

# expects cluster file as argument
function list_representative_pdb_proteins() {
    cluster_file=$1
    sort -nusk1,1 $cluster_file | cut -f3 | sed -r "s,(....):(.),\L$PDBDIR/all/pdb/pdb\1.ent.gz\E \2 $BSLIBDIR/bslib.txt,"
}

# expects cluster file as argument
# for each cluster member (non-representative), prepare a special bslib file based on (already calculated) Json
# file of representative protein (a cluster member will be compared only to proteins that are already found to be similar
# to its corresponding representative protein)
function list_member_pdb_proteins() {
    cluster_file=$1
    sort -nusk1,1 $cluster_file >$TMP/representatives.txt
    prev_cluster_id=""
    for line in $(grep -v -f $TMP/representatives.txt $cluster_file | tr -d " " | tr "\t" "@"); do
        cluster_id=$(echo $line | cut -f1 -d@)
        custom_bslib_file=$TMP/bslib_${cluster_id}.txt
        if [[ "$cluster_id" != "$prev_cluster_id" ]]; then
            rep_pdb_chain=$(egrep "^${cluster_id}\s+" $TMP/representatives.txt | cut -f3 | sed -r "s,(....):(.),\L\1\E\2,")
            zegrep -o "pdb_id\":\"[^\"]*\"" $PROBISDB/${rep_pdb_chain}.json.gz | cut -f3 -d"\"" |
                sed -r "s,^(.{4})(.{1})$,$SRFDIR/pdb/rep/\1\2.srf \2," >${custom_bslib_file}
            prev_cluster_id=$cluster_id
        fi
        echo $line | cut -f3 -d@ | sed -r "s,(....):(.),\L$PDBDIR/all/pdb/pdb\1.ent.gz\E \2 ${custom_bslib_file},"
    done
}

function list_pdb_surface() {
    cluster_file=$1
    while read line; do
        pdb_id=$(echo "$line" | cut -f3 | cut -f1 -d":" | tr '[:upper:]' '[:lower:]')
        chain_id=$(echo "$line" | cut -f3 | cut -f2 -d":")
        pdb_file=$pdb_id$chain_id.pdb
        srf_file=$pdb_id$chain_id.srf
        # # only do comp. intensive probis surface extraction if there is NO srf or srf is empty
        # [ -s $SRFDIR/pdb/all/$srf_file.gz ] && continue
        convert_format -i $BIODIR/${pdb_id}.json.gz -o $TMP/${pdb_id}${chain_id}_all.pdb
        # get pdb file from bio database
        cat $TMP/${pdb_id}${chain_id}_all.pdb |
            awk 'BEGIN{b="^ATOM|^HETATM"} $1 ~ "ENDMDL" {b="^ATOM"} $1 ~ b { print $0 } $1 ~ "^TER|^MODEL|^ENDMDL" { print $0 }' >$TMP/$pdb_file
        rm -f $TMP/${pdb_id}${chain_id}_all.pdb
        mkdir -p $SRFDIR/pdb/all
        echo $TMP/$pdb_file $chain_id $SRFDIR/pdb/all/$srf_file compress_srf
    done <$cluster_file
}

function list_alphafold_surface() {
    for alphafold_gz_file in $(find $ALPHAFOLD/Homo_sapiens -name *.pdb.gz); do
        alphafold_gz_basename=$(basename $alphafold_gz_file)
        alphafold_id=${alphafold_gz_basename%.pdb.gz}
        alphafold_dir=$(dirname $alphafold_gz_file)
        species=$(basename $alphafold_dir)
        alphafold_file=$TMP/${alphafold_gz_basename%.gz}
        gunzip -c $alphafold_gz_file >$alphafold_file
        mkdir -p $SRFDIR/alphafold/$species
        echo $alphafold_file A $SRFDIR/alphafold/$species/$alphafold_id.srf
    done
}

function disable_cluster() {
    echo -e "\n\nDisabling cluster & exiting..."
    clustermode.bash ${BASE} ${WORKNODE_LIST} down
    exit
}

export -f parse_json
export -f retry
export -f all_worknodes
export -f confirm
export -f timestamp
export -f download
export -f probisit
export -f probisligandsit
export -f probisdockdatabaseit
export -f list_alphafold_human_proteins
export -f list_representative_pdb_proteins
export -f list_member_pdb_proteins
export -f list_pdb_surface
export -f list_alphafold_surface
export -f disable_cluster
