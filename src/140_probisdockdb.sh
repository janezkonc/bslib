#!/bin/bash

confirm "Are you sure you want to delete ${PROBISDOCKDB}?" &&
	rm -Rf $PROBISDOCKDB $TMP/dbg140.txt && mkdir -p $PROBISDOCKDB

# create transient cluster (share base directory to all nodes -- sshfs must be installed on all nodes)
clustermode.bash ${BASE} ${WORKNODE_LIST} up
trap disable_cluster INT

confirm "Do you want to continue to run step 140 on the transient (sshfs) cluster?" || disable_cluster

# iterate over all predicted ligands files that we found with ProBiS-ligands
find $PROBISLIGANDSDB -name "aff_*.json.gz" | ${BASE}/bin/parallel/parallel --env PROBISDOCKDB --env PROBISLIGANDSDB \
	--env PROBISDOCKDBEXE --env PDBDIR --env ALPHAFOLD --env LIGDIR --env PATH --env TMP --env probisdockdatabaseit \
	--progress --jobs 100% --timeout 7200 --joblog $TMP/par140.log --retries 3 --memfree 1G \
	--sshloginfile $WORKNODE_LIST "140_parallel.sh {}"
# --retries 3 --memfree 1G --resume --sshloginfile $WORKNODE_LIST "140_parallel.sh {1} {2}"

# umount transient cluster
confirm "Parallel jobs have finished. Do you wish to disable cluster?" && disable_cluster
