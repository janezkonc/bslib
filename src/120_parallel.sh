#!/bin/bash

{
    json_file=$1
    id=$(basename $1 .json.gz)
    len=${#id}
    (($len == 5)) && {
        # PDB structure
        pdb_id=${id:0:4}
        chain_id=${id:4:1}
        receptor_file=$PDBDIR/all/pdb/pdb$pdb_id.ent.gz
    } || {
        # AlphaFold structure
        pdb_id=$id
        chain_id=A
        receptor_file=$ALPHAFOLD/Homo_sapiens/$id.pdb.gz
    }

    echo "Calculating protein ${pdb_id} chain ${chain_id} ..."
    probisligandsit $receptor_file $chain_id $json_file $id $TMP/filter_probis_json.txt

} &>>$TMP/dbg120.txt
