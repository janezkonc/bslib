#!/bin/bash

{
    pdb_file=$1

    [[ $pdb_file =~ .*AF.* ]] && {
        # AlphaFold structure (possibly in chunks)
        patt=$(echo $pdb_file | sed -r 's/-F1-/-F*-/')
        pdb_files=$(ls -v $patt) # sort chunks numerically, i.e., F1, F2, F3...
        pdb_id=$(basename $pdb_file .pdb.gz)
        pdb_id=$(echo $pdb_id | sed -r 's/-F1-/-/')
        sifts_file=""
    } || {
        # PDB structure
        pdb_files=$pdb_file
        pdb_id=$(basename $pdb_file .ent.gz)
        pdb_id=${pdb_id#pdb}
        code=${pdb_id:1:2}
        sifts_file=$SIFTS/$code/${pdb_id}.xml.gz
    }

    mapping_file=$GENPROBISDB/${pdb_id}_variants_structure_mapping.json.gz

    echo "Calculating UniProt to PDB mapping for protein ${pdb_id}..."

    # sifts_file must be escaped due to possible empty string
    genmapping \
        --pdb_files $pdb_files \
        --separated_uniprot_variants_dir $GENEXDB/uniprot_variants \
        --sifts_file "$sifts_file" \
        --mapping_file $mapping_file

} &>>$TMP/dbg160.txt
