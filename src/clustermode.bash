#!/bin/bash

# clustermode.bash:
# Set up a shared sshfs file system in the same path (relative to $HOME)
# Adapted from: https://gist.github.com/Brainiarc7/24c966c8a001061ee86cc4bc05826bf4

set -ue

usage_message="Usage: clustermode.bash directory-to-share worknodes-file <up|down> this-host-name"

# If script is called without specifying a master node, assume it's us.
sharedir=$1 # directory to be shared on all nodes
WORKNODES=$2 # a list of worknodes to be turned into transient cluster (ncpu/name)
master=${4:-$(hostname)}

up ()
{
    [ -d "$sharedir" ] || mkdir -p "$sharedir"
    for line in $(grep -v $master "${WORKNODES}"); do
        host=${line#*/} # trim everything before /
        # Connect to master node; -t required for passwords.
        ssh -t $host "[ -d \"$sharedir\" ] || mkdir -p \"$sharedir\";\
            nohup sshfs -o Cipher=arcfour -o follow_symlinks \
            \"${master}:$sharedir\" \"$sharedir\" &>/dev/null" \
            || echo "$host unreachable"
    done
}

down ()
{
    
    for line in $(grep -v $master "${WORKNODES}"); do
        host=${line#*/} # trim everything before /
        ssh -t $host "fusermount -z -u \"$sharedir\"" \
            || echo "Could not deactivate ${host}."
    done
}


if [[ $# -eq 0 ]];then
    echo "$usage_message"
    exit 1
fi

if [[ "$3" == "up" ]]; then
    up
elif [[ "$3" == "down" ]]; then
    down
else
    echo "$usage_message"
fi
