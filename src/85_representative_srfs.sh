#!/bin/bash

export CLUSTER_FILE=$1
confirm "Are you sure you want to delete $SRFDIR/pdb/rep?" && rm -Rf $SRFDIR/pdb/rep
mkdir -p $SRFDIR/pdb/rep

rm -f $TMP/dbg85.txt
for pdbchain in $(sort -nusk1,1 $CLUSTER_FILE | tr -d " " | tr "\t" "_" | cut -f3 -d"_" | sed -r "s/(.{4}):(.{1})$/\L\1\E\2/"); do
    rep=$pdbchain.srf
    gunzip -c $SRFDIR/pdb/all/$rep.gz >$SRFDIR/pdb/rep/$rep 2>>$TMP/dbg85.txt
    [[ ! -s $SRFDIR/pdb/rep/$rep ]] && rm -f $SRFDIR/pdb/rep/$rep
done
