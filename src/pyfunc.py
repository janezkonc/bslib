import glob
import io
import sys
import csv
from collections import defaultdict
import os
import gzip
import tarfile
import zipfile
import json
import multiprocessing as mp
import re
import subprocess


EVALUE = 0.001  # "E()-values < 0.001 can reliably be used to infer homology" (https://fasta.bioch.virginia.edu/wrpearson/fasta/fasta_guide.pdf)

# ligand is an object
# rep is 1allA or AF-Q7RTR0-F1-model_v1


def get_protein_similarity(ligand, rep, seq_dir_rep, seq_dir_lig):

    print("get_protein_similarity", ligand, rep, seq_dir_rep, seq_dir_lig)
    comments = ligand["comments"]

    lig_pdb_id = comments["pdb_id"]
    lig_chain_id = comments["bs_chain_id"]

    lig = lig_pdb_id + lig_chain_id
    # Poseben primer, ki je jasen
    if lig == rep:
        return 100.0
    # calculate seq. id. from PDB files
    representative_fasta_file = os.path.join(
        seq_dir_rep, rep + ".fasta")
    ligand_fasta_file = os.path.join(seq_dir_lig, lig + ".fasta")
    print("get_protein_similarity", representative_fasta_file,
          ligand_fasta_file, seq_dir_rep, seq_dir_lig)
    seq_id = subprocess.check_output(
        'bin/fasta36 -E ' + str(EVALUE) + ' ' + representative_fasta_file + ' ' + ligand_fasta_file +
        ' | grep identity | sed -r \'s/.* ([0-9\.]+)% identity.*/\\1/\' | head -1',
        shell=True)
    print("get_protein_similarity", seq_id)
    result = 0.0
    try:
        result = float(seq_id)
    except ValueError:
        pass
    return result


# echo "Predict binding sites: grids, centroids, ligands... for all PDB proteins"
def get_pdb_bind_data(download_dir):

    result = defaultdict(list)

    tmp_loc = os.path.join(download_dir, "PDBbind.tar.gz")
    data = tarfile.open(tmp_loc)
    # the name of the file changes with every new release
    pat = re.compile(r'.*INDEX_general_PL_data.*')
    filename = [m for m in data.getmembers() if pat.search(m.name)][0]
    data = data.extractfile(filename)
    data = data.read()
    data = data.decode().split("\n")

#	with open("pdb_bind/index/INDEX_general_PL_data.2017") as pdbBind:
    for line in data:
        if not line.startswith("#") and line:
            spl = line.split()
            binding_data = spl[4].strip()
            receptor = spl[0].strip().upper()
            ligand = spl[7].strip("()").strip()
            if "<" not in binding_data or ">" not in binding_data:
                unit = binding_data[-2:]
                if "=" in binding_data:
                    value = float(binding_data.split("=")[1][0:-2])
                    vType = binding_data.split("=")[0]
                if "~" in binding_data:
                    value = float(binding_data.split("~")[1][0:-2])
                    vType = binding_data.split("~")[0]
                vType = vType.upper()
                if vType == "IC50":
                    vType = "IC"
                ## all to nM! ##
                if unit == "fM":
                    value = value / 1000000
                if unit == "pM":
                    value = value / 1000
                if unit == "uM":
                    value = value * 1000
                if unit == "mM":
                    value = value * 1000000
                VAL = defaultdict(float)
                if "mer" not in ligand:
                    if "/" in ligand or "-" in ligand or "&" in ligand:  # multiple ligands in one line
                        if "/" in ligand:
                            ligand = ligand.split("/")
                        if "-" in ligand:
                            ligand = ligand.split("-")
                        if "&" in ligand:
                            ligand = ligand.split("&")

                        for l in ligand:
                            pair = l + "_" + receptor
                            VAL[vType] = value
                            result[pair] = VAL
                    else:
                        pair = ligand + "_" + receptor
                        VAL[vType] = value
                        result[pair] = VAL
    return result


def get_bindingDB_data(download_dir):

    pairs = []
    slovar = defaultdict(list)
    result = defaultdict(lambda: defaultdict(float))

    tmp_loc = os.path.join(download_dir, "BindingDB.tsv.zip")
    with zipfile.ZipFile(tmp_loc) as zfile:
        with zfile.open("BindingDB_All_202309.tsv", "r") as bdb:
            reader = csv.reader(io.StringIO(bdb.read().decode()),
                                delimiter='\t', quoting=csv.QUOTE_NONE)

            i = next(reader)
            li = i.index("Ligand HET ID in PDB")
            ri = i.index("PDB ID(s) of Target Chain")
            kii = i.index("Ki (nM)")
            ici = i.index("IC50 (nM)")
            kdi = i.index("Kd (nM)")
            eci = i.index("EC50 (nM)")
            for row in reader:
                if li > len(row) or ri > len(row) or kii > len(row) or ici > len(row) or kdi > len(row) or eci > len(row):
                    continue
                pairs = []
                ligand = row[li].strip()
                receptors = row[ri].strip().split(",")
                receptors = [r for r in receptors if r != ""]

                if ligand and receptors and len(ligand) > 2:
                    pairs = [ligand + "_" + r for r in receptors]
                    KI = row[kii].strip()
                    IC = row[ici].strip()
                    KD = row[kdi].strip()
                    EC = row[eci].strip()
                    if KI and "<" not in KI and ">" not in KI:
                        KI = float(KI.strip())
                    else:
                        KI = None
                    if IC and "<" not in IC and ">" not in IC:
                        IC = float(IC.strip())
                    else:
                        IC = None
                    if KD and "<" not in KD and ">" not in KD:
                        KD = float(KD.strip())
                    else:
                        KD = None
                    if EC and "<" not in EC and ">" not in EC:
                        EC = float(EC.strip())
                    else:
                        EC = None
                    for pair in pairs:
                        if not (KI or IC or KD or EC):
                            continue
                        if pair not in slovar:
                            slovar[pair] = defaultdict(list)
                        if KI:
                            slovar[pair]["KI"].append(KI)
                        if IC:
                            slovar[pair]["IC"].append(IC)
                        if KD:
                            slovar[pair]["KD"].append(KD)
                        if EC:
                            slovar[pair]["EC"].append(EC)
    for k, v in slovar.items():
        result[k] = defaultdict(float)
        for a in v:
            result[k][a] = min(v[a])
    return result


def get_pdb_bind_PROTEIN_data(download_dir):

    result = defaultdict(list)

    tmp_loc = os.path.join(download_dir, "PDBbind.tar.gz")
    data = tarfile.open(tmp_loc)
    # the name of the file changes with every new release
    pat = re.compile(r'.*INDEX_general_PP.*')
    filename = [m for m in data.getmembers() if pat.search(m.name)][0]
    data = data.extractfile(filename)
    data = data.read()
    data = data.decode("utf-8", "ignore").split("\n")
    for line in data:
        if not line.startswith("#") and line:
            spl = line.split()
            binding_data = spl[3].strip()
            receptor = spl[0].strip().upper()
            # ligand = spl[7].strip("()").strip()
            if "<" not in binding_data or ">" not in binding_data:
                unit = binding_data[-2:]
                if "=" in binding_data:
                    value = float(binding_data.split("=")[1][0:-2])
                    vType = binding_data.split("=")[0]
                if "~" in binding_data:
                    value = float(binding_data.split("~")[1][0:-2])
                    vType = binding_data.split("~")[0]
                vType = vType.upper()
                if vType == "IC50":
                    vType = "IC"
                ## all to nM! ##
                if unit == "fM":
                    value = value / 1000000
                if unit == "pM":
                    value = value / 1000
                if unit == "uM":
                    value = value * 1000
                if unit == "mM":
                    value = value * 1000000
                VAL = defaultdict(float)
                VAL[vType] = value
                result[receptor] = VAL
    return result


def add_to_ligand(ligands_file, pdb_bind_data, bindingDB_data, protein_data, probisligandsdb_dir, seq_dir_rep, seq_dir_lig):
    print("Go over ligand ligands_file = ", ligands_file)
    rep = str(os.path.basename(ligands_file)).replace(
        "ligands_", "").replace(".json.gz", "")

    with gzip.open(ligands_file, "rt", encoding="UTF-8") as ligands:
        print("Current:", ligands_file)
        try:
            binding_sites = json.load(ligands)
            if not binding_sites:
                print("exit due to null")
                return
            print("After loading:", ligands_file)
            for bsite in binding_sites:
                print("bsite", bsite)
                for ligand in bsite["data"]:
                    comments = ligand["comments"]
                    if "small" in comments["rest"]:
                        complex_pair = comments["resn"].upper(
                        ) + "_" + comments["pdb_id"].upper()
                        pdbbind = pdb_bind_data[complex_pair]
                        print("complex_pair ", complex_pair)
                        if pdbbind:
                            comments["pdb_bind"] = pdbbind
                        else:
                            comments["pdb_bind"] = None
                        print("before bindingdb")
                        bindingdb = bindingDB_data[complex_pair]
                        print("after bindingdb")
                        if bindingdb:
                            comments["binding_db"] = bindingdb
                        else:
                            comments["binding_db"] = None
                        print("before seq_id", ligand, rep)
                        S = get_protein_similarity(
                            ligand, rep, seq_dir_rep, seq_dir_lig)
                        print("after seq_id")
                        comments["seq_id"] = S
                    # PPI are from the PDBbind database.
                    elif "protein" in comments["rest"]:
                        print("before proteinBind")
                        proteinBind = protein_data[comments["pdb_id"].upper()]
                        print("after proteinBind")
                        # this database does not contain PPIs, therefore always None. Here for JSON consistency.
                        comments["binding_db"] = None
                        if proteinBind:
                            comments["pdb_bind"] = proteinBind
                        else:
                            comments["pdb_bind"] = None
                        S = get_protein_similarity(
                            ligand, rep, seq_dir_rep, seq_dir_lig)
                        comments["seq_id"] = S

            print("After reading ligands for:", ligands_file)

            mod_ligands_file = os.path.join(
                probisligandsdb_dir, "aff_" + str(os.path.basename(ligands_file)))

            with gzip.open(mod_ligands_file, "wt", encoding="utf8") as w:
                tst = json.dump(binding_sites, w)
                # print(tst)
            print("After writing aff ligands:", mod_ligands_file)
        except:
            # raise Exception("Error in reading JSON. Empty?', ligands_file, file=sys.stderr")
            print("Unexpected error:", sys.exc_info()[0])
            print('Error in processing JSON:', ligands_file)


def convert_to_fasta(pdb_file, seq_dir):
    try:
        print("Convert to fasta", pdb_file)
        protein_id = str(os.path.basename(pdb_file)).replace(".gz", "").replace(
            ".pdb", "").replace(".ent", "")
        protein_id = re.sub("^pdb", "", protein_id)
        is_alphafold = protein_id.startswith("AF-")
        fasta_file = os.path.join(seq_dir, protein_id + ".fasta")

        print("pdb_file", pdb_file)
        print("protein_id", protein_id)
        print("fasta_file", fasta_file)

        os.system("bin/convert_format -i " + pdb_file + " -o " + fasta_file)
        if is_alphafold:
            return
        # split into one file per Chain ID
        f = open(fasta_file, 'r')
        lines = f.readlines()
        f.close()
        cnt = 1
        chain_id = ""
        for line in lines:
            if cnt % 2 == 1:
                chain_id = line[7:8]
                ll = [line]
            elif cnt % 2 == 0:
                ll.append(line)
                # print("chain_id", chain_id)
                # print("seq", line)
                fasta_chain_file = os.path.join(
                    seq_dir, protein_id + chain_id + ".fasta")
                fw = open(fasta_chain_file, 'w')
                fw.writelines(ll)
                fw.close()
                chain_id = ""
            cnt += 1
    except:
        print("Unexpected error:", sys.exc_info()[0])


def go_over(files, funct):
    pool = mp.Pool(processes=mp.cpu_count() * 3)
    pool.map(funct, files)
    pool.close()


# these functions annotate pdb codes with taxonomical groups, cancer-relatedness, kinase activity...

def get_taxonomy(download_dir):
    tFile = os.path.join(download_dir, "taxdump.tar.gz")
    ## TAXONOMY DATABASE ##
    tf = tarfile.open(tFile, "r:gz")
    tax = tf.extractfile("nodes.dmp").read().decode().split("\n")
    reader = csv.DictReader(tax, fieldnames=["tax_id", "parent_tax_id", "rank", "embl_code", "division_id",
                                             "inherited_div_flag", "genetic_code_id", "inherited_GC__flag", "mitochondrial_genetic_code_id",
                                             "inherited_MGC_flag", "GenBank_hidden_flag", "hidden_subtree_root_flag", "comments"],
                            delimiter="|", quotechar='\t')
    tax_dict = {}
    for row in reader:
        tax_dict[int(row["tax_id"].strip("\t"))] = int(row["division_id"])
    # many of the PDBs contain obsolete tax_ids that were merged at some point
    mer = tf.extractfile("merged.dmp").read().decode().split("\n")
    reader = csv.DictReader(
        mer, fieldnames=['orig_tax_id', 'new_tax_id'], delimiter="|", quotechar='\t')
    mer_dict = {}
    for row in reader:
        mer_dict[int(row["orig_tax_id"].strip("\t"))] = int(row["new_tax_id"])
    # read also the legend for division_id to human readable division_name
    leg = tf.extractfile("division.dmp").read().decode().split("\n")
    reader = csv.DictReader(leg, fieldnames=[
                            'division_id', 'code', 'division_name', 'comment'], delimiter="|", quotechar='\t')
    div_dict = {}
    for row in reader:
        div_dict[int(row["division_id"].strip("\t"))] = row["division_name"]

    return tax_dict, mer_dict, div_dict


def get_ec_numbers(download_dir):
    tmpFile = os.path.join(download_dir, "pdb_chain_enzyme.tsv.gz")
    # Get human pdb codes
    field_names = ["pdb_id", "chain_id", "accession_id", "ec_number"]
    ec_numbers = {}
    with gzip.open(tmpFile) as f:
        f = f.read().decode().split("\n")
        reader = csv.DictReader(f, fieldnames=field_names, delimiter="\t")
        for row in reader:
            key = (row["pdb_id"], row["chain_id"])
            if not key in ec_numbers:
                ec_numbers[key] = list()
            ec_numbers[key].append(row["ec_number"])
    return ec_numbers


##
# Gets the non-unique mapping uniprot_id -> (pdb_id, chain_id) - one of the possible pdb/chain_ids is returned
# and the most likely unique mapping (in most cases) between (pdb_id, chain_id) and uniprot_id.
#


def get_uni_to_pdb_and_vice_versa(download_dir):
    tmpFile = os.path.join(download_dir, "pdb_chain_uniprot.tsv.gz")
    field_names = ["pdb_id", "chain_id", "uniprot_id", "RES_BEG",
                   "RES_END", "PDB_BEG", "PDB_END", "SP_BEG", "SP_END"]
    uni_to_pdb = {}
    pdb_to_uni = {}
    with gzip.open(tmpFile) as f:
        f = f.read().decode().split("\n")
        reader = csv.DictReader(f, fieldnames=field_names, delimiter="\t")
        for row in reader:
            key = row["uniprot_id"]
            val = (row["pdb_id"], row["chain_id"])
            uni_to_pdb[key] = val
            pdb_to_uni[val] = key
    return uni_to_pdb, pdb_to_uni


def get_cancer_related(download_dir):
    # Download uniprot to pdb mapping from SIFTS
    tmpFile = os.path.join(download_dir, "pdb_chain_ensembl.tsv.gz")
    tmpFile2 = os.path.join(download_dir, "pdb_chain_taxonomy.tsv.gz")

    # Get human pdb codes
    field_names = ["pdb_id", "chain_id", "tax_id", "scientific_name"]
    human_pdbs = set()
    with gzip.open(tmpFile2) as f:
        f = f.read().decode().split("\n")
        reader = csv.DictReader(f, fieldnames=field_names, delimiter="\t")
        for row in reader:
            if row["tax_id"] == 9606:
                human_pdbs.add(row["pdb_id"])

    # Get Ensembl to PDB mapping
    field_names = ["pdb_id", "chain_id", "uniprot_id",
                   "gene_id", "transcript_id", "translation_id", "exon_id"]
    ensembl_to_pdb = defaultdict(set)
    with gzip.open(tmpFile) as f:
        f = f.read().decode().split("\n")
        reader = csv.DictReader(f, fieldnames=field_names, delimiter="\t")
        for row in reader:
            if not row["pdb_id"] is None:
                print(row)
                pdb_id = row["pdb_id"]
                gene_id = row["gene_id"]
                ensembl_to_pdb[gene_id].add(pdb_id)
                print(gene_id, pdb_id)

    # Cancer related genes translated to existing proteins
    first = ["gene", "synonym", "ensembl"]
    together = set()
    t = gzip.open(os.path.join(download_dir, "cancer_related.tsv.gz")
                  ).read().decode().split("\n")
    reader = csv.DictReader(t, fieldnames=first, delimiter="\t")
    for row in reader:
        ensemble = row["ensembl"]
        if ensemble in ensembl_to_pdb:  # if PDB exists
            together.update(ensembl_to_pdb[ensemble])
    return together


def annotate_pdbs(download_dir, pdb_dir, alphafold_dir, anno_dir):
    # Here we create a list of PDB IDs annotated by taxonomy
    tax_dict, mer_dict, div_dict = get_taxonomy(download_dir)
    cancer_related = get_cancer_related(download_dir)
    ec_numbers = get_ec_numbers(download_dir)
    # Protein kinases are obtained from https://en.wikipedia.org/wiki/Protein_kinase and
    # https://en.wikipedia.org/wiki/List_of_EC_numbers_(EC_2)
    kinase_codes = ["2.7.10.", "2.7.11.", "2.7.12.", "2.7.13.", "2.7.99."]
    uni_to_pdb, pdb_to_uni = get_uni_to_pdb_and_vice_versa(download_dir)

    for pdb_file in glob.glob(os.path.join(pdb_dir, "*.ent.gz")) + glob.glob(os.path.join(alphafold_dir, "**", "*.pdb.gz")):
        print(pdb_file)
        filename = os.path.basename(pdb_file)
        alphafold_id = None
        uniprot_id = None
        pdb_id = None
        chain_id = None

        is_alphafold = True if "AF" in pdb_file else False

        if is_alphafold:
            alphafold_id = filename.replace(".pdb.gz", "")
            uniprot_id = filename.split("-")[1]
            val = uni_to_pdb.get(uniprot_id)
            if val:
                pdb_id, chain_id = val  # None if pdb does not exist
        else:
            pdb_id = filename[3:7]

        beta_factors = {}

        # key = mol_id, values are different annotations for a specific molecule
        anno_by_mol_id = {}
        # ...there can be more in one PDB
        mol_id = -1
        is_continued = False
        with gzip.open(pdb_file, "rt") as lines:
            for line in lines:
                if line.startswith("ATOM") or line.startswith("HETATM"):
                    resn = line[17:20].strip()
                    chain_id = line[21]
                    resi = int(line[22:26])
                    # ins_code must be character ASCII code (integer)
                    ins_code = ord(line[26])
                    temperature_factor = float(line[60:66])
                    if chain_id not in beta_factors:
                        beta_factors[chain_id] = {}
                    residue_id = str(resn) + '_' + \
                        str(resi) + '_' + str(ins_code)
                    beta_factors[chain_id][residue_id] = temperature_factor
                elif (line.startswith('COMPND') or line.startswith('SOURCE')) and "MOL_ID:" in line:
                    mol_id = line.partition("MOL_ID:")[
                        2].rstrip().strip(" ;")
                    # initialize the dictionary with empty lists
                    if (mol_id not in anno_by_mol_id.keys()):
                        anno_by_mol_id[mol_id] = {'pdb_id': pdb_id, 'chain_id': chain_id,
                                                  'molecule': '', 'tax_id': [],
                                                  'division_id': [], 'division_name': [], 'organism_scientific': [],
                                                  'ec': [], 'disease': 'cancer' if pdb_id in cancer_related else None, 'protein_class': None}
                        if is_alphafold:
                            anno_by_mol_id[mol_id]['alphafold_id'] = alphafold_id
                            anno_by_mol_id[mol_id]['uniprot_id'] = uniprot_id
                elif line.startswith('COMPND') and (re.search("^COMPND.{5}CHAIN:", line) or is_continued):
                    chains_str = line[11:].rstrip() if is_continued else line.partition("CHAIN:")[
                        2].rstrip()
                    chain_ids = chains_str.split(",")
                    new_chain_ids = [c.strip(" ;")
                                     for c in chain_ids if c != '']
                    if is_continued:
                        anno_by_mol_id[mol_id]['chain_ids'].extend(
                            new_chain_ids)
                    else:
                        anno_by_mol_id[mol_id]['chain_ids'] = new_chain_ids
                    is_continued = chains_str[-1] == ','
                elif line.startswith('COMPND') and ('MOLECULE:') in line:
                    anno_by_mol_id[mol_id]['molecule'] = line.partition("MOLECULE:")[
                        2].rstrip().strip(" ;")
                elif line.startswith('SOURCE') and "ORGANISM_TAXID:" in line:
                    tax_ids = line.partition("ORGANISM_TAXID:")[
                        2].rstrip().strip(",; ").split(",")
                    # correct obsolete tax_ids in PDBs (eg. 1ntc has tax_id = 602 which was merged into 90371)
                    tax_ids = [mer_dict.get(int(t), int(t))
                               for t in tax_ids if t.strip().isdigit()]
                    anno_by_mol_id[mol_id]['tax_id'] = tax_ids
                    division_ids = [tax_dict.get(t) for t in tax_ids]
                    anno_by_mol_id[mol_id]['division_id'] = division_ids
                    anno_by_mol_id[mol_id]['division_name'] = [
                        div_dict.get(t) for t in division_ids]
                elif line.startswith('SOURCE') and "ORGANISM_SCIENTIFIC:" in line:
                    prt = line.partition("ORGANISM_SCIENTIFIC:")[
                        2].rstrip().split(",")
                    prt = [p.strip(" ;") for p in prt if len(p) > 1]
                    anno_by_mol_id[mol_id]['organism_scientific'] = prt

        # print("anno_by_mol_id", anno_by_mol_id)

        for mol_id, anno in anno_by_mol_id.items():
            print("anno = ", anno)
            pdb_id = anno['pdb_id']
            for chain_id in anno['chain_ids']:
                # get a unique identifier of a protein chain in the database
                uniq_id = anno['alphafold_id'] if 'alphafold_id' in anno else pdb_id + chain_id
                annotated = {}
                annotated = anno.copy()

                # add chain_id
                annotated['chain_id'] = chain_id

                # add beta factors (must test if chain_id exist, due to problematics e.g. 7a4p)
                annotated['beta_factors'] = beta_factors[chain_id].copy(
                ) if chain_id in beta_factors else {}

                # add uniprot_id (only for PDB proteins)
                if not 'uniprot_id' in annotated:
                    annotated['uniprot_id'] = pdb_to_uni.get(
                        (pdb_id, chain_id))

                key = (pdb_id, chain_id)
                if key in ec_numbers:
                    ec_list = ec_numbers.get(key)
                    # add ec numbers
                    annotated['ec'] = ec_list
                    # check if protein chain is a "protein kinase"
                    protein_class = None
                    for ele in kinase_codes:
                        for x in ec_list:
                            if ele in x:
                                protein_class = 'kinase'
                                break
                    annotated['protein_class'] = protein_class
                # ...and remove unnecessary key
                del annotated['chain_ids']
                print("ANNOTATED uniq_id = ", uniq_id,
                      "annotated = ", annotated)

                # output annotated PDB and AlphaFold
                with gzip.open(os.path.join(anno_dir, str(uniq_id) + '.json.gz'), 'w') as fout:
                    fout.write(json.dumps(annotated).encode('utf-8'))


# Annotate each binding site with e.g., taxonomy, cancer-relatedness, kinase activity
# A binding site is identified by bsite_rest and bs_id, a protein is identified by uniq_id
# AlphaFold proteins contain confidence values as beta factors. These are added to each
# binding site, so that each binding site is annotated with the lowest (worst) confidence score
# between its residues.
# Each binding site is also annotated with a compound score (Sbsite score), relevant for small.*
# types of bsite_rest.

def extend_annotations(probisligandsdb_dir, probisdockdb_dir, anno_dir):
    # The input is a dictionary, return value is a list with added chain_id, bsite_rest keys
    extended = []
    compound_scores = {}
    conservations = {}
    num_occurrences = {}

    # read compound score for each binding site
    for ligands_file in glob.glob(os.path.join(probisligandsdb_dir, "aff_ligands*")) + glob.glob(os.path.join(probisdockdb_dir, "ligands*")):
        with gzip.open(ligands_file, "rt", encoding="UTF-8") as ligands_j:
            ligands_j = json.load(ligands_j)
            if ligands_j is None:
                continue
            uniq_id = str(os.path.basename(ligands_file)).replace(
                "aff_ligands_", "").replace("ligands_", "").replace(".json.gz", "")
            for binding_site in ligands_j:
                compound_scores[(uniq_id, binding_site["bsite_rest"],
                                 binding_site["bs_id"])] = binding_site["compound_score"]
                conservations[(uniq_id, binding_site["bsite_rest"],
                               binding_site["bs_id"])] = binding_site["cons"]
                num_occurrences[(uniq_id, binding_site["bsite_rest"],
                                 binding_site["bs_id"])] = binding_site["num_occ"]

    # annotate binding sites
    for bresi_file in glob.glob(os.path.join(probisligandsdb_dir, "bresi*")) + glob.glob(os.path.join(probisdockdb_dir, "bresi*")):
        print("Bresi file = ", bresi_file)
        with gzip.open(bresi_file, "rt", encoding="UTF-8") as bresi_j:
            bresi_j = json.load(bresi_j)
            if bresi_j is None:
                continue
            uniq_id = str(os.path.basename(bresi_file)).replace(
                "bresi_", "").replace(".json.gz", "")

            annotated = {}
            with gzip.open(os.path.join(anno_dir, uniq_id + '.json.gz'), "rt", encoding="UTF-8") as annotated:
                annotated = json.load(annotated)

            for binding_site in bresi_j:
                print("binding_site", binding_site)
                bsite_rest = binding_site["bsite_rest"]
                bs_id = binding_site["bs_id"]
                compound_score = compound_scores[(
                    uniq_id, bsite_rest, bs_id)]
                num_occ = num_occurrences[(
                    uniq_id, bsite_rest, bs_id)]
                cons = conservations[(
                    uniq_id, bsite_rest, bs_id)]
                min_beta_factor = 10000.0
                print("uniq_id", uniq_id, " annotated.keys()", annotated.keys())

                # calculate binding site's lowest beta factor - worst confidence level for AlphaFold
                for residue in binding_site["data"]:
                    # residue_id = (
                    #     residue["resn"], residue["resi"], residue["ins_code"])
                    residue_id = str(
                        residue["resn"]) + '_' + str(residue["resi"]) + '_' + str(residue["ins_code"])
                    chain_id = residue["chain_id"]
                    # print('residue_id', residue_id)
                    # print('chain_id', chain_id)
                    # print('keys', annotated["beta_factors"])

                    beta_factors = annotated["beta_factors"]
                    if residue_id in beta_factors:
                        beta_factor = beta_factors[residue_id]
                        if beta_factor < min_beta_factor:
                            min_beta_factor = beta_factor

                value = annotated.copy()
                del value['beta_factors']
                value['bsite_rest'], value['bs_id'], value['uniq_id'], value['compound_score'], value['min_beta_factor'], value[
                    'num_occ'], value['cons'] = bsite_rest, bs_id, uniq_id, compound_score, min_beta_factor, num_occ, cons
                extended.append(value)
                print("extended=", value)
    return extended
