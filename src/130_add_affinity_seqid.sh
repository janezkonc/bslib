#!/bin/bash

# Naredimo nove ligande "aff_ligands_" v PROBISLIGANDSDB direktoriju, ki vsebujejo affinity data
#(IC50, Ki, Kd) in njihov sequence identity glede na query protein

python3 src/130_add_affinity_seqid.py $DOWNLOAD $PROBISLIGANDSDB $SEQDIR &>$TMP/dbg130.txt
