## set of methods for copmutation of core network statistics

import matplotlib
matplotlib.use('Agg')
import networkx as nx
from collections import Counter
import operator
import numpy as np
import matplotlib.pyplot as plt
from misc import *
import pandas as pd
import seaborn as sns
sns.set_style("whitegrid")

def load_network(graph_file,tmap):

    mappings = {}        
    with open(tmap) as tm:
        for line in tm:
            line = line.strip().split("\t")
            if line[0] not in mappings:
                mappings[str(line[0])] = line[1]

    G = nx.Graph()
    with open(graph_file) as gf:
        for line in gf:
            line = line.strip().split(" ")
            if len(line) >=3:
                if line[2] == "TAG_BIN":
                    n1 = line[0]
                    n2 = line[1]

                    try:                        
                        species_first = " ".join(mappings[n1.split("_")[1]].split(" ")[0:2])
                        species_second = " ".join(mappings[n2.split("_")[1]].split(" ")[0:2])
                        G.add_node(n1,species=species_first)
                        G.add_node(n2,species=species_second)
                        G.add_edge(n1,n2,tag="TAG_BIN")
                    except:
                        print("No mapping for {} {}".format(n1,n2))

                if line[2] == "TAG_PRL":
                    n1 = line[0]
                    n2 = line[1]
                    try:
                        species_first = " ".join(mappings[n1.split("_")[1]].split(" ")[0:2])
                        species_second = " ".join(mappings[n2.split("_")[1]].split(" ")[0:2])
                        G.add_node(n2,species=species_second)
                        if len(line) < 5:
                            G.add_edge(n1,n2,tag="TAG_PRL")
                        else:
                            G.add_edge(n1,n2,tag="TAG_PRL")

                    except:
                        pass

    return G

def species_pie_chart(network,outfolder,models,tmap):

    mappings = {}        
    with open(tmap) as tm:
        for line in tm:
            line = line.strip().split("\t")
            if line[0] not in mappings:
                mappings[str(line[0])] = line[1]
                
    with open(models, 'r') as myfile:
        rep_spec=myfile.read().split("\n")
        rep_spec = [x for x in rep_spec if len(x) > 0]

    rep_spec = set([mappings[x] for x in rep_spec])

    species_list = [n[1]['species'] for n in network.nodes(data=True) if "species" in n[1].keys()]
    print(len(set(species_list)))
    species_list = [x for x in species_list if x in rep_spec]
    
    counts = Counter(species_list)
    counts = sorted(counts.items(), key=lambda x:x[1],reverse=True)
    print(counts)
    part_first = counts

#    part_second = counts[9:]
#    part_second_sum = ("Other",sum([x[1] for x in part_second]))
 #   part_first.append(part_second_sum)
    
    species = [x[0] for x in part_first]
    vals = [x[1] for x in part_first]
    explode = [0.0 for x in species]
    explode[0] = 0.1

    sv_counts = zip(species,vals)
    fig = plt.gcf()

    df = pd.DataFrame()
    df['species'] = species
    df['values'] = vals
    df = df.sort_values(by=['values'])
    g = sns.barplot(x='species', y="values", data=df, palette=sns.color_palette("BuGn_r",15), order=df['species'])

    groupedvalues=df.groupby('values').sum().reset_index()
    rank = groupedvalues["values"].argsort().argsort() 
    for index, row in groupedvalues.iterrows():

        g.text(index,row.values[0], row.values[0], color='black', ha="center")
        
    plt.xticks(rotation=30,size=10)
    plt.xlabel("")
    plt.ylabel("Number of proteins",size=10)
    fig.tight_layout()
    fig.set_size_inches(13, 7)
    plt.savefig(outfolder+"/barplot.png",dpi=300)
    
if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("--edgelist",default="/home/blazs/data/spin/whole_proteome_graph/reduced_proteome.edgelist")
    parser.add_argument("--outfolder",default="/home/blazs/data/spin/whole_proteome_graph")
    parser.add_argument("--taxmap",default="/home/blazs/data/spin/dbindex/taxonomy_map.tsv")
    parser.add_argument("--target_organisms",default="/home/blazs/bslib/spin_src/representative_species_list.txt")
    args = parser.parse_args()

    ## load the network
    global_network = load_network(args.edgelist,args.taxmap)
    species_pie_chart(global_network,args.outfolder,args.target_organisms,args.taxmap)
    
