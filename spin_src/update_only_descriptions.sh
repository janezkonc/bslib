## update descriptions

JSONBASE="../genprobisdb/json/"
INDEXFILES="../dbindex/"
LIGANDS="../genprobisdb/ligands/"
GENPROBISFOLDER="../genprobisdb/"

rm -rf ../geneDescriptionJson
mkdir tmpUniProt;
mkdir ../geneDescriptionJson;
cd tmpUniProt;
wget ftp://ftp.uniprot.org/pub/databases/uniprot/current_release/knowledgebase/complete/uniprot_sprot.dat.gz;
cd ..;
echo "individual gene records..";
mkdir tmpIndividualRecords;
cd tmpIndividualRecords;

zcat ../tmpUniProt/uniprot_sprot.dat.gz | perl ../parse_uniprot.pl; ## this splits the whole uniprot base to individual records
cd ..
ls tmpIndividualRecords | mawk '{print "node parse_uniprot2_json.js","tmpIndividualRecords/"$1,"../geneDescriptionJson"}' | parallel --progress --load=95%;

echo "Clearning files.."
rm -rvf tmpUniProt;
rm -rvf tmpIndividualRecords;
