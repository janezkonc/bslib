## run blast translation for homologs
## run as python3 compute_intersections_for_species.py --species_graph 4081_predictions.txt.gz --global_spin_graph whole.edgelist.gz --string_links protein.links.detailed.v10.5.txt.gz  --uniprot_string_map idmapping_string.dat.gz --uniprot_taxid_map idmapping_taxid.dat.gz


from __future__ import print_function
import argparse
from collections import defaultdict
import numpy as np
from operator import itemgetter
import gzip
import json
import subprocess
import csv

def create_dict(reader, keys, value):
	
	result_dict = defaultdict(set)
	for row in reader:
		skip = False
		if row[value] is None:
			skip = True
		for k in keys:
			if row[k] is None:
				skip = True
				break
		if skip:
			continue
		joint_keys = ''.join([row[k] for k in keys])
		result_dict[joint_keys].add(row[value])

	return result_dict

def read_tsv_file(file_name, field_names, delim='\t'):
	
	result_dict = defaultdict(set)
	with gzip.open(file_name) as f:
		f = f.read().decode().split('\n')
		return csv.DictReader(f, fieldnames=field_names, delimiter=delim, quoting=csv.QUOTE_NONE)
		
			
if __name__ == "__main__":
	
	## params: file, genes..
	parser = argparse.ArgumentParser()
	parser.add_argument("--string_links",default="protein.links.v10.5.txt.gz")
	parser.add_argument("--uniprot_string_map",default="idmapping_string.dat.gz")
	parser.add_argument("--uniprot_taxid_map",default="idmapping_taxid.dat.gz")
	args = parser.parse_args()

	string_uniprot_map = create_dict(read_tsv_file(args.uniprot_string_map, ['uni', 'skip', 'string']), ['string'], 'uni')
	uniprot_taxid_map = create_dict(read_tsv_file(args.uniprot_taxid_map, ['uni', 'skip', 'taxid']), ['uni'], 'taxid')

	#print(uniprot_taxid_map)
	
	# do the mappings
	with gzip.open(args.string_links, 'rb') as f:
		for row in f:
			line = row.decode().strip('\n')
			row = line.split(' ')
			string1 = row[0]
			string2 = row[1]
			
			#print(string1, string2)

			if string1 in string_uniprot_map and string2 in string_uniprot_map:
				uni1 = list(string_uniprot_map[string1])[0]
				uni2 = list(string_uniprot_map[string2])[0]

				#print(uni1, uni2)

				if uni1 in uniprot_taxid_map and uni2 in uniprot_taxid_map:
					taxid1 = list(uniprot_taxid_map[uni1])[0]
					taxid2 = list(uniprot_taxid_map[uni2])[0]
					#print('# ', string1, string2, uni1, uni2, taxid1, taxid2)
					print(line)
				else:
					pass
					#print('Uniprot id to taxid mapping was not found', uni1, ' or for ', uni2)
			else:
				pass
				#print('String id to uniprot mapping was not found', string1, ' or for ', string2)
