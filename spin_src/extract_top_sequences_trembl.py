## extract top sequences from trembl to a corresponding folder
import re
import argparse
import gzip
import os

def read_species_ids(id_file):
    ids = []
    with open(id_file) as idf:
        for line in idf:
            line = line.strip().split()
            ids.append(int(line[0]))
    return ids

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--species_ids")
    parser.add_argument("--input_fasta_file")
    parser.add_argument("--output_fasta_folders")
    regex = r"OX=.+S"
    args = parser.parse_args()
    tax_ids_of_relevance = set(read_species_ids(args.species_ids))
    print(tax_ids_of_relevance)
    current_sequence = []
    trigger=False
    current_os = None
    with gzip.open(args.input_fasta_file) as sin:
        for line in sin:
            line = line.decode("utf-8").strip()
            if ">" in line:
                tax_match = int(re.findall(regex, line, flags=0)[0].split(" ")[0].split("=")[1])
                if tax_match in tax_ids_of_relevance:
                    trigger = True
                    if len(current_sequence) > 0:
                        to_append = "\n".join(current_sequence)
                        directory = args.output_fasta_folders+"/"+str(tax_match)+"_"+str(tax_match)
                        if not os.path.exists(directory):
                            os.makedirs(directory)         
                        folname = directory+"/sequence_database.fa"
#                        print("Writing to: {}".format(folname))
                        with open(folname,"a") as outfa:
                            outfa.write(to_append)
                        current_sequence = []
                else:
                    trigger = False    
            if trigger:
                current_sequence.append(line)

#python3 extract_top_sequences_trembl.py --species_ids representative_species_list.txt --input_fasta_file ~/Downloads/uniprot_trembl.fasta.gz --output_fasta_folders ~/data/spin/species_specific_blast
