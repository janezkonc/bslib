// this script generates JSON files, corresponding to pathway commons
// this script takes curated disgenet and transforms it into a json file.

var fs = require('fs');
var zlib = require('zlib');

if (process.argv[2].indexOf('.gz') != -1){
    var LR = require('readline').createInterface({
	input: fs.createReadStream(process.argv[2]).on('error',()=>{	    
	    console.log("Error occured for file:",process.argv[2])
	}).pipe(zlib.createGunzip())
    });


}else{
    var LR = require('readline').createInterface({
	input: require('fs').createReadStream(process.argv[2])
    });           
}

LR.on('error', function (line) {    

    console.log("Error occured for file:",process.argv[2])    

})

var path = process.argv[3]+"/PC_pathways.txt"

LR.on('line', function (line) {

    try{
	var parts = line.split("\t")
	var g1 = parts[0]
	var g2 = parts[2]
	var pstring = parts[5].split(";")
	pstring.forEach((item)=>{
	    var ostring= g1 + "\t" + item + "\n" + g2 + "\t" + item + "\n"
	    fs.appendFileSync(path, ostring);
	})	
    }catch(err){}
})

LR.on('close', function (line){
    console.log("Finished with db generation..")
});
