var fs = require('fs');
var zlib = require('zlib');
var createGraph = require('ngraph.graph');

var graph = "";

if (process.argv[2].indexOf('.gz') != -1){
    var lineReader = require('readline').createInterface({
	   input: fs.createReadStream(process.argv[2]).on('error',()=>{	    
           console.log("Error occured for file:",process.argv[2])
       }).pipe(zlib.createGunzip())
    });
}
else{
    var lineReader = require('readline').createInterface({
        input: require('fs').createReadStream(process.argv[2])
    });           
}

lineReader.on('error', function (line) {    
    console.log("Error occured for file:",process.argv[2])      
})

lineReader.on('line', function (line) {
    graph+=line
})

lineReader.on('close', function () {
    
    graph = JSON.parse(graph);
    
    var diseases = "";
    
    if (process.argv[3].indexOf('.gz') != -1){
        var lineReader2 = require('readline').createInterface({
            input: fs.createReadStream(process.argv[3]).on('error',()=>{	    
                console.log("Error occured for file:",process.argv[3])
            }).pipe(zlib.createGunzip())
        });
    }
    else{
        var lineReader2 = require('readline').createInterface({
            input: require('fs').createReadStream(process.argv[3])
        });           
    }
    
    lineReader2.on('error', function (line) {    
        console.log("Error occured for file:",process.argv[3])      
    })

    lineReader2.on('line', function (line) {
        diseases+=line
    })
    
    lineReader2.on('close', function () {
        
        diseases = JSON.parse(diseases);
        //console.log(Object.keys(diseases))
        
        var nodes = graph.nodes;
        
        for (var partitionX in nodes) {
            for (var partitionY in nodes[partitionX]) {
                for (var i=0; i<nodes[partitionX][partitionY].length; i++) {
                    var geneName = nodes[partitionX][partitionY][i].data.label;
                    geneName = geneName.substring(0,geneName.indexOf("_"));
                    
                    var disease = diseases[geneName];
                    if (disease)
                        nodes[partitionX][partitionY][i].data.disease = disease;
                }
            }
            console.log(partitionX)
        }
        
        graph.nodes = nodes
        
        console.log("writing files")
        zlib.gzip(JSON.stringify(graph), function (error, result) {
            if (error){
                console.log(error)
            }else{
                fs.writeFile(process.argv[2],result, function(err) {
                    if(err) {
                        return console.log(err);
                    }
                });
            }
        });
    });
    
});