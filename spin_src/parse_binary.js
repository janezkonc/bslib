// parser for binary protein interactions PSI-MI format 25ish

const fs = require('fs')
const unzip = require('unzip')
const fn = process.argv[2]
const readline = require('readline');

// whitelist lists the experimental methods available.
var whitelist = []
const outfolder = process.argv[3]
var regex = "taxid:([0-9]+)"

function extract_interaction(sd_string){
    console.log(sd_string)
}

function parse_species(species_field,cb){
    try{
	var part_species = species_field.split("|")	
	//console.log(species_field)
	var result = part_species[0].match(regex);
	return result[1]+"_"+result[1];
    }catch(err){
	//console.log(err,"Proceeding with next entry")
	return null
    }
}

function parse_uniprot_id(sd_string){
    var pts = sd_string.split(":")
    if (pts[0] == "uniprotkb"){
	return pts[1]
    }else{
	return null
    }
}

function parse_gene_names(sd_string){
    try{
	var parts = sd_string.split("|")
	var gene_names = []
	parts.forEach((part)=>{
	    if (part.indexOf("gene name") != -1){
		var gene_name = part.split("(")[0].split(":")[1];
		if (gene_name.indexOf(" ") == -1)
		    gene_names.push(gene_name);
	    }
	})
	return gene_names
    }catch(err){
	//console.log(err)
	return null
    }
}

function parse_method(whitelist,sd_field){
    return sd_field
}

function parse_source_databases(sd_field){
    return sd_field
}

function compute_confidence(sd_field){
    
    if (sd_field){
	var confidence_score = sd_field.split(":")
	var cnf = confidence_score[confidence_score.length-1]
	if (cnf > 0.01){
	    return [cnf,true];
	    
	}else{
	    return [cnf,false];
	}
	
    }else{
	return [null,false];
    }    
}


function get_functional_annotations(sd_field){

    if (sd_field){
	var functions_first = sd_field[22].split("|")
	var functions_second = sd_field[23].split("|")
	var proper_go_first = ((fl)=>{
	    var goterms = [];
	    fl.forEach((item)=>{
		if (item.indexOf("go:\"GO") != -1){
		    goterms.push(item);
		}
	    });

	    return goterms
	})(functions_first)
	
	var proper_go_second = ((fl)=>{
	    var goterms = [];
	    fl.forEach((item)=>{
		if (item.indexOf("go:\"GO") != -1){
		    goterms.push(item);
		}
	    });

	    return goterms
	})(functions_second)

	return [proper_go_first,proper_go_second];
	
    }else{
	return false
    }    
}

function main_parser(filename){

    let lineReader = readline.createInterface({
	input: fs.createReadStream(filename)
    });

    var all_methods = {};
    var all_databases = {};
    var all_items = [];
    lineReader.on('line', (line) => {
	
	var parse = line.split("\t");
	var source_databases = (function(sc_field){
	    return parse_source_databases(sc_field)
	})(parse[12]);

	if (all_databases.hasOwnProperty(source_databases)){
	    all_databases[source_databases]++;
	}else{
	    all_databases[source_databases] = 1;
	}
		
	var interaction_method = (function(whitelist,int_field){
	    return parse_method(whitelist,int_field)
	})(whitelist,parse[6])

	if (all_methods.hasOwnProperty(interaction_method)){
	    all_methods[interaction_method]++;
	}else{
	    all_methods[interaction_method] = 1;
	}
	var species_first = (function(species_field){
	    return parse_species(species_field);
	})(parse[9])

	var species_second = (function(species_field){
	    return parse_species(species_field);
	})(parse[10])

	var uniprot_first = (function(uni_field){
	    return parse_uniprot_id(uni_field);
	})(parse[0])

	var uniprot_second = (function(uni_field){
	    return parse_uniprot_id(uni_field);
	})(parse[1])
	
	var gene_names_first = (function(gene_field){
	    return parse_gene_names(gene_field);
	})(parse[4])
	
	var gene_names_second = (function(gene_field){
	    return parse_gene_names(gene_field);
	})(parse[5])
	
	// get quality tags -- filter by that!
	var confidence = (function(conf_field){
	    return compute_confidence(conf_field);
	})(parse[14])
	
	var get_confidence_pass = confidence[1];
	var confidence_score = confidence[0];

	var annotations = (function(conf_field){
	    return get_functional_annotations(parse);
	})(parse)
	
	
	if (uniprot_first != null && uniprot_second != null && gene_names_first != null && gene_names_second != null && get_confidence_pass){
	    
	    var name_first = gene_names_first[0];
	    var name_second = gene_names_second[0];
	    var first_partner = name_first+"_"+species_first.replace(" ","_")+"_"+uniprot_first
	    var second_partner = name_second+"_"+species_second.replace(" ","_")+"_"+uniprot_second
	    //console.log(first_partner.replace(" ",""),second_partner.replace(" ",""),"TAG_BIN",interaction_method)
	    var gene_first = first_partner.split("_")[0];var gene_second = second_partner.split("_")[0];
	    var uni_first = first_partner.split("_")[first_partner.split("_").length-1];var uni_second = second_partner.split("_")[second_partner.split("_").length-1];
	    
	    annotations[0].forEach((ann)=>{
		var outstring = uni_first+"\t"+species_first.replace(" ","_")+"\t"+"TAG_EXP"+"\t"+ann+"\n";
		fs.appendFileSync(outfolder+"/function_mapping.txt", outstring);
	    });

	    annotations[1].forEach((ann2)=>{
		var outstring = uni_second+"\t"+species_second.replace(" ","_")+"\t"+"TAG_EXP"+"\t"+ann2+"\n";
		fs.appendFileSync(outfolder+"/function_mapping.txt", outstring);
	    });		    
	    
	    console.log([first_partner,second_partner,"TAG_BIN",interaction_method,confidence_score].join("\t"))
	    all_items.push(gene_first);all_items.push(uni_first);all_items.push(gene_second);all_items.push(gene_second);
	
	};
    });
    
    lineReader.on('close',()=>{
	//console.log("finished parsing binary files..");

	// process the databases
	var all_db_string = ""
	for (var key in all_databases){
	    var tmp_string = key+"\t"+all_databases[key]+"\n"
	    all_db_string+=tmp_string
	}

	// process the methods
	var all_md_string = ""
	for (var key in all_methods){
	    var tmp_string = key+"\t"+all_methods[key]+"\n"
	    all_md_string+=tmp_string
	}

	var index_string = all_items.join("\n");
	
	// append database statistics to this resource..
	fs.appendFileSync(outfolder+"/database_list.tsv", all_db_string);
	fs.appendFileSync(outfolder+"/method_list.tsv", all_md_string);
	fs.appendFileSync(outfolder+"/indexdb.txt", index_string);
    })
}

main_parser(fn)
