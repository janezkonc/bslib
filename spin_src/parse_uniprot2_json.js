// this script parses whole uniprot database into individual JSON-s, used on the frontend
// individual entries are saved based on gene symbols.
// tmpIndividualRecords/entry100098.txt
// node parse_uniprot2_json.js testtp53.txt tmp

var fs = require('fs');
var zlib = require('zlib');
var outFolder = process.argv[3];
var out_synonyms = process.argv[4];

if (process.argv[2].indexOf('.gz') != -1){
    var LR = require('readline').createInterface({
	input: fs.createReadStream(process.argv[2]).on('error',()=>{	    
	    console.log("Error occured for file:",process.argv[2])
	}).pipe(zlib.createGunzip())
    });

}else{
    var LR = require('readline').createInterface({
	input: require('fs').createReadStream(process.argv[2])
    });           
}

LR.on('error', function (line) {    
    console.log("Error occured for file:",process.argv[2]);
});

var entryCounter = 0
var idTrigger = false // this is to recognize the beginnings
var writeTrigger = false // this is to write
var currentGeneName = "" // gene name
var currentSpecies = "" // species
var geneDescription = {} // dict of terms
var currentTerm = ""
var pdbDict = {} // dictionary of PDB ligands with corresponding information
var expInt = [];
var uniID = [];
var rxTrigger = false;
var currentExperimentalPartner = "";
var synonyms;
var seqTrigger;
var sequence = [];
var NCBI_species;

LR.on('line', function (line) {
    
    var lParts = line.split(/\s+/)
    if (lParts[0] == "ID"){
	idTrigger = true
	entryCounter++
	if (entryCounter % 1000 == 0)
	    console.log("Parsed: ",entryCounter,"proteins..")	
    }
    
    if (idTrigger == true){

	if (lParts[0] == "AC"){
	    uniID.push(lParts[1].replace(";",""));
	    // lParts.forEach((part)=>{
	    // 	if (part != "AC"){
	    // 	    var px = part.replace(";","")
	    // 	    uniID.push(px)
	    // 	}
	    // })
	}
	
	if (lParts[0] == "OS")
	    currentSpecies = lParts.slice(1,lParts.length).splice(0,2).join("_")

	if(lParts[0] == "GN"){
	    if (lParts[1].split("=")[0] == "Name" || lParts[1].split("=")[0] == "OrderedLocusNames"){
		currentGeneName = lParts[1].split("=")[1]		
		try{currentGeneName = currentGeneName.replace(";","")}catch(err){}
	    }

	    var tmp_parts; var tmp_list = [];
	    try{
	    if (lParts[2].split("=")[0] == "Synonyms"){
		tmp_parts = lParts.splice(2,);
		tmp_parts.forEach(function(x){
		    if (x.indexOf("=") != -1){
			var tmp = x.split("=")[1];
			if (tmp.indexOf(",") != -1)
			    tmp = tmp.replace(",","");
			
			if (tmp.indexOf(";") != -1)
			    tmp = tmp.replace(";","");			
			tmp_list.push(tmp);
		    }else{			
			if (x.indexOf(",") != -1)
			    x = x.replace(",","");
			
			if (x.indexOf(";") != -1)
			    x = x.replace(";","");
			tmp_list.push(x);
		    };
		});
	    };
	    synonyms=tmp_list;
	    }catch(err){}
	};
	
	if (lParts[0] == "CC"){
	    if (lParts[1] == "-!-"){
		currentTerm = ((term_array)=>{
		    if (term_array[2].indexOf(":") != -1){
			return term_array[2].replace(":","")
		    }else{
			return term_array[2]+" "+term_array[3].replace(":","")
		    }		    				   
		})(lParts)
		
		if (geneDescription[currentTerm] == undefined)
		    geneDescription[currentTerm] = ""
		geneDescription[currentTerm] += lParts.slice(3,lParts.length).join(" ")+" "	
	    }else{	    
		geneDescription[currentTerm] += lParts.slice(1,lParts.length).join(" ")+" "
	    }
	}
	

	if (lParts[0] == "DR" && lParts[1] == "PDB;"){


	    // var pdbChain = lParts[2].replace(";","")+lParts[5].replace(";","").split("=")[0]
	    // var method = lParts[3].replace(";","")
	    // var resolution = lParts[4]
	    // var range = lParts[6]
	    // pdbDict[pdbChain] = {'detection_method' : method,
	    // 			 'detection_resolution' : resolution,
	    // 			 'chain_range' : range}
	    
	    var pdb = lParts[2].replace(";","")
	    var method = lParts[3].replace(";","")
	    var resolution = lParts[4]
	    var range = lParts[6]
	    if  (range != undefined){
	    	var chains = ((pdb_range)=>{
	    	    return pdb_range.split("=")[0].split("/")
	    	})(range)
		
	    	chains.forEach((chn)=>{		    
	    	    var pdbChain = pdb+chn		    
	    	    pdbDict[pdbChain] = {'detection_method' : method,
	    				 'detection_resolution' : resolution,
	    				 'chain_range' : range}
	    	})
	    }	    
	}

	if(seqTrigger){
	    sequence.push(lParts)
	    if (lParts.indexOf("//") != -1){
		seqTrigger = false;
	    }
	}
	
	if (lParts[0] == "SQ"){
	    seqTrigger = true;
	}

	if (lParts[0] == "OX"){
	    NCBI_species = lParts[1].split(";")[0].split("=")[1]
	}
	
	if (lParts[0] == "RP" && lParts[1] == "INTERACTION"){

	    rxTrigger = true	    
	    var partner = ""
	    if (lParts[3].indexOf('PHOSPH') != -1){
		partner = lParts[3]+" "+lParts[4].replace(".","")
	    }else{
		partner = lParts[3].replace(".","")
	    }

	    if (partner != ""){
		if(partner.indexOf(";") != -1){
		    partner.replace(";","")
		}

		if(partner.indexOf(",") != -1){
		    partner.replace(",","")
		}		
		currentExperimentalPartner = partner.replace(",","")
	    }
	}

	if (rxTrigger){	    
	    if (lParts[0] == "RX"){		
		expInt.push({'pmid' : lParts[1],
			     'partner_name': currentExperimentalPartner})
		rxTrigger=false
	    }
	}

	if (lParts.indexOf("//") != -1){
	    uniID.forEach((uniprotID)=>{
		var outJson = new Object();
		var global_sequence = ((seq,uni,spe,gen)=>{
		    var seqa = ""
		    seq.forEach((item)=>{
			item.forEach((ex)=>{
			    if (ex.indexOf("//") == -1)
				seqa+=ex
			});
		    });// maybe comparable header file?
		    var header = ">sp"+ "|"+ uni + " " + "OX="+spe + " " + "GN="+gen + "\n";
		    return (header+seqa.trim())		   
		})(sequence,uniprotID,currentSpecies,currentGeneName);

		outJson['sequence'] = global_sequence;
		outJson['gene_name'] = currentGeneName;
		outJson['uniprot_name'] = uniprotID;
		outJson['species'] = NCBI_species+"_"+NCBI_species;
		outJson['synonyms'] = synonyms;
		fs.appendFileSync(out_synonyms, currentGeneName+"\t"+uniprotID+"\n");
		outJson['description'] = ((dict)=>{
		    var fdict = {}
		    for (var k in dict){
			var darray = dict[k].split(" ");
			var narray;
			if (darray[0].indexOf(":") != -1){
			    narray = darray.splice(1,darray.length).join(" ")
			}else{
			    narray = darray.join(" ")
			}
			fdict[k] = narray
		    }
		    return fdict
		})(geneDescription);
		outJson['associated_pdb_chains'] = pdbDict		
		zlib.gzip(JSON.stringify(outJson), function (error, result) {
	    	    if (error){
	    		console.log(error)
	    	    }else{
			try{
	    		    if (currentGeneName.indexOf("/") != -1)
	    			currentGeneName.replace("/","_")
	    		    if (currentGeneName.indexOf(",") != -1)
	    			currentGeneName.replace(",","")
			}catch(err){
			    console.log(err, currentGeneName,uniprotID);
			}
	    		fs.writeFile(outFolder+"/"+uniprotID+".json.gz",result, function(err) {
	    	    	    if(err) {
	    	    		return console.log(err,"random");
	    	    	    };
	    		});
	    	    };
		});		
	    });

	    // reset triggers	    
	    idTrigger = false;
	    uniID=[];
	};
    }; 
});

LR.on('close', function (line) {
    
});
