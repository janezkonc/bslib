## Construct the homolog mappings between different species.
## This is done in the following way..
## map_homolog_edges(source_organism,target_organism)

## for each edge in source organism
## check if partners have homologs in target organism's database
## if yes, consider this a prediction
## do for all.

import gzip
import json
import subprocess
import pandas as pd
import multiprocessing
from collections import defaultdict
import os

def proper_scoring_function(n1,n2):

    if n1 == 0 or n2 == 0:
        return (False,False,False)
    
    else:
        
        ## b'sp|P42644|1.47e-135|92|243|77.778' b'sp|O65272|0.010|30|187|26.203'
        try:
            ## coverage E identity
            
            i1 = float(n1.split("|")[-1][:-2])
            i2 = float(n2.split("|")[-1][:-2])
            score= (i1+i2)/2  
            p1 = n1.split("|")[1]
            p2 = n2.split("|")[1]
            return(p1,p2,"\t".join(n1.split("|"))+"\t"+"\t".join(n2.split("|")))
        
        except:
            
            return(False,False,0)

def return_description(path):

    if os.path.exists(path):    
        with gzip.open(path, "rb") as f:           
            d = json.loads(f.read().decode("ascii"))
        return d
    else:
        return False

def run_blast_query(sequence,species_folder):
    blastCommandfirst = "echo -e \""+sequence.split("\n")[1]+"\""+ " | "+blast_binary+" -db "+species_folder+" -outfmt \"6 sseqid evalue qcovs length pident\" | mawk '{print $1\"|\"$2\"|\"$3\"|\"$4\"|\"$5}' | head -n 1"

    a1 = subprocess.Popen(blastCommandfirst, shell=True, stdout=subprocess.PIPE)
    output_first = str(a1.stdout.read().strip())
    return output_first

def get_homologs(n):
    uni = spindb+"/geneDescriptionJson/"+n.split("_")[-1]+".json.gz"
    d = return_description(uni)
    if d != False:
        if "sequence" in d.keys():
            seq = d['sequence']            
            query = run_blast_query(seq,query_species_folder)
            return(n,query)
    else:
        return (False,False)

def map_to_homolog_predictions(network,query_species,spindb_db,outfile,origin_species,description_json_folder,taxonomy_mapping,blast_binary_info):

    tmap = {}
    
    with open(taxonomy_mapping) as tm:
        for line in tm:
            line = line.strip().split("\t")
            tmap[line[0]] = line[1].replace(" ","_")

    global spindb

    spindb = spindb_db
    global blast_binary

    blast_binary = blast_binary_info
    node_selection = []
    interaction_mapping = defaultdict(list)
    with open(network) as nt:
        for line in nt:
            line = line.strip().split()
            node_first = line[0]
            node_second = line[1]
            interaction_mapping[node_first.split("_")[0]].append((node_second.split("_")[0],line[2]))
            interaction_mapping[node_second.split("_")[0]].append((node_first.split("_")[0],line[2]))
            node_selection.append(node_first)
            node_selection.append(node_second)
                    
    node_homologs = {}
    global query_species_folder
    query_species_folder = spindb+"/species_specific_blast/"+tmap[query_species]+"/sequence_database.fa"

    with multiprocessing.Pool(processes=multiprocessing.cpu_count()) as pool:
        results = pool.map(get_homologs, node_selection)
        
    for x in results:
        try:
            a,b = x
            node_homologs[a] = b
        except Exception as ex:
            print(ex)
            pass
    print("Finished BLASTing..",len(node_homologs))
    
    final_predictions = []
    with open(network) as nt:
        for line in nt:
            line = line.strip().split()
            node_first = line[0]
            node_second = line[1]
            if node_first in node_homologs and node_second in node_homologs:
               
                n1,n2,score = proper_scoring_function(node_homologs[node_first],node_homologs[node_second])
                if n1 != False and n2 != False:

                    ## construct a proper edge from the uniprot entries 
                    n1_description = return_description(description_json_folder+"/"+n1+".json.gz")
                    n2_description = return_description(description_json_folder+"/"+n2+".json.gz")
                    try:
                      
                        n1_species = n1_description['species']
                        n2_species = n2_description['species']
                    
                        n1_gene = n1_description['gene_name']
                        n2_gene = n2_description['gene_name']

                        n1 = n1_gene+"_"+n1_species+"_"+n1
                        n2 = n2_gene+"_"+n2_species+"_"+n2
                        
                        tag_name = "TAG_BLAST"
                        for partner in interaction_mapping[n1]:
                            print("MXXXXX",partner[0],n2_gene,partner)
                            if partner[0] == n2_gene:
                                tag_name = partner[1]

                        #with open(outfile,"w+") as of:
                        print("INTERACTION\t{}".format("\t".join([n1,n2,str(score)+"\t"+node_first+"\t"+node_second+"\t"+tag_name])))

                            # of.write("\t".join([n1,n2,str(score)])+"\n")
                    except Exception as e:
                        print(e)
                        pass
                        
if __name__ == "__main__":

    # python3 prepare_datasets.py --query_network ~/data/spin/whole_proteome_graph/reduced_proteome.edgelist --spin_database ~/data/spin
    
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("--query_network",default="reduced_network.txt")
    parser.add_argument("--query_species",default="3702")
    parser.add_argument("--spin_database",default="./spin")
    parser.add_argument("--out_predictions",default="../results/predictions.txt")
    parser.add_argument("--mean_iden",default="../results/predictions.txt")
    parser.add_argument("--description_folder",default="")
    parser.add_argument("--origin_species",default="9606")
    parser.add_argument("--blast_bin",default="./bin/blastp")
    parser.add_argument("--taxonomy_mapping",default="/servers/insilab.org/files/spin/dbindex/taxonomy_map.tsv")
    args = parser.parse_args()
    
    ## obtain predictions for all pairs..
    map_to_homolog_predictions(args.query_network,args.query_species,args.spin_database,args.out_predictions,args.origin_species,args.description_folder,args.taxonomy_mapping,args.blast_bin)
