#!/bin/bash

FINAL_TSV=$1
taxid=$(echo "${FINAL_TSV}"|sed -r "s,.*/([^_]+)_final.tsv$,\1,")

# heterodimers -> not in string (or missing) -> predicted (TAG_PRL, TAG_PRL+TAG_BIN, TAG_PRL_EXP+TAG_BIN) -> unique
head -1 ${FINAL_TSV} > ${HOMO_PAPER}/${taxid}_hetero_not_in_string_predicted.tsv && tail -n +2 ${FINAL_TSV} |awk 'BEGIN{ FS=OFS="\t" }{if (!($1 ~ $9)) print $0}'|grep -v STRING_YES|grep TAG_PRL|sort -u -t$'\t' -k1,1 -k9,9 -k17,17 >> ${HOMO_PAPER}/${taxid}_hetero_not_in_string_predicted.tsv

# heterodimers -> not in string (or missing) -> binary (TAG_BIN) -> unique
head -1 ${FINAL_TSV} > ${HOMO_PAPER}/${taxid}_hetero_not_in_string_binary.tsv && tail -n +2 ${FINAL_TSV} |awk 'BEGIN{ FS=OFS="\t" }{if (!($1 ~ $9)) print $0}'|grep -v STRING_YES|grep TAG_BIN|grep -v TAG_PRL|sort -u -t$'\t' -k1,1 -k9,9 -k17,17 >> ${HOMO_PAPER}/${taxid}_hetero_not_in_string_binary.tsv

# heterodimers -> in string -> predicted (TAG_PRL, TAG_PRL+TAG_BIN, TAG_PRL_EXP+TAG_BIN) -> unique
head -1 ${FINAL_TSV} > ${HOMO_PAPER}/${taxid}_hetero_in_string_predicted.tsv && tail -n +2 ${FINAL_TSV} |awk 'BEGIN{ FS=OFS="\t" }{if (!($1 ~ $9)) print $0}'|grep STRING_YES|grep TAG_PRL|sort -u -t$'\t' -k1,1 -k9,9 -k17,17 >> ${HOMO_PAPER}/${taxid}_hetero_in_string_predicted.tsv

# heterodimers -> in string -> binary (TAG_BIN) -> unique
head -1 ${FINAL_TSV} > ${HOMO_PAPER}/${taxid}_hetero_in_string_binary.tsv && tail -n +2 ${FINAL_TSV} |awk 'BEGIN{ FS=OFS="\t" }{if (!($1 ~ $9)) print $0}'|grep STRING_YES|grep TAG_BIN|grep -v TAG_PRL|sort -u -t$'\t' -k1,1 -k9,9 -k17,17 >> ${HOMO_PAPER}/${taxid}_hetero_in_string_binary.tsv

# homodimers -> not in string (or missing) -> predicted (TAG_PRL, TAG_PRL+TAG_BIN, TAG_PRL_EXP+TAG_BIN) -> unique
head -1 ${FINAL_TSV} > ${HOMO_PAPER}/${taxid}_homo_not_in_string_predicted.tsv && tail -n +2 ${FINAL_TSV} |awk 'BEGIN{ FS=OFS="\t" }{if ($1 ~ $9) print $0}'|grep -v STRING_YES|grep TAG_PRL|sort -u -t$'\t' -k1,1 -k9,9 -k17,17 >> ${HOMO_PAPER}/${taxid}_homo_not_in_string_predicted.tsv

# homodimers -> not in string (or missing) -> binary (TAG_BIN) -> unique
head -1 ${FINAL_TSV} > ${HOMO_PAPER}/${taxid}_homo_not_in_string_binary.tsv && tail -n +2 ${FINAL_TSV} |awk 'BEGIN{ FS=OFS="\t" }{if ($1 ~ $9) print $0}'|grep -v STRING_YES|grep TAG_BIN|grep -v TAG_PRL|sort -u -t$'\t' -k1,1 -k9,9 -k17,17 >> ${HOMO_PAPER}/${taxid}_homo_not_in_string_binary.tsv

# homodimers -> in string -> predicted (TAG_PRL, TAG_PRL+TAG_BIN, TAG_PRL_EXP+TAG_BIN) -> unique
head -1 ${FINAL_TSV} > ${HOMO_PAPER}/${taxid}_homo_in_string_predicted.tsv && tail -n +2 ${FINAL_TSV} |awk 'BEGIN{ FS=OFS="\t" }{if ($1 ~ $9) print $0}'|grep STRING_YES|grep TAG_PRL|sort -u -t$'\t' -k1,1 -k9,9 -k17,17 >> ${HOMO_PAPER}/${taxid}_homo_in_string_predicted.tsv

# homodimers -> in string -> binary (TAG_BIN) -> unique
head -1 ${FINAL_TSV} > ${HOMO_PAPER}/${taxid}_homo_in_string_binary.tsv && tail -n +2 ${FINAL_TSV} |awk 'BEGIN{ FS=OFS="\t" }{if ($1 ~ $9) print $0}'|grep STRING_YES|grep TAG_BIN|grep -v TAG_PRL|sort -u -t$'\t' -k1,1 -k9,9 -k17,17 >> ${HOMO_PAPER}/${taxid}_homo_in_string_binary.tsv
