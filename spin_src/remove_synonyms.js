// this script reads the network and removes synonyms based on a mapping.
var fs = require('fs');
var zlib = require('zlib');
var network = process.argv[3]
var redundant = JSON.parse(fs.readFileSync(process.argv[4], 'utf8'));

var synonym_genemap = ((fname)=>{
    gmap = {}
    var text = fs.readFileSync(fname,'utf8').split("\n")
    text.forEach((line)=>{
	var parts = line.split("\t");
	if (parts[0] != "")
	    gmap[parts[1]] = parts[0];
    });
    return gmap
})(process.argv[5])

function filter_the_network(network_file,mapping,callback){
    
    if (network_file.indexOf('.gz') != -1){
	var LR2 = require('readline').createInterface({
	    input: fs.createReadStream(network_file).on('error',()=>{	    
		console.log("Error occured for file:",network_file)
	    }).pipe(zlib.createGunzip())
	});

    }else{
	var LR2 = require('readline').createInterface({
	    input: require('fs').createReadStream(network_file)
	});           
    }

    LR2.on('error', function (line) {    
	console.log("Error occured for file:",network_file)
    })

    LR2.on('line', function (line) {
	var parts = line.trim().split("\t");

	if (parts.length > 2){

	    var partner_first = parts[0].split("_");
	    var partner_second = parts[1].split("_");
	    
	    var tag = parts[2];
	    
	    var gene_first = partner_first[0];
	    var gene_second = partner_second[0];
	    
	    var uni_first = partner_first[3];
	    var uni_second = partner_second[3];
	    	    
	    var geneSpeciesFirst = partner_first.slice(0,3).join("_");
	    var geneSpeciesSecond = partner_second.slice(0,3).join("_");
	    
	    var first_gene_mapped = mapping[geneSpeciesFirst] || gene_first;
	    var second_gene_mapped = mapping[geneSpeciesSecond] || gene_second;

	    if (uni_first == redundant[geneSpeciesFirst] && uni_second == redundant[geneSpeciesSecond]){		
		partner_first[0] = synonym_genemap[uni_first] || gene_first;
		partner_second[0] = synonym_genemap[uni_second] || gene_second;
		
		// if genespecies[uniprot] == gene
		var final_first = partner_first.join("_");
		var final_second = partner_second.join("_");
		
		parts[0] = final_first;
		parts[1] = final_second;		
		console.log(parts.join("\t"));

	    };
	};
    });

    LR2.on('close', function (line) {	
	
    });    
}

if (process.argv[2].indexOf('.gz') != -1){
    var LR = require('readline').createInterface({
	input: fs.createReadStream(process.argv[2]).on('error',()=>{	    
	    console.log("Error occured for file:",process.argv[2])
	}).pipe(zlib.createGunzip())
    });

}else{
    var LR = require('readline').createInterface({
	input: require('fs').createReadStream(process.argv[2])
    });           
}


LR.on('error', function (line) {    
    console.log("Error occured for file:",process.argv[2])
})


var mappings = {}
LR.on('line', function (line) {
    var parts = line.trim().split(" ");
    if (parts.length > 1){
	mappings[parts[1]+"_"+parts[0]] = parts[1];
    }
});

LR.on('close', function (line) {   
    
    filter_the_network(network,mappings);

});
