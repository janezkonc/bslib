#!/usr/bin/python

import sys
import networkx as nx
import json
import gzip

def read_and_parse_proteome(infile,outfolder):

    final= []
    name_to_int = {}
    int_counter = 0
    
    G = nx.read_edgelist(infile)
    G.remove_edges_from(G.selfloop_edges())
    print(nx.info(G))

    ## compute layout for the whole graph!
    ## G = G.subgraph(sorted(nx.connected_components(G), key = len, reverse=True)[3])

    pos = nx.spectral_layout(G)
        
    print("formating nodes")
    for node in pos:
        if node in name_to_int:
            node_int = name_to_int[node]
        else:
            node_int = int_counter
            name_to_int[node] = int_counter
            int_counter += 1
        final.append({
            "group": "nodes",
            "data": {
                "id": str(node_int),
                "label": node
            },
            "position": {"x": float(pos[node][0]), "y": float(pos[node][1])}
        })

    print("formating edges")
    for edge in G.edges():
        int1 = name_to_int[edge[0]]
        int2 = name_to_int[edge[1]]
        final.append({
            "group": "edges",
            "data": {
                "id": "e"+str(int1)+"_"+str(int2),
                "source": str(int1),
                "target": str(int2)
            }
        });
    
    print("saving file {}".format(outfolder))

    with gzip.GzipFile(outfolder, 'w') as fout:            
        json_str = json.dumps(final)
        json_bytes = json_str.encode('utf-8')
        fout.write(json_bytes)

if __name__ == "__main__":

    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("--out_graph_json",default="../whole_proteome_graph/proteome_with_layout.json.gz")
    parser.add_argument("--graph_mat",default="../whole_proteome_graph/proteome.edgelist")
    args = parser.parse_args()
    read_and_parse_proteome(args.graph_mat, args.out_graph_json)
