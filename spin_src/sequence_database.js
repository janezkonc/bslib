// generate the sequence database form UniProt files..

// this script parses whole uniprot database into individual JSON-s, used on the frontend
// individual entries are saved based on gene symbols.
// tmpIndividualRecords/entry100098.txt
// node parse_uniprot2_json.js testtp53.txt tmp

var fs = require('fs');
var zlib = require('zlib');
var outFolder = process.argv[3]

var taxonomy_mapping = (function(x){
    var taxMap = {}
    var mappings_lines = fs.readFileSync(x).toString().split("\n");
    mappings_lines.forEach((ex)=>{
	var pts = ex.split("\t")
	if(!taxMap.hasOwnProperty(pts[0]))
	    taxMap[pts[0]] = pts[1];
    });
    return taxMap
})(process.argv[4]);

if (process.argv[2].indexOf('.gz') != -1){
    var LR = require('readline').createInterface({
	input: fs.createReadStream(process.argv[2]).on('error',()=>{	    
	    console.log("Error occured for file:",process.argv[2])
	}).pipe(zlib.createGunzip())
    });

}else{
    var LR = require('readline').createInterface({
	input: require('fs').createReadStream(process.argv[2])
    });           
}

LR.on('error', function (line) {    
    console.log("Error occured for file:",process.argv[2]);
});

var entry = "";
LR.on('line', function (line) {   
    entry+=line;
});

LR.on('close', function (line) {

    parsed_entry = JSON.parse(entry);
    var spec = parsed_entry.species.split("_")[0]
    try{
	var of = outFolder+"/"+taxonomy_mapping[spec].replaceAll(" ","_")
	// also- append sequences from other organisms here, I guess
	
	
	if (!fs.existsSync(of)){
    	    fs.mkdirSync(of);
	}

	fs.appendFileSync(of+"/"+"sequence_database.fa", parsed_entry.sequence+"\n");
    }catch(err){}

});

String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};
