## visualize graph in py

import matplotlib
matplotlib.use('Agg')

import gzip
import matplotlib.pyplot as plt
import networkx as nx
import json
import numpy as np
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("--infile",default="None")
parser.add_argument("--outfile",default="None")
args = parser.parse_args()

graph = nx.Graph()
nodes_positions = {}
with gzip.open(args.infile) as pih:
        d = json.loads(pih.read().decode("ascii"))
        for x in d:
            
            if x['group'] == "nodes":
                position = x['position']
                try:
                        id_n = x['data']['id']
                        nodes_positions[id_n] = np.array([position['x'],position['y']])
                except:
                        print(x['data'])
                
            if x['group'] == "edges":
                graph.add_edge(x['data']['source'],x['data']['target'])                
                
print(nx.info(graph))
print("drawing..")

nx.draw(graph,nodes_positions,node_size=0.005,node_color="green",width=0.05,alpha=0.5,font_size=0)
plt.xlim(0,1)
plt.ylim(0,1)
plt.gca().invert_yaxis()
outfile_prot = args.outfile
print(outfile_prot)
plt.savefig(outfile_prot)
