## this perl script transforms the uniprot into many single files..

## define vars
my $delimiter = "//";
my $currentEntry = "";
my $lineCounter = 0;

## read from input
while (<STDIN>){

    my @tokens = split /\s+/, $_;

    if ($tokens[0] ne "//"){
	$currentEntry .= $_;
    }
    
    if ($tokens[0] eq "//"){
    	if (index($_, $delimiter) != -1) {
	    if ($lineCounter % 1000 == 0){
		print "Records processed:".$lineCounter."\n";
	    }
    	    $lineCounter++;
    	    $currentEntry .= "//"."\n";
    	    my $filename = "entry".$lineCounter.".txt";
    	    open(my $fh, '>', $filename) or die "Could not open file '$filename' $!";
    	    print $fh $currentEntry;
    	    close $fh;	
    	    $currentEntry = "";
    	}
    }else{
    }
}

