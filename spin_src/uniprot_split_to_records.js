//this script splits the whole uniprot database into separate, gzipped records which can be processed in parallel further on.

var fs = require('fs');
var zlib = require('zlib');
var outFolder = process.argv[3]
var LR = require('readline').createInterface({
    input: fs.createReadStream(process.argv[2]).on('error',()=>{	    
	console.log("Error occured for file:",process.argv[2])
    }).pipe(zlib.createGunzip())
});

LR.on('error', function (line) {
    
    console.log("Error occured for file:",process.argv[2])
    
})

var currentString = ""
var counter = 0

LR.on('line', function (line) {
    counter++
    var lParts = line.split(/\s+/)
    if (lParts[0] == "//"){	
	zlib.gzip(currentString, function (error, result) {
	    if (error){
		console.log(error)
	    }else{
		fs.writeFile(outFolder+"/record"+counter,result, function(err) {
		    if(err) {
			return console.log(err);
		    }
		    currentString = ""
		});
	    }
	})	
    }else{
	currentString +=line+"\n"
    }
})

LR.on('close', function (line) {	
    console.log("Finished splitting the file..")    
});
