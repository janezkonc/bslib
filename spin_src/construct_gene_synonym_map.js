// synonymous genes --- redundancy filter
// read the json.gz file. Get the synonyms key
// write this to indexdb folder
// REPRESENTATIVE SYNONYM style

var fs = require('fs');
var zlib = require('zlib');
var outFolder = process.argv[3]

if (process.argv[2].indexOf('.gz') != -1){
    var LR = require('readline').createInterface({
	input: fs.createReadStream(process.argv[2]).on('error',()=>{	    
	    console.log("Error occured for file:",process.argv[2])
	}).pipe(zlib.createGunzip())
    });

}else{
    var LR = require('readline').createInterface({
	input: require('fs').createReadStream(process.argv[2])
    });           
}


LR.on('error', function (line) {    
    console.log("Error occured for file:",process.argv[2])
})

var body = "";
LR.on('line', function (line) {
    body+=line;     
});

LR.on('close', function (line) {	
    var doc = JSON.parse(body);
    var uniprot = process.argv[2].split("/").splice(-1)[0].split(".")[0]
    
    if (doc.gene_name == "" || doc.gene_name == undefined){
	console.log(uniprot,"Nan",doc.species);
    }else{
	console.log(uniprot,doc.gene_name,doc.species);
    }
});
