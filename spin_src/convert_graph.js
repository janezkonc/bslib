var exec = require('child_process').exec;
var fs = require('fs');
var zlib = require('zlib');
var async = require('async');
var readline = require('readline');
var cytoscape = require('cytoscape');

var argv = require('minimist')(process.argv.slice(2));
var graphPath = argv.input_edgelist;
var geneDescriptionJson = argv.gene_info_folder
var taxonomy = argv.tax_mappings;
var writeToFile = argv.output_edgelist;
var species_blast_folder = argv.species_blast
var tax_list = argv.tax_list;

var global_network = cytoscape();
var subgraph = cytoscape();
var tax_dictionary = {};
var nid = {};

var filter_tax = []//[3702]//[9606,3702,559292,6239,7227,10090,4081,10116]

load_graph_to_RAM(graphPath,()=>{

    console.log("Whole graph loaded into RAM.");

    get_tax_list();

    var LR = require('readline').createInterface({
        input: require('fs').createReadStream(taxonomy)
    });
		  
    LR.on('error', function (line) {
        console.log("Error occured for file:",taxonomy)
	
    })

    LR.on('line', function (line) {
	line = line.split("\t");
	if (!(tax_dictionary.hasOwnProperty(line[0])))
            tax_dictionary[line[0]] = line[1];
    })
    
    LR.on('close', function (line) {

	var jobs = [];
	for (var i=0; i<filter_tax.length; i++) {
	    var tax = filter_tax[i];
	    jobs.push(tax);
	}

	async.mapSeries(jobs,homolog_species,(e,r)=> {
	    if (e)
		console.log(e)
	    else
		console.log("finished job for species "+r);
	});
    });

    
});

function get_tax_list() {
    var LR = require('readline').createInterface({
        input: require('fs').createReadStream(tax_list)
    });

    LR.on('error', function (line) {
        console.log("Error occured for file:",tax_list)

    })

    LR.on('line', function (line) {
        filter_tax.push(line.trim());
    })

    LR.on('close', function (line) {

        console.log("The following homologs will be found: "+filter_tax);
    });

}

function homolog_species(tax,cb) {
    console.log("started job for species "+tax);
    nid[tax] = global_network.nodes().length;
    var nodes = global_network.nodes("[label @*='_"+tax+"_"+tax+"_']")
    
    subgraph.elements().remove();
    console.log("initialize subgraph");
    subgraph.add(nodes.closedNeighborhood());
    console.log("subgraph for "+tax+" has "+subgraph.nodes().length+" nodes and "+subgraph.edges().length+" edges");

    var nodes = subgraph.nodes("[label !@*='_"+tax+"_"+tax+"_']")
    console.log(nodes.length);

    // containers                                                                                                                                                                                              
    var jobs = []
    // this adds the jobs for the first recursive step                                                                                                                                                         
    for (var i=0; i<nodes.length; i++) {
        var label = nodes[i].data().label;
        var uniprot = label.substring(label.lastIndexOf("_")+1);
        jobs.push({tax: tax,uniprot: uniprot, id: nodes[i].id()});
    }

    counter = jobs.length;
    // reduce bresi?                                                                                                                                                                                           
    async.map(jobs,get_homologs,(e,r)=>{

        console.log("Nodes homology complete.");

	//console.log("no of TAG_HOMO "+subgraph.edges(".TAG_HOMO").length);

        var edgecounter = r.length;
        for (var i=0; i<r.length; i++) {
            if (r[i] && r[i].length > 0) {
                revireNewNode(tax,r[i][0], function(origNode) {
                    origNode.remove();
                    edgecounter--;

                    if (edgecounter == 0)
                        save_graph(tax,function(saved) {
			    if (saved)
				cb(null,tax);
			});
                })
            }
            else {
                edgecounter--;

                if (edgecounter == 0) {
                    save_graph(tax,function(saved) {
			if (saved) {
			    cb(null,tax);
			} 
		    });
		}
            }
        }
    });
}

function save_graph(tax,cb) {
    var stream = fs.createWriteStream(writeToFile+"/"+tax+".edgelist");
    stream.once('open', function(fd) {

	var edges = subgraph.edges();
	for (var i=0; i<edges.length; i++) {
	    var edge = edges[i];
	    var source = edge.source().data().label;
	    var target = edge.target().data().label

	    if (source != target) {

		if (edge.hasClass("TAG_BIN")) {
		    stream.write(source+" "+target+" TAG_BIN\n");
		}
		if (edge.hasClass("TAG_PRL")) {
		    stream.write(source+" "+target+" TAG_PRL\n");
		}
		if (edge.hasClass("TAG_HOMO")) {
		    stream.write(source+" "+target+" TAG_HOMO\n");
		}
	    }
	}
	stream.end();
	cb(true)
    });
}

function revireNewNode(tax,newNodeData,cb) {
    
    var origNode = subgraph.$("#"+newNodeData.fromNodeID);
    var label = newNodeData.name+"_"+tax+"_"+tax+"_"+newNodeData.uniprot;
    var oldLabel = origNode.data().label;

    var outgoers = origNode.outgoers("edge");
    var incomers = origNode.incomers("edge");
    
    var edgesToAdd = [];

    for (var i=0; i<outgoers.length; i++) {
        var edge = outgoers[i];
	var edgejson = edge.json();
        edgejson.data.id = newNodeData.label+"_"+edge.target().data().label;
        edgejson.data.source = newNodeData.id;
	edgejson.classes+=" TAG_HOMO";
	//console.log("revire outgoer",edgejson);
	edgesToAdd.push(edgejson);
    }
    for (var i=0; i<incomers.length; i++) {
        var edge = incomers[i];
	var edgejson = edge.json();
        edgejson.data.id = edge.source().data().label+"_"+newNodeData.label;
        edgejson.data.target = newNodeData.id;
	edgejson.classes+=" TAG_HOMO";
	//console.log("revire incomer",edgejson)
	edgesToAdd.push(edgejson);
    }
    subgraph.add(edgesToAdd);
    //console.log("subgraph for "+tax+" has "+subgraph.elements().length+" elements");

    cb(origNode);
}



function load_graph_to_RAM(pathToGraph,cb){

    var nodeToId = {};
    var edgeToIndex = {};
    var elementsToAdd = [];

    var idcounter = 0;
    var edgecounter = -1;
    if (pathToGraph.indexOf('.gz') != -1){
	var LR = require('readline').createInterface({
	    input: fs.createReadStream(pathToGraph).on('error',()=>{	    
		console.log("Error occured for file:",pathToGraph)
	    }).pipe(zlib.createGunzip())
	});
	
    }else{
	var LR = require('readline').createInterface({
	    input: require('fs').createReadStream(pathToGraph).on('error',()=>{
                console.log("Error occured for file:",pathToGraph)})
	});           
    }
    
    LR.on('error', function (line) {    
	console.log("Error occured for file:",pathToGraph)
	
    })
    var finalJson = ""
    LR.on('line', function (line) {
	line = line.split(" ").filter(Boolean);
	var classtag = line[2];
	var nodeB = line[1];
	var nodeA = line[0];

	var counterA = nodeToId[nodeA];
	if (counterA == undefined) {
	    //console.log(nodeA);
	    var counterA = idcounter++;
	    elementsToAdd.push({
		group: "nodes",
		data: {
		    id: counterA,
		    label: nodeA
		}
	    });
	    nodeToId[nodeA] = counterA;
	    edgecounter++;
	}

	var counterB = nodeToId[nodeB];
        if (counterB == undefined) {
	    //console.log(nodeB);
            var counterB = idcounter++;
            elementsToAdd.push({
                group: "nodes",
                data: {
                    id: counterB,
                    label: nodeB
                }
            });
	    nodeToId[nodeB] = counterB;
	    edgecounter++;
        }

	var edgeidAB = "e_"+nodeA+"_"+nodeB;
	var edgeidBA = "e_"+nodeB+"_"+nodeA;

	var counterAB = edgeToIndex[edgeidAB];
	var counterBA = edgeToIndex[edgeidBA];

	if (counterAB) {
	    elementsToAdd[counterAB].classes+=classtag;
	}
	else if (counterBA) {
	    elementsToAdd[counterBA].classes+=classtag;
	}
	else {
	    //console.log(edgeidAB);
	    counterAB = edgecounter++;
	    elementsToAdd.push({
		group: "edges",
		classes: classtag,
		data: {
		    id: edgeidAB,
		    source: counterA,
		    target: counterB
		}
	    });
	    edgeToIndex[edgeidAB] = counterAB;
	}
    })

    LR.on('close', function (line) {
	global_network.add(elementsToAdd);
	console.log(global_network.nodes().length,global_network.edges().length);
	cb();
    });
}

function get_homologs(ele,cb){
    var uniprot = ele.uniprot;
    var nodeID = ele.id;
    var species = tax_dictionary[ele.tax].replace(/"/g,"").replace(/ /g,"_");

    var node = subgraph.$("#"+nodeID);

    // get the source_node's species -> use that in blast
    // go to that folder, run a blast search with neigh's sequence and pick top result
    // return top result as uniprot ID.
    var target_uniprot = geneDescriptionJson+uniprot+".json.gz"
    var source_species = null
    read_json_uni(target_uniprot,(info)=>{
	//console.log(counter--);
    	var seq = info.sequence;
	
    	if (seq) {
	    var blastCommand = "echo -e \""+seq.split("\n")[1]+"\""+ " | algorithms/blastp -db "+species_blast_folder
                +species+"/sequence_database.fa -outfmt \"6 sseqid evalue qcovs length pident stitle\" | head -n 1"
	    var exec = require('child_process').exec;
	    var child = exec(blastCommand,(error, stdout, stderr) => {
	    	if (error !== null) {
		    //console.log(error)
		    node.remove();
		    //console.log(counter--);
	    	    cb(null,false)
	    	}
		else {
	    	    var hit = stdout.split("\n").filter(Boolean)[0];
	    	    var counter_final = 0;
		    var nodesToAdd = [];
	    	    var homolog_info = []
	    	    if (hit) {
	    		hit = hit.split("\t");
			
			var uniprot = hit[0].split("|")[1]
			
			var fasta_header = hit[5]
			var gene_name = (fasta_header) ? fasta_header.split(" ")[2].split("=")[1] : "";
			
	    		var id = subgraph.nodes("[label $= '"+uniprot+"']")
	    		id = (id.length > 0) ? id[0].id() : undefined
			var label = gene_name+"_"+ele.tax+"_"+ele.tax+"_"+uniprot;
			
			if (id) {
			    var existingEdges = subgraph.$("#"+id).openNeighborhood("edge");
			    var outgoers = node.outgoers("edge");
			    var incomers = node.incomers("edge");
			    
			    for (var i=0; i<outgoers.length; i++) {
				var edge = outgoers[i];
				if (existingEdges.filter("[id = '"+(label+"_"+edge.target().data().label).replace(/"/g,'\\"').replace(/'/g,'\\"')+"'],[id = '"+(edge.target().data().label+"_"+label).replace(/"/g,'\\"').replace(/'/g,'\\"')+"']").length == 0) {
				    var edgejson = edge.json();
				    edgejson.data.id = label+"_"+edge.target().data().label;
				    edgejson.data.source = id;
				    edgejson.classes+=" TAG_HOMO";
				    //console.log("homo outgoer",edgejson);
				    nodesToAdd.push(edgejson);
				}
			    }
			    for (var i=0; i<incomers.length; i++) {
				var edge = incomers[i];
				if (existingEdges.filter("[id = '"+(label+"_"+edge.target().data().label).replace(/"/g,'\\"').replace(/'/g,'\\"')+"'],[id = '"+(edge.target().data().label+"_"+label).replace(/"/g,"\\'").replace(/'/g,"\\'")+"']").length == 0) {
				    var edgejson = edge.json();
				    edgejson.data.id = edge.source().data().label+"_"+label;
				    edgejson.data.target = id;
				    edgejson.classes+=" TAG_HOMO";
				    //console.log("home incomer",edgejson);
				    nodesToAdd.push(edgejson);
				}
			    }
			    node.remove();
			    
			}
			else {
			    var id = nid[ele.tax]++;
			    var newNode = {
				group: "nodes",
				data: {
				    id: id,
				    label: label,
				    weight: 0.2,
				    disease: ""
				},
				position: {
				    x: 0,
				    y: 0
				}
			    }
			    nodesToAdd.push(newNode);
			    
	    		    homolog_info.push({label: label, id: id, fromNodeID: nodeID});
	    		}
			//console.log("add "+nodesToAdd.length+" elements",nodesToAdd);                                                                                                                            
			subgraph.add(nodesToAdd);
			//console.log("subgraph for "+ele.tax+" has "+subgraph.elements().length+" elements");                                                                                                     
			//console.log(counter--);                                                                                                                                                                  
			cb(null,homolog_info);
			
		    }
		    else {
			node.remove();
			cb(null,false);
		    }
		}
	    });	
	}
	else {
	    node.remove();
	    //console.log(counter--);
	    cb(null,false);
	}
    });
}

function read_json_uni(name,cb){

	if (name.indexOf('.gz') != -1){
		var LR = require('readline').createInterface({
			input: fs.createReadStream(name).on('error',()=>{
			    console.log("Error occured for file:",name);
			    cb({});
			}).pipe(zlib.createGunzip())
		});

	}else{
		var LR = require('readline').createInterface({
			input: require('fs').createReadStream(name).on("error",()=>{
			    console.log("Error occured for file:",name);
			    cb({});
			})
		});           
	}

	LR.on('error', function (line) {
	    console.log("Error occured for file:",name);
	    cb({});
	});

	var entry = "";
	LR.on('line', function (line) {   
		entry+=line;
	});

	LR.on('close', function (line) {
		cb(JSON.parse(entry));
	});
}
