// this javascript file goes through all tsv entries and maps pdb-pdb interactions, which are then mapped to gene-gene interactions..

var fs = require('fs');
var zlib = require('zlib');

if (process.argv[3] == "--pdb_pdb"){
    output_pdb_edges()
}else if (process.argv[3] == "--gene_gene"){
    output_gene_edges()
}else if (process.argv[3] == "--gene_tofiles"){
    map_ggraph_files()
}

function map_ggraph_files(){

    var lineReader = require('readline').createInterface({
	input: fs.createReadStream(process.argv[2]).on('error',()=>{	    
	    console.log("Error occured for file:",process.argv[2])
	})
    });

    lineReader.on('error', function (line) {
	console.log("Error occured for file:",process.argv[2])
    })

    lineReader.on('line', function (line) {

	//write all the necessary information into target specific files, which are to be transformed to a json
	
	var parts = line.split(/\s+/)
	var source = parts[0]
	var target = parts[1]

	if (source != "" && target != ""){
	    var filename1 = "../gene_graph_individual/"+source+".txt"
	    var filename2 = "../gene_graph_individual/"+target+".txt"
	    try{
	        fs.appendFileSync(filename1, target+"\n");
	    }catch(err){
	    }

	    try{
	        fs.appendFileSync(filename2, source+"\n");
	    }catch(err){
	    }	 
	}
    })		 		 
}

function output_gene_edges(infile){

    var lineReader = require('readline').createInterface({
	input: fs.createReadStream(process.argv[2]).on('error',()=>{	    
	    console.log("Error occured for file:",process.argv[2])
	}).pipe(zlib.createGunzip())
    });
    
    lineReader.on('error', function (line) {	
	console.log("Error occured for file:",process.argv[2])
    })

    var ligs = []
    var main_gene = ""
    var organism = ""
    var main_uniprot = ""
    var lcount = 0
    
    lineReader.on('line', function (line) {
	lcount++

	//tukaj dodaj pdb 
	var parts = line.split("\t")
	var pdb_receptor =parts[17]+parts[15] //*
	var ligand_string = parts[6] //*
	if(lcount == 50){
	    main_uniprot = parts[20]
	    main_gene = parts[21]
	    organism = parts[22]
	}
	
	var interaction_ligand_type = parts[10]	//*
	var ligand_receptor = "" //*
	var writeTrigger = 0 // *
	
	if (interaction_ligand_type == "protein"){
	    ligand_receptor = ligand_string.split("_").slice(2,4).join("")
	    writeTrigger = 1
	}
	
	if (writeTrigger){
	    if (ligs.indexOf(ligand_receptor) == -1){
		ligs.push(ligand_receptor)
	    }
	}
    })

    lineReader.on('close', function (line) {
	ligs.forEach((ex)=>{
	    var partners = []
	    get_gene(ex,(partner)=>{
		
		var parSpec = partner[1]
		var partnerN = partner[0]
		var parUni = partner[2]
		if (partnerN != "-" && partnerN != undefined && partnerN != main_gene && main_gene != "-"){
		    var p1 = main_gene+"_"+organism.split(" ").splice(0,2).join("_")+"_"+main_uniprot
		    var p2 = partnerN+"_"+parSpec.split(" ").splice(0,2).join("_")+"_"+parUni
		    console.log(p1,p2)
//		    console.log(main_gene+"\t"+partner+"\t"+organism.split(" ").splice(0,2).join("_")+"\t"+parSpec.split(" ").splice(0,2).join("_"))
		}		
	    })
	})
    });
}


function get_gene(pdb_name,gname){

    var lineReader = require('readline').createInterface({
	input: fs.createReadStream("../genprobisdb/tsv/"+pdb_name+".tsv.gz").on('error',()=>{   
	    //callback(null,"NODATA")
	}).pipe(zlib.createGunzip())	  
    });
    
    lineReader.on('error', function (line) {	
//	callback(null,"NODATA")
    })

    var ligs = []
    var lcount = 0
    var main_gene = ""
    var main_species = ""
    var main_uniprot = ""
    
    lineReader.on('line', function (line) {
	lcount++
	var parts = line.split("\t")	
	//this is a valid line already..
	if (lcount  <  50 && lcount > 48){
	    main_gene = parts[21]
	    main_species = parts[22]
	    main_uniprot = parts[20]
	    lineReader.close([main_gene,main_species,main_uniprot])
	    gname([main_gene,main_species,main_uniprot])
	}	
    })
}

function output_pdb_edges(infile){

    var lineReader = require('readline').createInterface({
	input: fs.createReadStream(process.argv[2]).on('error',()=>{	    
	    callback(null,"NODATA")
	}).pipe(zlib.createGunzip())	  
    });

    var tmpdict = {
	'receptor_id' : "",
	'receptor_properties' : {},
	'species' : "",
	'ligands' : {},
	'second_layer' : {}
    }

    var processed_ligands = []    
    
    lineReader.on('error', function (line) {
	
	callback(null,"NODATA")
    })

    var ligs = []
    
    lineReader.on('line', function (line) {

	parts = line.split("\t")
	//definitions
	
	var pdb_receptor =parts[17]+parts[15] //*
	var ligand_string = parts[6] //*
	var interaction_ligand_type = parts[10]	//*
	var ligand_receptor = "" //*
	var writeTrigger = 0 // *

	if (interaction_ligand_type == "protein"){
	    ligand_receptor = ligand_string.split("_").slice(2,4).join("")
	    writeTrigger = 1
	}
	
	if (writeTrigger){
	    if (ligs.indexOf(ligand_receptor) == -1){
		console.log(ligand_receptor,pdb_receptor)
		ligs.push(ligand_receptor)
	    }
	}
    })

    lineReader.on('close', function (line) {
	
	
    });


}
