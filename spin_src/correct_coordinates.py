## given input coordinate file, correct node coordinates.

import json
import argparse
import gzip

def extract_coordinates(input_json):
    with open(input_json) as f:
        data = json.load(f)
        
    cprs = {}
    #`{'data': {'id': 4711, 'label': 'Ddit3_Mus_musculus_P35639', 'weight': 0.2}, 'group': 'nodes', 'position': {'x': 0.3915583372828445, 'y': 0.40875830452694173}}

    for entry in data:
        if entry['group'] == "nodes":
            cprs[entry['data']['id']] = entry['position']            

    return cprs

def change_coordinates(old_json,mapping):
    
    with gzip.open(old_json) as f:
        net = json.load(f)            
    missing = 0
    if "grid" in old_json:
        for entry in net['nodes']:
            entry['position'] = mapping[entry['data']['id']]
    else:
        for entry in net:
            if entry['group'] == "nodes":
                try:
                    entry['position'] = mapping[entry['data']['id']]
                except:
                    missing+=1

    print("finished with",missing,"missing.")
    with gzip.GzipFile(old_json, 'w') as fout:
        fout.write(json.dumps(net).encode('utf-8'))                       

if __name__ == "__main__":
        
    parser = argparse.ArgumentParser()
    parser.add_argument("--layout_file",default="default")
    parser.add_argument("--input_old_layout",default="default")
    args = parser.parse_args()    
    coordinates = extract_coordinates(args.layout_file)    
    change_coordinates(args.input_old_layout,coordinates)
