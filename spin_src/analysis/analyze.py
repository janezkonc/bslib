import gzip
import json
import math
import numpy as np
from matplotlib import pyplot as plt

def partitionByZoom(partitions,zoom,centerDim):
    
    centerDim = math.ceil((centerDim-1)/2)
    partNo = math.ceil((50/zoom-1)/2)
    
    minCount = float('inf')
    maxCount = -float('inf')
    
    edges = np.zeros((50,50))
    
    scaned = {}
    
    for i in range(50):
        for j in range(50):
            centerPartition = str(i)+"_"+str(j)
            
            edgeCount = 0
            
            minX = max(0,i-partNo+min(49-(i+partNo),0))
            maxX = min(i+partNo-min(0,i-partNo),50)
            minY = max(0,j-partNo+min(49-(j+partNo),0))
            maxY = min(j+partNo-min(0,j-partNo),50)
            
            if centerDim and centerDim<partNo:
                minXc = max(0,i-centerDim+min(49-(i+centerDim),0))
                maxXc = min(i+centerDim-min(0,i-centerDim),50)
                minYc = max(0,j-centerDim+min(49-(j+centerDim),0))
                maxYc = min(j+centerDim-min(0,j-centerDim),50)

            else:
                minXc = minX
                maxXc = maxX
                minYc = minY
                maxYc = maxY
            
            partitionName = str(minX)+"_"+str(maxX)+"_"+str(minY)+"_"+str(maxY)
            skip = False
            
            if partitionName in scaned:
                skip = True
                edgeCount = scaned[partitionName]
            
            if not(skip):
                #print(minX,maxX,minY,maxY)

                for partitionFrom in partitions:

                    [partition1X,partition1Y] = partitionFrom.split("_")
                    partitionFromIn = (int(partition1X) >= minX and int(partition1Y) < maxX and int(partition1Y) >= minY and int(partition1Y) < maxY)
                    partitionFromInc = (int(partition1X) >= minXc and int(partition1Y) < maxXc and int(partition1Y) >= minYc and int(partition1Y) < maxYc)

                    for partitionTo in partitions[partitionFrom]:

                        [partition2X,partition2Y] = partitionTo.split("_")
                        partitionToIn = (int(partition2X) >= minX and int(partition2Y) < maxX and int(partition2Y) >= minY and int(partition2Y) < maxY)
                        partitionToInc = (int(partition2X) >= minXc and int(partition2Y) < maxXc and int(partition2Y) >= minYc and int(partition2Y) < maxYc)

                        if ((partitionFromInc and not(partitionToIn)) or (partitionToInc and not(partitionFromIn))):
                            #print([minX,maxX],[minY,maxY],partitionFrom,partitionTo)
                            edgeCount += partitions[partitionFrom][partitionTo]
                            
                scaned[partitionName] = edgeCount
                        
            #print(edgeCount)
            edges[i][j] = edgeCount
            if edgeCount < minCount:
                minCount = edgeCount
            if edgeCount > maxCount:
                maxCount = edgeCount
                
        print(i)
        
    #if (minCount != float('inf') and minCount != maxCount):
    #    edges = [[(j)/(maxCount) for j in i] for i in edges]
    print(minCount,maxCount)     
    return edges,maxCount

def draw_matrix(edges,maxCount):
    
    from matplotlib import cm as cm
            
    fig = plt.figure()
    ax1 = fig.add_subplot(111)
    cmap = cm.get_cmap('jet', 30)
    cax = ax1.imshow(edges)
    ax1.grid(True)
    #plt.title('Abalone Feature Correlation')
    #labels=['Sex','Length','Diam','Height','Whole','Shucked','Viscera','Shell','Rings',]
    #ax1.set_xticklabels(labels,fontsize=6)
    #ax1.set_yticklabels(labels,fontsize=6)
    # Add colorbar, make sure to specify tick locations to match desired ticklabels
    fig.colorbar(cax, ticks=[x*maxCount for x in [0,0.2,0.4,0.6,.8,.90,1]])
    plt.show()

with gzip.open("../whole_proteome_graph/grid_proteome.json.gz") as file:
    
    partitions = json.loads(file.read().decode("ascii"))["edges"]

    countZeros = 0
    for partitionFrom in partitions:
        for partitionTo in partitions[partitionFrom]:
            partitions[partitionFrom][partitionTo] = len(partitions[partitionFrom][partitionTo])
            
    edges,maxCount = partitionByZoom(partitions,3,5)
    draw_matrix(edges,maxCount)
