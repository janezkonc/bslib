#!/bin/bash

POSITIONAL=()
while [[ $# -gt 0 ]]
do
key="$1"

case $key in
    --genprobis_json)
    JSONBASE="$2"
    shift
    shift
    ;;
    --probisdb_ligands)
    LIGANDS="$2"
    shift
    shift
    ;;
    --probisdb_bresi)
    BRESIFOLDER="$2"
    shift
    shift
    ;;
    --install_dependencies)
    DEPS="$2"
    shift
    shift
    ;;
    --image_outfile)
    IMAGEOUTFOLDER="$2"
    shift
    shift
    ;;
    --reduce_ligands)
    REDUCELIGANDSTRIGGER="$2"
    shift
    shift
    ;;    
esac

done
echo "INFO TESTING $JSONBASE $LIGANDS $DEPS"

## install dependencies..
if [ "$DEPS" == "yes" ]; then
    wget -qO- https://raw.githubusercontent.com/creationix/nvm/v0.33.1/install.sh | bash
    export NVM_DIR="$HOME/.nvm"
    [ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh" ## path
    nvm install node
    nvm use 7.9 ## node version to use

    ## nx for graph processing -- this also generates images!
    apt-get install python3-pip
    pip3 install networkx
    pip3 install matplotlib
    pip3 install numpy
    apt-get install unzip

else
    echo "INFO Dependencies installed.."
fi

mkdir data; ## if not already there
mkdir $SPINDB; ## generate target folder

INDEXFILES="$SPINDB/dbindex/"
REDUCEDLIGANDSFOLDER="$SPINDB/reduced_ligands"
JOBS=16

# ## remove old
# rm -rf tmp/*
# rm -rf $SPINDB/tmp_index
# rm -rf $SPINDB/dbindex
# rm -rf $SPINDB/genegraph
# rm -rf $SPINDB/gene_graph_individual
# rm -rf $SPINDB/geneDescriptionJson
# rm -rf $SPINDB/pdbseq
# rm -rf $SPINDB/statistics
# rm -rf $SPINDB/enrichment_db
# rm -rf $SPINDB/local_networks

# ## get new
# mkdir $SPINDB/local_networks
# mkdir $SPINDB/tmp_index
# mkdir $SPINDB/dbindex
# mkdir $SPINDB/genegraph
# mkdir $SPINDB/gene_graph_individual
# mkdir $SPINDB/geneDescriptionJson
# mkdir $SPINDB/pdbseq
# mkdir $SPINDB/statistics
# mkdir $SPINDB/enrichment_db
# mkdir $REDUCEDLIGANDSFOLDER
# mkdir tmp

# ###########BLAST PART #######
# echo "INFO ..................Updating the blast database"
# rm -rf tmp_blast/
# mkdir tmp_blast
# cd tmp_blast/
# wget ftp://ftp.rcsb.org/pub/pdb/derived_data/pdb_seqres.txt -O pdbase.fa
# cd ..
# "$SPINSRC/"/makeblastdb -in tmp_blast/pdbase.fa -dbtype prot
# mkdir $SPINDB/pdbseq
# cp -rf tmp_blast/* $SPINDB/pdbseq
# rm -rvf tmp_blast

# echo "INFO ..................Updating the SIFTS"

# mkdir tmp_sifts;cd tmp_sifts;wget ftp://ftp.ebi.ac.uk/pub/databases/msd/sifts/flatfiles/tsv/pdb_chain_uniprot.tsv.gz;cd ..;
# node "$SPINSRC/"parse_sifts.js tmp_sifts/pdb_chain_uniprot.tsv.gz $SPINDB/dbindex/sifts_mapping.json.gz;
# rm -rf tmp_sifts;

# if [ "$REDUCELIGANDSTRIGGER" == "yes" ];then
#     echo "INFO ..................Reducing ligands.....................";
#     ls "$LIGANDS/" | mawk -v rl="$REDUCEDLIGANDSFOLDER" -v jl="$LIGANDS/" -v node_src="$SPINSRC/" '{print "node",node_src"reduce_genprobis_ligands.js "jl$1,rl}' | parallel --progress;
# fi

# echo "INFO ..................Parsing the UniProt annotations"


# mkdir tmpUniProt;
# mkdir $SPINDB/geneDescriptionJson;
# cd tmpUniProt;
# wget ftp://ftp.uniprot.org/pub/databases/uniprot/current_release/knowledgebase/complete/uniprot_sprot.dat.gz;
# cd ..;
# echo "INFO individual gene records..";
# mkdir tmpIndividualRecords;
# cd tmpIndividualRecords;

# zcat ../tmpUniProt/uniprot_sprot.dat.gz | perl $SPINSRC/parse_uniprot.pl; ## this splits the whole uniprot base to individual records

# cd ..;
# ls tmpIndividualRecords | mawk -v db="$SPINDB" -v node_src="$SPINSRC/" '{print "node",node_src"parse_uniprot2_json.js","tmpIndividualRecords/"$1,db"/geneDescriptionJson",db"/dbindex/unigene_mappings.txt"}' | parallel --progress --load=95%;

# echo "INFO Clearning files.."
# rm -rf tmpUniProt;
# rm -rf tmpIndividualRecords;

# echo "INFO Workflow executed, gene descriptions generated.."

# echo "INFO Creating index.."

# echo "INFO ..........................Generating index of genes.."
# ls "$JSONBASE/" | mawk -v jb="$JSONBASE/" -v node_src="$SPINSRC/" '{print "node",node_src"generate_gene_index.js",jb$1}' | parallel --progress >> $SPINDB/dbindex/genelistTMP.txt;

# cat $SPINDB/dbindex/genelistTMP.txt | sort -u > $SPINDB/dbindex/genelist.txt;rm $SPINDB/dbindex/genelistTMP.txt

# mkdir tmp; mkdir $SPINDB/tmp_index;

# echo "INFO ..................Parsing the index....................."
# ls "$JSONBASE/" | mawk -v db="$SPINDB" -v jdb="$JSONBASE" -v node_src="$SPINSRC/" -v  tmp_folder="$SPINDB/tmp_index/" '{print "node",node_src"genIndex.js",$1,jdb,tmp_folder}' | parallel --progress --load=95%

# echo "INFO .. Mapping the files to final form .."

# node "$SPINSRC/"generate_indexfiles.js $SPINDB/tmp_index/index_SNP_pdb $SPINDB/tmp_index
# node "$SPINSRC/"generate_indexfiles.js $SPINDB/tmp_index/index_SNP_uni $SPINDB/tmp_index
# node "$SPINSRC/"generate_indexfiles.js $SPINDB/tmp_index/index_SNP_gene $SPINDB/tmp_index
# node "$SPINSRC/"generate_indexfiles.js $SPINDB/tmp_index/index_OTHER $SPINDB/tmp_index

# echo "INFO Making index for the webpage.."

# cat $SPINDB/tmp_index/index_SNP_pdb | mawk '{print $1"\n"$2}' | sort -u >> $SPINDB/tmp_index/indexdb.txt
# cat $SPINDB/tmp_index/index_OTHER | mawk '{print $1"\n"$2}' | sort -u >> $SPINDB/tmp_index/indexdb.txt

# cat $SPINDB/dbindex/genelist.txt | mawk -F "_" '{split($4,a,"."); print a[1]"\n"$1}' | sort -u >> $SPINDB/tmp_index/indexdb.txt

# cat $SPINDB/tmp_index/indexdb.txt | sort -u > $SPINDB/dbindex/indexdb.txt

# echo "INFO Making examples.."
# cat $SPINDB/tmp_index/indexdb.txt | tail -n 1000 | sort -u > $SPINDB/dbindex/examples.txt
# cat $SPINDB/tmp_index/indexdb.txt | head -n 1000 | sort -u > $SPINDB/dbindex/examples.txt

# echo "INFO Statistics calculation.."
# cat ./tmp/species| mawk -F " " '{print $1"\t"$2}' | sort | uniq -c | sort -nr > $SPINDB/statistics/SummarySpecies.txt

# rm -rf tmp
# cp -rf $SPINDB/tmp_index/*.json $SPINDB/dbindex/
# rm -rf $SPINDB/tmp_index

# mkdir $SPINDB/non_redundant_pdbs
# cd $SPINDB/non_redundant_pdbs
# rm -rvf *
# wget ftp://resources.rcsb.org/sequence/clusters/bc-100.out

# echo "INFO ..................Disease annotations......................."
# cd ../../..
# mkdir tmp_disgenet
# cd tmp_disgenet
# wget http://www.disgenet.org/ds/DisGeNET/results/curated_gene_disease_associations.tsv.gz
# cd ..
# rm -rvf $SPINDB/gene_disease_associations
# mkdir $SPINDB/gene_disease_associations
# node "$SPINSRC/"parse_disgenet2json.js tmp_disgenet/curated_gene_disease_associations.tsv.gz $SPINDB/gene_disease_associations $SPINDB/enrichment_db
# rm -rvf tmp_disgenet

# mkdir tmpgrid
# cd tmpgrid

# wget https://thebiogrid.org/downloads/archives/External%20Database%20Builds/UNIPROT.tab.txt
# wget https://thebiogrid.org/downloads/archives/Latest%20Release/BIOGRID-ALL-LATEST.tab2.zip
# zcat BIOGRID-ALL-LATEST.tab2.zip  > unzippedGRID.txt; gzip unzippedGRID.txt
# cd ..
# rm -rvf $SPINDB/proteome_experimental;
# mkdir $SPINDB/proteome_experimental;
# node "$SPINSRC/"parse_biogrid2json.js tmpgrid/UNIPROT.tab.txt tmpgrid/unzippedGRID.txt.gz $SPINDB/proteome_experimental;

# rm -rf tmpgrid

# mkdir tmpcommons
# cd tmpcommons
# wget http://www.pathwaycommons.org/archives/PC2/v9/PathwayCommons9.All.hgnc.sif.gz
# cd ..

# node "$SPINSRC/"parse_commons.js tmpcommons/PathwayCommons9.All.hgnc.sif.gz $SPINDB/pathway_commons $SPINDB/enrichment_db
# rm -rvf tmpcommons/*
# cd tmpcommons

# wget http://www.pathwaycommons.org/archives/PC2/v9/PathwayCommons9.All.hgnc.txt.gz
# cd ..
# node "$SPINSRC/"parse_commons_pathways.js tmpcommons/PathwayCommons9.All.hgnc.txt.gz $SPINDB/enrichment_db
# cat $SPINDB/enrichment_db/PC_pathways.txt | sort -u >  $SPINDB/enrichment_db/PCPA.txt
# rm -rf tmpcommons
# cd $SPINDB/enrichment_db;gzip *;cd ../../..;

# echo "INFO Generating the whole proteome..";

# mkdir $SPINDB/whole_proteome_graph;
# rm -rf $SPINDB/whole_proteome_graph/*;

# echo "INFO Constructing the whole graph.."
# ls $SPINDB/geneDescriptionJson/ | mawk -v dbif="$SPINDB/dbindex/local_index.txt"  -v node_src="$SPINSRC/" -v core="$SPINDB" -v ldb="$REDUCEDLIGANDSFOLDER" -v js="$JSONBASE" -v bf="$BRESIFOLDER" '{split($1,a,"."); print "node",node_src"uniprotGraph.js "a[1],core,ldb,js,bf,dbif}' | parallel --progress -j12  >> $SPINDB/whole_proteome_graph/proteome_PRL.edgelist;

# LC_ALL=C sort --parallel=32 -uo $SPINDB/dbindex/local_index.txt $SPINDB/dbindex/local_index.txt;

# mkdir tmp_disease;mkdir $SPINDB/all_associations;cd tmp_disease;
# wget http://www.disgenet.org/ds/DisGeNET/results/all_gene_disease_pmid_associations.tsv.gz;
# cd ..;
# node "$SPINSRC/"parse_disease_pubmed2graph.js tmp_disease/all_gene_disease_pmid_associations.tsv.gz $SPINDB/all_associations;
# rm -rf tmp_disease;

# echo "INFO Binary interaction parsing in progress.."
# cp -rf $SPINDB/whole_proteome_graph/proteome_PRL.edgelist $SPINDB/whole_proteome_graph/proteome.edgelist
# rm -rf $SPINDB/dbindex/method_list.tsv;
# rm -rf $SPINDB/dbindex/database_list.tsv;
# rm -rf $SPINDB/whole_proteome_graph/preprocessed_proteome.edgelist;

# rm -rf $SPINDB/binary/*;
# mkdir $SPINDB/binary;cd $SPINDB/binary;
# wget ftp://ftp.ebi.ac.uk/pub/databases/intact/current/psimitab/intact.zip;
# unzip intact.zip;

# node --max-old-space-size=8192 "$SPINSRC/"parse_binary.js $SPINDB/binary/intact.txt $SPINDB/dbindex >> $SPINDB/whole_proteome_graph/proteome_BIN.edgelist $SPINDB/dbindex;

# cat $SPINDB/whole_proteome_graph/proteome_PRL.edgelist $SPINDB/whole_proteome_graph/proteome_BIN.edgelist > $SPINDB/whole_proteome_graph/proteome.edgelist;

# LC_ALL=C sort --parallel=32 -uo $SPINDB/whole_proteome_graph/proteome.edgelist $SPINDB/whole_proteome_graph/proteome.edgelist;

# LC_ALL=C sort --parallel=32 -uo $SPINDB/dbindex/function_mapping.txt $SPINDB/dbindex/function_mapping.txt;

# cat $SPINDB/dbindex/indexdb.txt | sort -u > $SPINDB/dbindex/indexdb_temp.txt;
# mv $SPINDB/dbindex/indexdb_temp.txt $SPINDB/dbindex/indexdb.txt;

# echo "Finished reading..";


# cat $SPINDB/whole_proteome_graph/proteome.edgelist | mawk -F "\t" '{split($1,a,"_");split($2,b,"_");print a[3],a[1],a[2]"_"a[3]"\n"b[3],b[1],b[2]"_"b[3]}'|sort -u > $SPINDB/dbindex/synonyms.txt;


# node --max-old-space-size=8192 "$SPINSRC/"preprocess_edgelist.js $SPINDB/whole_proteome_graph/proteome.edgelist $SPINDB/dbindex/redundancy_mapping.json;


# node --max-old-space-size=8192 $SPINSRC/remove_synonyms.js $SPINDB/dbindex/synonyms.txt $SPINDB/whole_proteome_graph/proteome.edgelist $SPINDB/dbindex/redundancy_mapping.json $SPINDB/dbindex/unigene_mappings.txt | sort -u > $SPINDB/whole_proteome_graph/preprocessed_proteome.edgelist;


# cat $SPINDB/whole_proteome_graph/preprocessed_proteome.edgelist | grep 'TAG_BIN' | mawk -F "\t" '{print $1,$2,$3,$5}' | sort -u > $SPINDB/whole_proteome_graph/reduced_proteome.edgelist;

# cat $SPINDB/whole_proteome_graph/preprocessed_proteome.edgelist | grep 'TAG_PRL' |mawk -F "\t" '{print $1,$2,$3,$4}' | sort -u >> $SPINDB/whole_proteome_graph/reduced_proteome.edgelist;

# cat $SPINDB/whole_proteome_graph/reduced_proteome.edgelist | mawk '{split($1,a,"_");print a[1]"\n"a[4];split($2,b,"_");print b[1]"\n"b[4]}' | sort -u >> $SPINDB/dbindex/indexdb.txt;

# cat $SPINDB/whole_proteome_graph/preprocessed_proteome.edgelist | mawk -F "\t" '{print $1"\n"$2}' |sort -u > $SPINDB/dbindex/genelist.txt;

# mkdir $SPINDB/taxonomy;rm -rf $SPINDB/taxonomy/*;
# cd $SPINDB/taxonomy;
# wget ftp://ftp.ncbi.nlm.nih.gov/pub/taxonomy/taxdmp.zip;
# unzip taxdmp.zip;

# cat names.dmp| mawk -F '\t' '{if($7 == "scientific name"){print $1"\t"$3}}' > $SPINDB/dbindex/taxonomy_map.tsv;

# rm -rf $SPINDB/taxonomy;
# rm -rf $SPINDB/dbindex/genelist.txt; # if exists
# rm -rf $SPINDB/dbindex/node_index_folder;


# mkdir $SPINDB/whole_proteome_graph/species_graphs;
# mkdir $SPINDB/whole_proteome_graph/visualizations;


# mkdir $SPINDB/whole_proteome_graph/server_ready_graphs;
# mkdir $SPINDB/dbindex/node_index_folder;

# cp -rf $SPINDB/whole_proteome_graph/reduced_proteome.edgelist $SPINDB/whole_proteome_graph/species_graphs/whole.edgelist;

# rm -rf $SPINDB/species_specific_blast;

# mkdir $SPINDB/species_specific_blast;

# mkdir $SPINDB/trembl_seqdump;cd $SPINDB/trembl_seqdump;mkdir $SPINDB/whole_proteome_graph/disease_node_mapping;

# ls $SPINDB/geneDescriptionJson/ | mawk -v node_src="$SPINSRC/" -v gdb="$SPINDB/geneDescriptionJson/" -v blast_db="$SPINDB/species_specific_blast" -v taxmap="$SPINDB/dbindex/taxonomy_map.tsv" '{print "node",node_src"sequence_database.js",gdb$1,blast_db,taxmap}' | parallel --progress;

# wget ftp://ftp.uniprot.org/pub/databases/uniprot/current_release/knowledgebase/complete/uniprot_trembl.fasta.gz;

# python3 $SPINSRC/extract_top_sequences_trembl.py --species_ids $SPINSRC/representative_species_list.txt --input_fasta_file $SPINDB/trembl_seqdump/uniprot_trembl.fasta.gz --output_fasta_folders $SPINDB/species_specific_blast;

# ls $SPINDB/species_specific_blast | mawk -v bdb="$SPINDB/species_specific_blast" -v bbin="$SPINSRC/makeblastdb -in"  '{print bbin, bdb"/"$1"/sequence_database.fa","-dbtype prot"}' | parallel --progress;

# mkdir $SPINDB/whole_proteome_graph/species_graphs;
# mkdir $SPINDB/whole_proteome_graph/server_ready_graphs;
# mkdir $SPINDB/whole_proteome_graph/visualizations;
# mkdir $SPINDB/filtered_predictions;rm -rf $SPINDB/filtered_predictions/*
# mkdir $SPINDB/prediction_results;rm -rf $SPINDB/prediction_results/*

# cd $SPINSRC;cd ..;

# while read p; do
#   echo $p;
#   python3 $SPINSRC/prepare_datasets.py --query_network $SPINDB/whole_proteome_graph/reduced_proteome.edgelist --query_species $p --spin_database $SPINDB --description_folder $SPINDB/geneDescriptionJson >> $SPINDB/prediction_results/$p"_predictions.txt"
# done <$SPINSRC/representative_species_list.txt;

# ######python3 $SPINSRC/prepare_datasets.py --query_network $SPINDB/whole_proteome_graph/reduced_proteome.edgelist --query_species 4113 --spin_database $SPINDB --description_folder $SPINDB/geneDescriptionJson >> $SPINDB/prediction_results/"4113_predictions.txt"

# for j in $SPINDB/prediction_results/*;
# do
#     ## filter and add edges
#     Fnm="$SPINDB/whole_proteome_graph/species_graphs/"$(echo $j | mawk '{split($1,b,"/");split(b[8],a,"_");print a[1]".edgelist"}' | mawk '{print $1}')
#     echo "Processing and filtering: "$Fnm
#     perl -lane '$id = 40;$cov = 40;$eval = 0.001;print $F[1]," ",$F[2]," ",$F[17]," ",(($F[6]+$F[12])/2)*(($F[8]+$F[14])/2) if $F[6] > $id && $F[5] < $eval && $F[8] > $cov && $F[11] < $eval && $F[12] > $id && $F[14] > $cov ' $j > $Fnm

# done

# cd $SPINSRC;git clone https://github.com/snap-stanford/snap.git;cd snap; make;
# cd $SPINSRC;git clone https://github.com/SkBlaz/Py3Plex.git; cd Py3Plex; python3 setup.py install;
# cd $SPINSRC;git https://github.com/DmitryUlyanov/Multicore-TSNE.git; cd Multicore-TSNE; python3 setup.py install;

mkdir $SPINDB/whole_proteome_graph/tmp_species_graphs;
python3 $SPINSRC/add_edge_types_to_species_graphs.py --networks_folder $SPINDB/whole_proteome_graph/species_graphs
cp -rf $SPINDB/whole_proteome_graph/species_graphs $SPINDB/whole_proteome_graph/species_graphs_raw_backup
cp -rf $SPINDB/whole_proteome_graph/tmp_species_graphs/* $SPINDB/whole_proteome_graph/species_graphs/

mkdir $SPINDB/whole_proteome_graph/embeddings;
for j in $SPINDB/whole_proteome_graph/species_graphs/*;

do

    TMP=$(echo $j | mawk '{n=split($1,a,"/");split(a[n],b,".");print b[1]}');

    python3 $SPINSRC/get_embedded_coordinates.py --edgelist_file $j --output_json $SPINDB/whole_proteome_graph/embeddings/"$TMP"_embedding.json --n2v_binary $SPINSRC/snap/examples/node2vec/./node2vec;

    node --max-old-space-size=8192  "$SPINSRC/"proces_proteome_node.js $SPINDB/whole_proteome_graph/species_graphs/"$TMP".edgelist $SPINDB/whole_proteome_graph/server_ready_graphs/"$TMP"_proteome_with_layout.json.gz $SPINDB/whole_proteome_graph/server_ready_graphs/"$TMP"_grid_proteome.json.gz $SPINDB/all_associations/disease_associations_whole.json.gz $SPINDB/whole_proteome_graph/disease_node_mapping/"$TMP"_disease_node_mapping.json.gz $SPINDB/dbindex/taxonomy_map.tsv $SPINDB/dbindex/node_index_folder/"$TMP".txt $SPINDB/whole_proteome_graph/embeddings/"$TMP"_embedding.json;
    
    $SPINSRC/./force_layout_correction --network $SPINDB/whole_proteome_graph/server_ready_graphs/"$TMP"_proteome_with_layout.json.gz --opti_network $SPINDB/whole_proteome_graph/tmp_coordinates.json --nsteps 2000    

    python3 $SPINSRC/correct_coordinates.py --layout_file $SPINDB/whole_proteome_graph/tmp_coordinates.json --input_old_layout $SPINDB/whole_proteome_graph/server_ready_graphs/"$TMP"_proteome_with_layout.json.gz;

    python3 $SPINSRC/correct_coordinates.py --layout_file $SPINDB/whole_proteome_graph/tmp_coordinates.json  --input_old_layout $SPINDB/whole_proteome_graph/server_ready_graphs/"$TMP"_grid_proteome.json.gz;


    python3 "$SPINSRC/"visualize_graph.py --outfile $SPINDB/whole_proteome_graph/visualizations/"$TMP".png --infile $SPINDB/whole_proteome_graph/server_ready_graphs/"$TMP"_proteome_with_layout.json.gz;

    cat $SPINDB/dbindex/node_index_folder/"$TMP".txt | sort -u > $SPINDB/dbindex/tmp.txt; mv $SPINDB/dbindex/tmp.txt $SPINDB/dbindex/node_index_folder/"$TMP".txt;
    
done

python3 "$SPINSRC/"compute_network_statistics.py --outfolder $SPINDB/whole_proteome_graph --edgelist $SPINDB/whole_proteome_graph/reduced_proteome.edgelist --taxmap $SPINDB/dbindex/taxonomy_map.tsv;

python3 $SPINSRC/network_summary.py --network_folder $SPINDB/whole_proteome_graph --taxmap $SPINDB/dbindex/taxonomy_map.tsv
