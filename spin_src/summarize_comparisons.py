## simple script for analysis of STRINGdb intersections..
import pandas as pd
import glob
import numpy as np
from collections import Counter
if __name__ == "__main__":

    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("--summary_folder",default="~/paper")
    parser.add_argument("--output_latex",default="~/paper/summary.tex")
    args = parser.parse_args()
    rows = []
    scores = []
    for fname in glob.glob(args.summary_folder+"/*"):
        
        try:
            splits = fname.split("_")            
            if len(splits) == 5:
                species, dimer, presence, _, typ = splits
                
            else:
                
                species, dimer, presence,_, _, typ = splits
            species = species.split("/")[-1]
            typ = typ.split(".")[-2]

            if presence == "not":
                
                presence = "no"
            else:
                presence = "yes"
                
            dataset = pd.read_csv(fname,sep="\t")
            subset = dataset[dataset.tags == "TAG_PRL"].spin_homo_score.tolist()
            scores+=subset
            cnt = Counter()
            for item in dataset.tags:
                if "," in item:
                    item = item.split(",")
                    cnt.update(item)
                else:
                    cnt.update([item])
                    
            print(cnt)
            
            tag_bin = 0
            tag_prl = 0
            tag_prl_exp = 0
            if "TAG_BIN" in cnt.keys():
                tag_bin = cnt['TAG_BIN']
            if "TAG_PRL" in cnt.keys():
                tag_prl = cnt['TAG_PRL']
            if "TAG_PRL_EXP" in cnt.keys():
                tag_prl_exp = cnt['TAG_PRL_EXP']
            
            
            num_of_items = dataset.shape[0]
            rows.append({"species": species, "interaction_type" : typ, "dimer" : dimer, "in STRINGdb" : presence,"count":num_of_items,"binary":tag_bin,"predicted":tag_prl,"pred_exp":tag_prl_exp})        

        except Exception as es:
            print(es)

    print(np.mean(scores),np.std(scores))
    fframe = pd.DataFrame(rows)
    fframe = fframe.sort_values(by=["count","in STRINGdb"])
    #fframe.to_latex(args.output_latex,index=False)

    completely_new_predictions = fframe[fframe['in STRINGdb'] == "no"]['predicted'].sum()
    print(completely_new_predictions)

    ## organism-specific new interactions
    orgs = fframe[fframe['in STRINGdb'] == "no"][['predicted','species']].groupby('species').sum()
    print(orgs['predicted'].sum())
    orgs2 = fframe[fframe['in STRINGdb'] == "yes"][['predicted','species']].groupby('species').sum()
    print(orgs2['predicted'].sum())
#    print(fframe)
    all_int = fframe['predicted'].sum()
    print(all_int)

    all_count = fframe['count'].sum()
    print(all_count)

    homol = fframe[fframe['in STRINGdb'] == "yes"]['binary'].sum()
    knhomol = fframe[fframe['in STRINGdb'] == "no"]['binary'].sum()
    print(homol,knhomol)

    ## exp del.
    homol = fframe[fframe['in STRINGdb'] == "yes"]['pred_exp'].sum()
    knhomol = fframe[fframe['in STRINGdb'] == "no"]['pred_exp'].sum()
    print(homol,knhomol)
    
    
#    print(completely_new_predictions)
          
