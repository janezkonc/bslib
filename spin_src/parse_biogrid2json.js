// this script parses the whole biogrid into small, comprehensible json-s,
// which are used as gene info

// this script takes curated disgenet and transforms it into a json file.

var fs = require('fs');
var zlib = require('zlib');

generateBGmap(process.argv[2],process.argv[3],(infile2,mapping)=>{

    // use mapping to construct the proteome.
    
    var bioGRID = {}

    if (infile2.indexOf('.gz') != -1){
	var LR = require('readline').createInterface({
	    input: fs.createReadStream(infile2).on('error',()=>{	    
		console.log("Error occured for file:",infile2)
	    }).pipe(zlib.createGunzip())
	});

    }else{
	var LR = require('readline').createInterface({
	    input: require('fs').createReadStream(process.argv[3])
	});           
    }

    LR.on('error', function (line) {
	console.log("Error occured for file:",infile2)    
    })

    var cntr = 0
    LR.on('line', function (line) {

	var parts = line.split("\t")
	var i1 = parts[3]
	var i2 = parts[4]

	// re-map
	var uterm1 = mapping[i1]
	var uterm2 = mapping[i2]

	if (uterm1 == null | uterm1 == undefined)
	    uterm1 = "-"

	if (uterm2 == null | uterm2 == undefined)
	    uterm2 = "-"

	// add both partners to the base
	if (!bioGRID.hasOwnProperty(uterm1))
	    bioGRID[uterm1] = []

	if (!bioGRID.hasOwnProperty(uterm2))
	    bioGRID[uterm2] = []

		
	var partner_info_first = {'partner_name' : uterm2,
				  'partner_symbol' : parts[8],
				  'experimental_system' : parts[11],
				  'partner_organism' : parts[16],
				  'source_db' : parts[23],
				  'PMID' : parts[14],
				  'pheno' : parts[20]}
	
	var partner_info_second = {'partner_name' : uterm1,
				  'partner_symbol' : parts[7],
				  'experimental_system' : parts[11],
				  'partner_organism' : parts[15],
				  'source_db' : parts[23],
				  'PMID' : parts[14],
				   'pheno' : parts[20]}

	bioGRID[uterm1].push(partner_info_first)
	bioGRID[uterm2].push(partner_info_second)

	cntr+=2

	if (cntr % 100000 == 0)
	    console.log("Parsed",cntr,"entries.")
		
    })

    LR.on('close', function (line) {

	console.log("Writing to the database..")
	
	var outpath = process.argv[4]
	for (var k in bioGRID){
	    var path = outpath+"/"+k+".json"
	    // write under k the json
	    fs.writeFileSync(path, JSON.stringify(bioGRID[k]))
	    
	}
    })
})


function generateBGmap(infile, infile2, cb){

    // create this core mapping
    
    var uni2bg = {}

    if (infile.indexOf('.gz') != -1){
	var LR = require('readline').createInterface({
	    input: fs.createReadStream(infile).on('error',()=>{	    
		console.log("Error occured for file:",infile)
	    }).pipe(zlib.createGunzip())
	});

    }else{
	var LR = require('readline').createInterface({
	    input: require('fs').createReadStream(infile)
	});           
    }

    LR.on('error', function (line) {    
	console.log("Error occured for file:",infile)    
    })

    LR.on('line', function (line) {

	var parts = line.split(/\s+/)
	uni2bg[parts[1]] = parts[0]
		
    })

    LR.on('close', function (line) {
	
	cb(infile2,uni2bg)
    })
    
}
