## Run to compute the intersection between SPIN protein-protein interactions and interactions in the STRING database.
## To prepare for arabidopsis or solanum uncomment the commented sections
##

from __future__ import print_function
import argparse
from collections import defaultdict
import numpy as np
from operator import itemgetter
import gzip
import json
import subprocess
import csv

def create_dict(reader, keys, value):
	
	result_dict = defaultdict(set)
	for row in reader:
		skip = False
		if row[value] is None:
			skip = True
		for k in keys:
			if row[k] is None:
				skip = True
				break
		if skip:
			continue
		joint_keys = ''.join([row[k] for k in keys])
		result_dict[joint_keys].add(row[value])

	return result_dict

def read_tsv_file(file_name, field_names, delim='\t'):
	
	result_dict = defaultdict(set)
	with gzip.open(file_name) as f:
		f = f.read().decode().split('\n')
		return csv.DictReader(f, fieldnames=field_names, delimiter=delim, quoting=csv.QUOTE_NONE)
		
def create_file_hash(file_name, delim='\t'):

	string_hash = {}
	prev_string1 = ''
	loc = 0
	with gzip.open(file_name) as f:
		for line in f:
			line = line.decode()
			if line[0] != "#":
				#print(line)
				split = line.strip().split(delim)
				string1 = split[0]
				if prev_string1 != string1:
					key = string1.split('.')[1] # ex. 3702.AT124356.1
					string_hash[key] = loc
					prev_string1 = string1
				loc = f.tell()
	return string_hash
			
def link_exists(file_name, string_hash, string1, string2, delim='\t'):

	key1 = string1.split('.')[1]
	key2 = string2.split('.')[1]
	
	if key1 not in string_hash:
		return False, 0
	with gzip.open(file_name) as f:
		f.seek(string_hash[key1])
		for line in f:
			line = line.decode()
			if line[0] != "#":
				split = line.strip().split(delim)
				cmp1 = split[0].split('.')[1]
				cmp2 = split[1].split('.')[1]
				if key1 != cmp1:
					return False, 0
				if key2 == cmp2:
					combined_score = float(int(split[2]) / 1000)
					return True, combined_score

#def get_string_or_araport(uniprot, uniprot_string_map, uniprot_araport_map, arabidopsis):
#	try:
#		return list(uniprot_string_map[uniprot])[0]
#	except:
#		pass
#	if arabidopsis:
#		try:
#			return '3702.' + list(uniprot_araport_map[uniprot])[0]
#		except:
#			pass
#	return 'NOT_DETERMINED'

def get_string(uniprot, uniprot_string_map):
	try:
		return list(uniprot_string_map[uniprot])[0]
	except:
		pass
	return 'NOT_DETERMINED'

if __name__ == '__main__':
	
	## params: file, genes..
	parser = argparse.ArgumentParser()
	parser.add_argument('--species_graph',default='4113_predictions.txt.gz')
	parser.add_argument('--global_spin_graph',default='whole.edgelist.gz')
	parser.add_argument('--string_links',default='protein.links.v10.5.txt_taxid.gz')
	parser.add_argument('--uniprot_string_map',default='idmapping_string.dat.gz')
	#parser.add_argument('--uniprot_araport_map',default='idmapping_araport.dat.gz')
	#parser.add_argument('--gomapman_map',default='stu_PGSC_transcript_2018-05-25_mapping.txt.gz') # ath_Araport11_2018-05-25_mapping.txt.gz, sly_SL3.0_ITAG3.2_2018-05-25_mapping.txt.gz
	args = parser.parse_args()

	species_graph = read_tsv_file(args.species_graph, ['inter', 'homo1', 'homo2', 'bsp1', 'uni1', 'eval1', 'cov1', 'len1', 'seqid1',
						    'bsp2', 'uni2', 'eval2', 'cov2', 'len2', 'seqid2', 'orig1', 'orig2', 'tag'])

	global_spin_graph = list(read_tsv_file(args.global_spin_graph, ['orig1', 'orig2', 'tag', 'score'], ' '))
	
	origs_tag = create_dict(global_spin_graph, ['orig1', 'orig2'], 'tag')
	origs_score = create_dict(global_spin_graph, ['orig1', 'orig2'], 'score')

	uniprot_string_map = create_dict(read_tsv_file(args.uniprot_string_map, ['uni', 'skip', 'string']), ['uni'], 'string')

	#arabidopsis = True if '3702' in args.species_graph else False
	#
	#uniprot_araport_map = defaultdict(set)
	#if arabidopsis:
	#	uniprot_araport_map = create_dict(read_tsv_file(args.uniprot_araport_map, ['uni', 'skip', 'araport']), ['uni'], 'araport')
	#
	#gomapman_name_map1 = create_dict(read_tsv_file(args.gomapman_map, ['bincode', 'name', 'id', 'description']), ['id'], 'name')
	#gomapman_desc_map1 = create_dict(read_tsv_file(args.gomapman_map, ['bincode', 'name', 'id', 'description']), ['id'], 'description')
	#
	#gomapman_name_map = defaultdict(set)
	#gomapman_desc_map = defaultdict(set)
	#
	#for key, val in gomapman_name_map1.items():
	#	key = key.split('.')[0]
	#	val = list(val)[0]
	#	gomapman_name_map[key].add(val)
	#
	#for key, val in gomapman_desc_map1.items():
	#	key = key.split('.')[0]
	#	val = list(val)[0]
	#	gomapman_desc_map[key].add(val)


	string_hash = create_file_hash(args.string_links, ' ')

	#print('uni1', 'gene1', 'string1', 'seqid1', 'align_length1', 'coverage1', 'e-value1', 'id1', 'gomapman_name1', 'gomapman_desc1',
	#      'uni2', 'gene2', 'string2', 'seqid2', 'align_length2', 'coverage2', 'e-value2', 'id2', 'gomapman_name2', 'gomapman_desc2',
	#      'tags', 'scores', 'spin_homo_score', 'in_string', 'string_score',
	#      'uni_orig1', 'gene_orig1', 'taxid_orig1',
	#      'uni_orig2', 'gene_orig2', 'taxid_orig2', sep='\t'
	#)
	print('uni1', 'gene1', 'string1', 'seqid1', 'align_length1', 'coverage1', 'e-value1', 'id1',
	      'uni2', 'gene2', 'string2', 'seqid2', 'align_length2', 'coverage2', 'e-value2', 'id2',
	      'tags', 'scores', 'spin_homo_score', 'in_string', 'string_score',
	      'uni_orig1', 'gene_orig1', 'taxid_orig1',
	      'uni_orig2', 'gene_orig2', 'taxid_orig2', sep='\t'
	)

	# do the mappings
	for row in species_graph:

		# TAG_PRL{_EXP} or TAG_BIN or both
		orig1 = row['orig1']
		orig2 = row['orig2']

		if orig1 is None or orig2 is None:
			continue

		tags = ','.join(origs_tag[orig1 + orig2])
		scores = ','.join(origs_score[orig1 + orig2])
			
		# calculate score
		seqid1 = int(row['seqid1'])
		seqid2 = int(row['seqid2'])

		cov1 = float(row['cov1'].strip("'"))
		cov2 = float(row['cov2'].strip("'"))

		if not (seqid1 > 40 and seqid2 > 40 and cov1 > 40 and cov2 > 40):
			continue
		
		spin_homo_score = (seqid1 + seqid2) * (cov1 + cov2) / 4


		# check if this interaction is in STRING
		uni_orig1 = orig1.split('_')[3]
		uni_orig2 = orig2.split('_')[3]

		gene_orig1 = orig1.split('_')[0]
		gene_orig2 = orig2.split('_')[0]
		
		taxid_orig1 = orig1.split('_')[1]
		taxid_orig2 = orig2.split('_')[1]
		
		homo1 = row['homo1']
		homo2 = row['homo2']

		uni1 = homo1.split('_')[3]
		uni2 = homo2.split('_')[3]

		gene1 = homo1.split('_')[0]
		gene2 = homo2.split('_')[0]
		
		eval1 = row['eval1']
		eval2 = row['eval2']

		len1 = row['len1']
		len2 = row['len2']

		id1 = ''
		id2 = ''

		#gomapman_name1 = ''
		#gomapman_name2 = ''
		#
		#gomapman_desc1 = ''
		#gomapman_desc2 = ''


		#string1 = get_string_or_araport(uni1, uniprot_string_map, uniprot_araport_map, arabidopsis)
		#string2 = get_string_or_araport(uni2, uniprot_string_map, uniprot_araport_map, arabidopsis)
		string1 = get_string(uni1, uniprot_string_map)
		string2 = get_string(uni2, uniprot_string_map)

		string_names = True if string1 != 'NOT_DETERMINED' and string2 != 'NOT_DETERMINED' else False

		try: 
			id1 = string1.split('.')[1]
			#gomapman_name1 = list(gomapman_name_map[id1])[0]
			#gomapman_desc1 = list(gomapman_desc_map[id1])[0]
		except:
			pass

		try: 
			id2 = string2.split('.')[1]
			#gomapman_name2 = list(gomapman_name_map[id2])[0]
			#gomapman_desc2 = list(gomapman_desc_map[id2])[0]
		except:
			pass

		combined_score = 0.0
		if string_names:
			in_string = 'STRING_NO'
			exists, combined_score = link_exists(args.string_links, string_hash, string1, string2, ' ')
			if exists:
				in_string = 'STRING_YES'
		else:
			in_string = 'STRING_MISSING'
			
			
		#print(uni1, gene1, string1, seqid1, len1, cov1, eval1, id1, gomapman_name1, gomapman_desc1, 
		#      uni2, gene2, string2, seqid2, len2, cov2, eval2, id2, gomapman_name2, gomapman_desc2,
		#      tags, scores, spin_homo_score, in_string, combined_score,
		#      uni_orig1, gene_orig1, taxid_orig1, uni_orig2, gene_orig2, taxid_orig2, sep='\t'
		#)
		print(uni1, gene1, string1, seqid1, len1, cov1, eval1, id1,
		      uni2, gene2, string2, seqid2, len2, cov2, eval2, id2,
		      tags, scores, spin_homo_score, in_string, combined_score,
		      uni_orig1, gene_orig1, taxid_orig1, uni_orig2, gene_orig2, taxid_orig2, sep='\t'
		)
