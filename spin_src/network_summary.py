## summarise datasets

## summarise datasets
## for each dataset

import numpy as np
import scipy.io
import networkx as nx
import glob
import pandas as pd
import argparse
from py3plex.core import multinet
from collections import defaultdict
parser = argparse.ArgumentParser()
parser.add_argument("--network_folder",default="default")
parser.add_argument("--taxmap",default="/temp/blazs/spin/dbindex/taxonomy_map.tsv")
args = parser.parse_args()

mappings = {}        
with open(args.taxmap) as tm:
    for line in tm:
        line = line.strip().split("\t")
        if line[0] not in mappings:
            mappings[str(line[0])] = line[1]
mappings["whole"] = "Global network"
rframe = pd.DataFrame(columns=["Name","Nodes","Edges","Connected components","Clustering coefficient","Density"])
fls = glob.glob(args.network_folder+"/species_graphs"+"/*")

cont = defaultdict(list)
## koliko je binary interakcij v posameznih grafih

with open(args.network_folder+"/species_graphs/whole.edgelist") as el:
    for line in el:
        node1,node2,tag,score = line.strip().split()
        cont[tag].append((node1,node2,score))

bin_hash = set([(x[0],x[1]) for x in cont['TAG_BIN']])
pred_hash = set([(x[0],x[1]) for x in cont['TAG_PRL']])
exp_hash = set([(x[0],x[1]) for x in cont['TAG_PRL_EXP']])
        
## compute statistics for this part..
for fi in fls:
        
    multilayer_network = multinet.multi_layer_network().load_network(input_file=fi,directed=False,input_type="edgelist_spin")


    # if "whole.edgelist" in fi:
    #     num_binary = len(cont['TAG_BIN'])
    #     num_pred = len(cont['TAG_PRL'])
    #     num_exp = len(cont['TAG_PRL_EXP'])
    # else:
    print("finding intersections")
    edge_hash = set()
    for edge in multilayer_network.get_edges():
        node1,node2 = edge
        edge_hash.add((node1,node2))
        edge_hash.add((node2,node1))

    num_exp = int(len(set.intersection(edge_hash,exp_hash)))
    num_bin = int(len(set.intersection(edge_hash,bin_hash)))
    num_pred = int(len(set.intersection(edge_hash,pred_hash)))

    ## dodati je treba se novel interactions!
    print(num_exp,num_bin,num_pred)
    G = multilayer_network.core_network
    nodes = len(G.nodes())
    edges = len(G.edges())
    cc = len(list(nx.connected_components(G)))
    
    try:
        clustering = nx.average_clustering(G)
    except:
        clustering = None
        
    dx = nx.density(G)
    mean_degree = np.mean([G.degree(j) for j in G.nodes()])
    name = mappings[fi.split("/")[-1].replace(".mat","").split(".")[0]]
    if nodes > 0:
        point = {"Name": name,
                 "Nodes":nodes,
                 "Edges":edges,
                 "Mean degree":np.round(mean_degree,3),
                 "Connected components":cc,
                 "Clustering coefficient":np.round(clustering,3),
                 "Density":np.round(dx,3),
                 "ProBiS Ligands predicted":num_pred,
                 "Binary":num_bin,
                 "Experimental structural":num_exp}

        rframe = rframe.append(point,ignore_index=True)
        print(point)
    
rframe = rframe.sort_values(by='Nodes', ascending=False)
rframe.reset_index(drop = True, inplace = True)
print(rframe)
rframe.to_latex(args.network_folder+"/network_summary.tex",columns=rframe.columns,index=False)
