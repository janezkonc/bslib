// this file generates index files from datadumps

var fs = require('fs')
var infile = process.argv[2]

var lineReader = require('readline').createInterface({    
    input: require('fs').createReadStream(infile)
});

var outfolder = process.argv[3]+"/"+infile.split("/").splice(-1)+".json"
var outjson = {}
console.log("Processing:",outfolder)
lineReader.on('line', function (line) {    
    var parts = line.split(" ")
    if (!outjson.hasOwnProperty(parts[0])){
	outjson[parts[0]] = [parts[1]]
    }else{
	outjson[parts[0]].push(parts[1])
    }    
});

lineReader.on('close', function (line) {

    for (var k in outjson){
	var set = new Set(entries[k]);
	outjson[k] = Array.from(set);
    }

    fs.writeFile(outfolder, JSON.stringify(outjson), function(err) {
	if(err) {

	}
    });     
});
