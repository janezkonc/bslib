// this script generates the index of name_species_uniprot triplets into a desired file

var fs = require('fs');
var zlib = require('zlib');

function extract_triplet(){
    var lineReader = require('readline').createInterface({
	input: fs.createReadStream(process.argv[2]).on('error',()=>{	    
	    console.log("Error occured for file:",process.argv[2])
	}).pipe(zlib.createGunzip())
    });    
    lineReader.on('error', function (line) {	
	console.log("Error occured for file:",process.argv[2])
    })


    var wbody = ""
    lineReader.on('line', function (line) {
	wbody+=line
    })

    lineReader.on('close', function (line) {
	var uniprot = ""; var species = ""; var gene_name = "";	
	var wjson = JSON.parse(wbody);
	for(j = 0; j < wjson.length;j++){
	    var ent = wjson[j]
	    try{
	    var uniacc = ent.protein.uniprot.ac
	    var gname = ent.protein.uniprot.gene_name
	    if (ent.protein.uniprot.organism != undefined){
		var org = ent.protein.uniprot.organism.split(" ").splice(0,2).join("_")
		if (uniprot != "" && species != "" && gene_name != ""){
		    var triplet = gene_name+"_"+species+"_"+uniprot;
		    console.log(triplet)
		    break;
		}
		else{
		    if(uniacc != undefined)
			uniprot = uniacc;
		    if(gname != undefined)
			gene_name = gname;
		    if(org != undefined)
			species = org;
		}
	    }
	    }catch(err){
		
	    }
	}	
    });
}

extract_triplet()
