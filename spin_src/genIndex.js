//this node file is responsible for index generation..
// read wholebase from STDIN zip

const fs       = require('fs');
const zlib     = require('zlib');
const readline = require('readline');
const db_folder = process.argv[3]+"/"+process.argv[2]

var tmp_folder = process.argv[4]
var tmp_SNP_pdb = tmp_folder+"index_SNP_pdb"
var tmp_other = tmp_folder+"index_OTHER"
var tmp_species = tmp_folder+"species"
var tmp_SNP_gene = tmp_folder+"index_SNP_gene"
var tmp_SNP_uni = tmp_folder+"index_SNP_uni"

let lineReader = readline.createInterface({
    input: fs.createReadStream(db_folder).pipe(zlib.createGunzip())
});

var tmp_json = ""
lineReader.on('line', (line) => {
    tmp_json+=line
});

lineReader.on('close', (line) => {

    //async code is not important here..    
    var organism = get_unique_mapping(JSON.parse(tmp_json))['organism']
    if (organism != undefined){
	var uniq_map = get_unique_mapping(JSON.parse(tmp_json))['mapping_data']
	append_snps_pdb(uniq_map)
	append_snps_uni(uniq_map)
	append_snps_gene(uniq_map)
	append_other(uniq_map) 
	append_organism(organism)
    }    
});

function get_unique_mapping(jsonfile){
    
    var final_json = JSON.parse(tmp_json)
    var mapping = {'snp_pdb' : {}, 'uni' : {},'snp_uni' : {},'snp_gene' : {}}
    var organism_global
    var defined_organism = false
    var target = final_json.length
    var index = 0
    final_json.forEach((key)=>{

	try{
	    var UniProt = key['protein']['uniprot']['ac']
	}catch(err){
	    var UniProt = undefined;
	}

	try{
	    var gene_name = key['protein']['uniprot']['gene_name']
	}catch(err){
	    var gene_name;
	}
	
	var PDB_id = key['protein']['pdb']['pdb_id']+key['protein']['pdb']['chain_id']

	try{
	    var organism = key['protein']['uniprot']['organism']
	}catch(err){
	    var organism;
	}
	if (organism != undefined){
	    if (defined_organism == false){
		defined_organism == true
		organism_global = organism
	    }
	}

	if (UniProt != null || UniProt != undefined && UniProt.length > 2){
	    mapping['uni'][UniProt] = PDB_id
	}

	if (gene_name != null || gene_name != undefined && gene_name.length > 2){
	    mapping['uni'][gene_name] = PDB_id
	}
	
	try{
	    
	    var snv_id = key['snp']['uniprot']['source_id']

	    mapping['snp_pdb'][snv_id] = PDB_id
	    mapping['snp_uni'][snv_id] = UniProt
	    mapping['snp_gene'][snv_id] = gene_name
	    index++
	}catch(err){
	    index++
	}
    })		      

    if(index == target){
	var outhash = {}
	return {'mapping_data' : mapping,'organism' : organism_global}
    }
}

function append_other(mapping){

    var outstring = ""
    for (var key in mapping['uni']){
	if (key != undefined)
	    outstring += key+" "+mapping['uni'][key]+"\n"
    }
    if (outstring != ""){
	fs.appendFile(tmp_other, outstring, function (err) {
	    if (err) throw err;
	});
    }
}

function append_snps_pdb(mapping){

    var outstring = ""
    for (var key in mapping['snp_pdb']){
	outstring += key+" "+mapping['snp_pdb'][key]+"\n"
    }

    if (outstring != ""){
	fs.appendFile(tmp_SNP_pdb, outstring, function (err) {
	    if (err) throw err;
	});
    }
}


function append_snps_gene(mapping){

    var outstring = ""
    for (var key in mapping['snp_gene']){
	outstring += key+" "+mapping['snp_gene'][key]+"\n"
    }
    if (outstring != ""){
	fs.appendFile(tmp_SNP_gene, outstring, function (err) {
	    if (err) throw err;
	});
    }
}

function append_snps_uni(mapping){

    var outstring = ""
    for (var key in mapping['snp_uni']){
	outstring += key+" "+mapping['snp_uni'][key]+"\n"
    }
    if (outstring != ""){
	fs.appendFile(tmp_SNP_uni, outstring, function (err) {
	    if (err) throw err;
	});
    }
}

function append_organism(organism){

    fs.appendFile(tmp_species, organism+"\n", function (err) {
	if (err) throw err;
    });
}
