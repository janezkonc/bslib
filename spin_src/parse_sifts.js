// open and save SIFTS mappings

var fs = require('fs');
var zlib = require('zlib');


if (process.argv[2].indexOf('.gz') != -1){
    var LR = require('readline').createInterface({
	input: fs.createReadStream(process.argv[2]).on('error',()=>{	    
	    console.log("Error occured for file:",process.argv[2])
	}).pipe(zlib.createGunzip())
    });


}else{
    var LR = require('readline').createInterface({
	input: require('fs').createReadStream(process.argv[2])
    });           
}

LR.on('error', function (line) {    
    console.log("Error occured for file:",process.argv[2])    
})

var entries = {};
LR.on('line', function (line) {    
    var parts = line.split("\t");
    entries[parts[0]+parts[1]] = parts[2];
})

LR.on('close', function (line) {

    zlib.gzip(JSON.stringify(entries), function (error, result) {
	if (error){
	    console.log(error)
	}else{	    
	    fs.writeFile(process.argv[3],result, function(err) {
	    	if(err) {
	    	    return console.log(err,"random");
	    	}
	    });
	}
    })    
});

