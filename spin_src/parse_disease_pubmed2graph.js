// parse all disease associations to a graph..

// this script takes curated disgenet and transforms it into a json file.

var fs = require('fs');
var zlib = require('zlib');


if (process.argv[2].indexOf('.gz') != -1){
    var LR = require('readline').createInterface({
	input: fs.createReadStream(process.argv[2]).on('error',()=>{	    
	    console.log("Error occured for file:",process.argv[2])
	}).pipe(zlib.createGunzip())
    });


}else{
    var LR = require('readline').createInterface({
	input: require('fs').createReadStream(process.argv[2])
    });           
}


LR.on('error', function (line) {    

    console.log("Error occured for file:",process.argv[2])    

})

var geneHash = {};
var tcount = 0;
var disease_set = [];

LR.on('line', function (line) {
    //geneId	diseaseId	associationType	sentence	pmid	score	originalSource	diseaseName	diseaseType	geneSymbol
    
    var parts = line.split("\t")
    var pmid = parts[4]
    var disease = parts[7]
    var geneSymbol = parts[9]
    if (pmid != "NA"){
    disease_set.push(disease)
    if (!geneHash.hasOwnProperty(geneSymbol))
	geneHash[geneSymbol] = {};geneHash[geneSymbol][disease] = []
        
    if(geneHash[geneSymbol].hasOwnProperty(disease))
	geneHash[geneSymbol][disease].push(pmid)
    
	tcount+=1;
    }
    
})

LR.on('close', function (line) {

    console.log(disease_set.length)
    var set = new Set(disease_set);
    console.log(set.size)
    zlib.gzip(JSON.stringify(geneHash), function (error, result) {
	if (error){
	    console.log(error)
	}else{
	    
	    fs.writeFile(process.argv[3]+"/"+"disease_associations_whole"+".json.gz",result, function(err) {
	    	if(err) {
	    	    return console.log(err,"random");
	    	}
	    });
	}
    });
    
});


