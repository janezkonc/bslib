### first load all graphs
## then for each graph remember which nodes are present, also edges
## map this additional info to individual species graphs

import glob

def load_whole_network(folder):
    
    '''
    load first the whole network
    '''

    individual_edges_with_tags = []
    only_edges = []
    with open(folder,"r+") as we:
        for line in we:
            n1,n2,tag,score = line.strip().split()
            individual_edges_with_tags.append((n1,n2,tag,score))
            only_edges.append((n1,n2))
    return only_edges,individual_edges_with_tags

def return_enriched_net(whole_edges,whole_all,tmp_edges,tmp_whole):

    tmp = set(tmp_edges)
    to_add_edges = []
    for ix,el in enumerate(whole_edges):
        if el in tmp:
            to_add_edges.append(whole_all[ix])
    all_edges = to_add_edges + tmp_whole
    return all_edges

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("--networks_folder",default="./data/spin/whole_proteome_graph/species_graphs")
    args = parser.parse_args()
    whole_network_edges,whole_network = load_whole_network(args.networks_folder+"/whole.edgelist")
    
    for network in glob.glob(args.networks_folder+"/*"):
        if network != "whole.edgelist":
            tmp_net_edges,tmp_net_whole = load_whole_network(network)
            enriched_net = return_enriched_net(whole_network_edges,whole_network,tmp_net_edges,tmp_net_whole)
            if len(enriched_net) > 0:
                out_network_string = "\n".join(" ".join(x) for x in enriched_net)
                fx = open(network.replace("species_graphs","tmp_species_graphs"),"w+")
                fx.write(out_network_string)
                fx.close()
           
            print("Finished for {}".format(network))
            
