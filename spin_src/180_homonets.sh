#!/bin/bash

export HOMO_TMP=$SPINDB/homologous_networks/tmp
export HOMO_FINAL=$SPINDB/homologous_networks/final
export HOMO_PAPER=$SPINDB/homologous_networks/paper

rm -Rf $HOMO_TMP $HOMO_FINAL $HOMO_PAPER
mkdir -p $HOMO_TMP $HOMO_FINAL $HOMO_PAPER

# dobimo idmapping.dat.gz z UniProt strani
retry "wget -q -O ${HOMO_TMP}/idmapping.dat.gz ftp://ftp.uniprot.org/pub/databases/uniprot/current_release/knowledgebase/idmapping/idmapping.dat.gz"

# dobimo STRING bazo
retry "wget -q -O ${HOMO_TMP}/protein.links.v10.5.txt.gz https://stringdb-static.org/download/protein.links.v10.5.txt.gz"

# prepare Tax ID specific id mappings
cat $SPINSRC/representative_species_list.txt|parallel "zgrep \"NCBI_TaxID[[:space:]]{}$\" ${HOMO_TMP}/idmapping.dat.gz|gzip > ${HOMO_TMP}/idmapping_{}.dat.gz"

# prepare Uniprot -> STRING mapping
zgrep STRING ${HOMO_TMP}/idmapping.dat.gz|gzip > ${HOMO_TMP}/idmapping_string.dat.gz

# prepare Tax ID specific string databases
cat $SPINSRC/representative_species_list.txt|parallel "python3 spin_src/180_prepare_string_single_species.py --string_links ${HOMO_TMP}/protein.links.v10.5.txt.gz --uniprot_string_map ${HOMO_TMP}/idmapping_string.dat.gz --uniprot_taxid_map ${HOMO_TMP}/idmapping_{}.dat.gz|gzip > ${HOMO_TMP}/protein.links.v10.5_{}.txt.gz"

# gzip some files
cat $SPINSRC/representative_species_list.txt|parallel --progress "cat $SPINDB/prediction_results/{}_predictions.txt|gzip > ${HOMO_TMP}/{}_predictions.txt.gz"
cat $SPINDB/whole_proteome_graph/species_graphs/whole.edgelist|gzip > ${HOMO_TMP}/whole.edgelist.gz

# annotate homologous networks with additional info:
#   * whether an interaction is in STRING,
#   * show original (global) interaction partners as well as homologous proteins,
#   * different scores,
#   * whether interaction is binary (TAG_BIN), predicted (TAG_PRL) or PDB experimental (TAG_PRL_EXP)
cat $SPINSRC/representative_species_list.txt|parallel --progress "python3 spin_src/180_compute_intersections_for_species.py --species_graph ${HOMO_TMP}/{}_predictions.txt.gz --global_spin_graph ${HOMO_TMP}/whole.edgelist.gz --string_links ${HOMO_TMP}/protein.links.v10.5_{}.txt.gz --uniprot_string_map ${HOMO_TMP}/idmapping_string.dat.gz > ${HOMO_FINAL}/{}_final.tsv"
	
# prepare subsets for the paper (homodimers vs. heterodimers, in STRING vs. not in STRING, predicted vs. binary)
ls ${HOMO_FINAL}/*.tsv|parallel --progress --env HOMO_PAPER "180_prepare_subsets.sh {}"
