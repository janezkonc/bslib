// this script takes curated disgenet and transforms it into a json file.

var fs = require('fs');
var zlib = require('zlib');

if (process.argv[2].indexOf('.gz') != -1){
    var LR = require('readline').createInterface({
	input: fs.createReadStream(process.argv[2]).on('error',()=>{	    
	    console.log("Error occured for file:",process.argv[2])
	}).pipe(zlib.createGunzip())
    });


}else{
    var LR = require('readline').createInterface({
	input: require('fs').createReadStream(process.argv[2])
    });           
}

LR.on('error', function (line) {    

    console.log("Error occured for file:",process.argv[2])    

})

var entries = {}


LR.on('line', function (line) {
    
    var parts = line.split("\t")

    if (!entries.hasOwnProperty(parts[1]))
    	entries[parts[1]] = []
    
    entries[parts[1]].push({'gene_id' : parts[0],
    			    'gene_symbol' : parts[1],
    			    'disease_id' : parts[2],
    			    'disease_name' : parts[3],
    			    'score' : parts[4],
    			    'n_snp' : parts[6],
    			    'source' : parts[7]})    
})

LR.on('close', function (line) {	

    var dbentry = []
    for (var k in entries){
	fs.writeFileSync(process.argv[3]+"/"+k+".json", JSON.stringify(entries[k]))
	entries[k].forEach((entry)=>{
	    dbentry.push(k+"\t"+entry['disease_name']+"\t"+entry['disease_id']+"\n")
	})	
    }

    var path = process.argv[4]+"/DGN.txt" // save as annotation file..
    console.log("Constructing the enrichment database: DGN",path)  
    var dbset = new Set(dbentry);
    var outarray = Array.from(dbset).join("")
    fs.appendFileSync(path, outarray);
});

