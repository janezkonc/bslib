var async = require('async');
var zlib = require('zlib');
var fs = require('fs');
var readline = require('readline');

var core_path = process.argv[3];
var ligands_folder = process.argv[4];
var jsons = process.argv[5];
var bresi_folder_path = process.argv[6];
var local_index_folder  = process.argv[7];

var uniprot_to_pdb_index = core_path+"/dbindex/index_OTHER.json";
var gene_description_folder = core_path+"/geneDescriptionJson/";
var sifts_mapping = core_path+"/dbindex/sifts_mapping.json.gz";
var ligandsdb = ligands_folder+"/ligands_";
var jsondb = jsons+"/"
var nrd_folder = core_path+"/non_redundant_pdbs/bc-100.out";
var dgn_folder = core_path+"/gene_disease_associations/";

var tmpset = fs.readFileSync(nrd_folder, "utf8").split("\n");
var tset = [];

tmpset.forEach((en)=>{
    var pts = en.split(" ")
    pts.forEach((ex)=>{
	tset.push(ex);
    })
});

var nrdset = new Set(tset);
var waterTreshold = 0.90 // water ligands filter based on conservation
var uniprot = process.argv[2];
var sifts_mapping_global;

read_json_direct(sifts_mapping, function() {

    obtain_index_pdbs(uniprot, function(err,result) {
	
        if (err) {
            return err;

        } else {
            var centerNode = (result.gene_name || "NaN").trim()+"_"+(result.species || "")+"_"+uniprot;
	    fs.appendFileSync(local_index_folder, centerNode+"\n");
            var PDBs = result.associated_pdb_chains;
            var proteins = [];
            var edges = [];
            var pdbCount = Object.keys(PDBs).length;

            // containers
            var jobs = [];
	    
            // this adds the jobs for the first recursive step
            for (var pdb in PDBs) {
                pdb = pdb.substring(0,pdb.length-1).toLowerCase()+pdb.substring(pdb.length-1);
                jobs.push(pdb);
            }

            // reduce bresi?
            async.mapLimit(jobs,30,concatPDBligands,(e,r)=>{

                // containers
                var jobs2 = []
                var lmap = {}
		
                // this adds the jobs for the first recursive step
                for (var i=0; i<proteins.length; i++){
                    var qname = sifts_mapping_global[proteins[i].ligand_name];
                    if (qname != undefined) {
                        qname = qname.trim();
                        if (lmap.hasOwnProperty(qname)){
                            lmap[qname].push(i);
                        }

                        else {
                            lmap[qname] = [i];			    
                            jobs2.push(qname)
                        }
                    }                                   
                }

		// jobs2 so proteinski ligandi.
                async.mapLimit(jobs2,50,readGeneNameAndSpecies,(e,r)=>{
                    for(var job =0;job<jobs2.length;job++){
                                
                        if (r[job] != undefined) {
                                        
                            var geneName = (r[job].gene_name || "NaN").trim()+"_"+(r[job].species || "")+"_"+jobs2[job];
			    var experimental_structure = false;
                            var sum_zscore = 0;
                            var snps_in_binding_site = []
			    var max_zscore = 0;
			    var experimental_structures = []
                            for (var i=0; i<lmap[jobs2[job]].length; i++) {
                                var protein = proteins[lmap[jobs2[job]][i]];
				
				snps_in_binding_site.push(protein.interface_aa_snp);
                                var zscore = protein.zscore;
				
				if (protein.gp_query == protein.superimposed_from){
		//		    console.log(protein.ligand_name,protein.superimposed_from,protein);
				 //   experimental_structures = [protein.ligand_name,protein.superimposed_from]
				    experimental_structure = true;
				}

				if (zscore> max_zscore)
				    max_zscore = zscore;

				sum_zscore += protein.zscore;
                            }
			    
			    var snp_string = (function(snp){
				var selection = [];
				snp.forEach((x)=>{
				    x.forEach((y)=>{
					var change = y.uniprot.change;
					var source_db = y.uniprot.source_id;
					var clin_sig = y.uniprot.clin_sig;
					var pdb_id = y.pdb.pdb_id+y.pdb.chain_id+"_"+y.pdb.resi
					if (source_db != "-" && source_db != "" && clin_sig != "")
					    selection.push([change,source_db,clin_sig].join(";"));
				    });
				});

				// remove duplicates..
				var set = new Set(selection);
				let array = Array.from(set);
				return array;
				
			    })(snps_in_binding_site).join("|");

			    var int_tag = "TAG_PRL";
			    if (experimental_structure)
				int_tag = "TAG_PRL_EXP";

                            var ex = [centerNode,geneName,int_tag,max_zscore,snp_string];
			    console.log([ex[0],ex[1],ex[2],ex[3],ex[4]].join("\t")); // n1 n2 mean_Z
//			    console.log([ex[0],ex[1],ex[2]].join("\t"),experimental_structures); // n1 n2 mean_Z
			    fs.appendFileSync(local_index_folder, ex[0]+"\n"+ex[1]);
                        }
                    }
		    // edges.forEach((ex)=>{
		    // 	console.log([ex[0],ex[1],ex[2],ex[3],ex[4]].join("\t")); // n1 n2 mean_Z
		    // 	fs.appendFileSync(local_index_folder, ex[0]+"\n"+ex[1]);
		    // });
                });
            });
            
            function readGeneNameAndSpecies(uniprot_name,callback) {
                readGeneDescription(uniprot_name,(err,res)=> {
                    if (err) {
                        callback(null,null)
                    }
                    else {
                        callback(null, {species: res.species, gene_name: res.gene_name})
                    }
                })
            }

            function concatPDBligands(pdb_name,callback) {
                var gpName = jsondb+pdb_name+".json.gz"
                query_pdb_genprobisdb(gpName,(err,tmpresult)=>{
		    
                    query_pdb_probisdb(pdb_name,tmpresult,(probis_results)=>{
                        pdbCount--;
                        proteins.push.apply(proteins, probis_results.proteins);
//                        console.log("pdb concat done for "+pdb_name+", "+pdbCount+" pdbs left.")
                        callback(null,true)
                    })
                });                    
            }
        }
    });
});
        
function obtain_index_pdbs(query,callback){

    readGeneDescription(query, function(err,result) {
        if (err) {
            var pdb_index = JSON.parse(fs.readFileSync(uniprot_to_pdb_index, 'utf8'));
            var associated_pdbs = pdb_index[query];
            var results = {associated_pdb_chains : {}, gene_name: "", species: ""};	    	    
            associated_pdbs.forEach((entry)=>{
                if (nrdset.has(entry)){
                    results.associated_pdb_chains[entry] = ""
                }
            })	    
            callback(false,results);
	    
        }else{

            var checked_ranges = {};
            for (var k in result.associated_pdb_chains){
                var query_pdb = k.substring(0,4)+"_"+k.substring(4,5);
                var cRange = result.associated_pdb_chains[k].chain_range;
                if (!checked_ranges.hasOwnProperty(cRange))
                    checked_ranges[cRange] = 0;
		
                checked_ranges[cRange]++;
            
                if (checked_ranges[cRange] > 4){   
                     delete result.associated_pdb_chains[k];
                }

                // if (!nrdset.has(query_pdb)){
                //     delete result.associated_pdb_chains[k];
                // }
		
            }
	    
            callback(false,result);
        }
    })
}

function readGeneDescription(uniprot_name,cb) {
    
    var g_file = gene_description_folder+uniprot_name+".json.gz";    
    let lineReader = readline.createInterface({
        input: fs.createReadStream(g_file).on('error',()=>{
            cb(true,"NOT FOUND");
        }).pipe(zlib.createGunzip()).on('error',()=>{
            logger("err");cb(true,"INTERNAL ERROR");
        })
    });

    let buff = ""
    lineReader.on('line', (line) => {
        buff+=line
    });

    lineReader.on('close',()=>{   
        cb(false,JSON.parse(buff));	
	
    })
    
    lineReader.on('error',()=>{
        cb(true,"TEST")
    })
}

function read_json_direct(query_path,callback){
    
    var lineReader = require('readline').createInterface({
	input: fs.createReadStream(query_path).on('error',(err)=>{
//	    console.log("couldn't read the file..",err)
	}).pipe(zlib.createGunzip())
    });

    lineReader.on('error', function (err) {

//	console.log(err);
    })

    var data = "";

    lineReader.on('line', function (line) {
	data+=line
    })

    lineReader.on('close',()=>{
        sifts_mapping_global = JSON.parse(data);
        callback()
    })
}

// get variant info
function query_pdb_genprobisdb(query_name,callback){
    read_json(query_name,(outjson_AA)=>{
	var protein = {
            'bresi' : [],
	    'snp' : []
	}
	
	// zmapiraj snpje na strukture..	
	var bresi_file = bresi_folder_path+"/bresi_"+query_name.split("/").splice(-1);
	
	read_json(bresi_file,(outjson)=>{
	    if (outjson_AA != null){
		outjson_AA.forEach((ent)=>{
		    if (ent.snp){
			ent.snp.pdb = ent.protein.pdb;
			protein.snp.push({'snp_info':ent.snp,'pdb_resi':ent.protein.pdb.resi})
		    };
		});
	    }
	    
            if (outjson == null){
		callback(null,protein)
            }else{
		// assign
		protein.bresi = outjson
		
		// pass on 
		callback(null,protein)
            }
	});
    });
}   

function read_json(query_path,callback){

    var lineReader = require('readline').createInterface({
	input: fs.createReadStream(query_path).on('error',()=>{
	    callback(null,"NODATA")
	}).pipe(zlib.createGunzip())
    });

    lineReader.on('error', function (line) {

	callback(null,"NODATA")
    })

    var data = ""

    lineReader.on('line', function (line) {

	data+=line
    })

    lineReader.on('close',()=>{

	datafile = JSON.parse(data)

	callback(datafile)
    })
}

// get bs info
function query_pdb_probisdb(pdb_name,source_bs,callback){

    var bresi = source_bs.bresi;
    var snps = source_bs.snp;
    var query_name = ligandsdb+pdb_name+".json.gz";
    var tmpresult = {};
    var snp_map = {};

    snps.forEach((x)=>{
	if (!snp_map.hasOwnProperty(x.pdb_resi)){
	    snp_map[x.pdb_resi] = [x.snp_info];
	}	    
    });

    read_json(query_name,(outjson)=>{

	// first, screen ligand types	
	var ligands = {};
	var genes = [];
	
	if (outjson == null){
	    
	    callback("ERROR")
	    
	}else{

            var bs_map = {}

	    // map source bs info to individual keys
	    bresi.forEach((bs)=>{
		var key = bs['ltype']+"_"+bs['bs_id']
		if (!bs_map.hasOwnProperty(key)){
		    bs_map[key] = []
		}
		bs_map[key].push(bs['resi'])
	    });

	    // this adds the jobs for the first recursive step
	    for (var i=0; i<outjson.length; i++){
		
		var item = outjson[i];
		var lid = item.lig_id;
		var components = lid.split("_");
		var type = item.ltype;
		var zscore = item.z_score;
		var binding_site = item.bs_id;
		var pdbid_host = components[1]+components[2];
		var lname = components[3];
		var tmpLig = components[1].toUpperCase()+"_"+components[2];
		var num_occurrence = item.num_occ;
		var cons = item.cons;

		// find AAs, associated with this binding site..
		var bs_mk = type+"_"+binding_site
		var bsite_aa = bs_map[bs_mk]		
		
		if (bsite_aa == undefined)
                    bsite_aa = [];

		var snp_intersection = (function(a,b){
		    var intersection = []
		    bsite_aa.forEach((x)=>{
			if (a[x] != undefined){
			    intersection.push(a[x][0]);
			};
		    });
		    return intersection;		   
		})(snp_map,bsite_aa);

		if (type == "protein") {
                    
                    lid = "ligand_"+lid;
                    var components = lid.split("_");
		    var query = item.pdb_id + item.chain_id;
                    var qname = jsondb+query+".json.gz";
		    var tmpQuery = item.pdb_id.toUpperCase()+"_"+item.chain_id;
		    
                    if (nrdset.has(tmpQuery)){
			genes.push({
                            zscore : zscore,
                            superimposed_from : pdbid_host,
                            bsid : binding_site,
			    conservation : cons,
			    interface_aa_snp : snp_intersection,
			    num_occ : num_occurrence,
                            ligand_type : type,
                            ligand_name :  query,
                            interface_aa : bsite_aa,
                            lig_id : lid,
                            gp_query : pdb_name
			});
                    };
		}
		
		else if (type != "water" || (type == "water" && cons >= waterTreshold)) {       


		    if (type == "nucleic"){
			if (lname == "A" || lname == "T" || lname == "G" ||  lname == "C" || lname == "U"){
			    lname = "RNA";
			}else{
			    lname = "DNA";
			}			
		    }
		    
                    if (!ligands.hasOwnProperty(lname)) {
			ligands[lname] = {'ligand_name': lname, 'ligand_type': type, min_zscore: Infinity, max_zscore: 0, sum_zscore: 0, 'core_info': [], gp_queries: {}}
                    }
                    
                    if (ligands[lname].gp_queries.hasOwnProperty(pdb_name)) {
			if (ligands[lname].gp_queries[pdb_name].indexOf(binding_site) == -1)
                            ligands[lname].gp_queries[pdb_name].push(binding_site)
                    }
                    else {
			ligands[lname].gp_queries[pdb_name] = [binding_site];
                    }


                    ligands[lname].min_zscore = Math.min(zscore, ligands[lname].min_zscore);
                    ligands[lname].max_zscore = Math.max(zscore, ligands[lname].max_zscore);
                    ligands[lname].sum_zscore += zscore;
		    
                    ligands[lname].core_info.push({
			zscore : zscore,
			conservation : cons,
			superimposed_from : pdbid_host,
			interface_aa : bsite_aa,
			interface_aa_snp : snp_intersection,
			num_occ : num_occurrence,
			bsid : binding_site,
			ligand_type : type,
			ligand_name : lname,
			ligand_string : lid,
			gp_query: pdb_name
                    })
		}
            }
            
            tmpresult.ligands = ligands
            tmpresult.proteins = genes;
            callback(tmpresult)
	}
    })    
}
