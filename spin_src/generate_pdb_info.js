// this generates json-s consisting of PDBs, ligands and disease associations..

// Created by Blaz Skrlj of KI, 2017
// these are the key algorithms behind the NetProBiS platform
// MIT license applies.

var exec = require('child_process').exec;
var fs = require('fs');
//var PriorityQueue = require('priorityqueuejs');
var zlib = require('zlib');
var async = require('async')

// system-level databse - this is localhost-specific!
var ligandsdb = "../genprobisdb/ligands/ligands_"

// these are local databases..
var high_level_graph = "../gene_graph_individual/"
var tsvdb = "../genprobisdb/tsv/"
var jsondb = "../genprobisdb/json/"
var bresidb = "../genprobisdb/bresi/bresi_"
var mapping_folder = "../pdb_mapping/uni_pdb.json"
var nrd_folder = "..//non_redundant_pdbs/bc-100.out"
var dgn_folder = "../gene_disease_associations/"

var tmpset = fs.readFileSync(nrd_folder, "utf8").split(" ")
var nrdset = new Set(tmpset);

module.exports = {

    // this is the backup function, reading tsv-s
    // skripta, ki to vnaprej poracuna za vse znane PDBje, ki jih imamo v GenProBiSdb-ju.
    returnJson_tsv : function(query_path, cb){
	query_single_entry_tsv(tsvdb+query_path+".tsv.gz",(err,result)=>{
	    if (result == "NODATA"){
		cb(result)
	    }else{
		var ligands = result['ligands']
		var to_parse = []
		var gene_names = []
		var firstgene = result['receptor_properties']['gene']
		gene_names.push(firstgene)
		for (var key in ligands){
		    var type = ligands[key][0]['type']
		    if (type == "protein"){
			var qname = key
			var tmpLig = key.substr(0,4).toUpperCase()+"_"+key.substr(4,5)
			if (nrdset.has(tmpLig)){
	    		    to_parse.push(qname)
			}
		    }
		}
		console.log("Size: ",to_parse.length)
		source_processed = true
		console.log(result)
		cb(result)
	    }
	})
    },

    // this is the main, json-based constructor
    returnNetworkFromJson : function(query, cb){

	// filename definitions
	var gpName = jsondb+query+".json.gz"
	var pdName = ligandsdb+query+".json.gz"

	// TODO -- dodaj tukaj bresi informacijo.
	
	query_pdb_genprobisdb(gpName,(err,tmpresult)=>{
	    query_pdb_probisdb(pdName,tmpresult,(result)=>{
		cb(result)
	    })
	})
    },

    returnPath : function(q1, q2, cb){

	// custom Dijsktra run - works for any two terms, as long as types are specified.
	dijkstra_truncated(q1,q2,(result)=>{	    	    
	    cb(result)
	})
    },

    returnTFnet : function(input, path, cb){

	read_tf_csv(input,path,(err,matches)=>{
	    if(err != null)
		console.log(err)
	    cb(matches)
	})	
    }
}

//////////////////////////// crawler ////////////////////////////////


function readLines(input, func) {
    var remaining = []
    input.on('line', function(data) {
	remaining.push(data)
    });

    input.on('end', function() {
	func(remaining)
    });

    input.on('error',function(){
	func(undefined)
    });    
}

function dijkstra_truncated(q1,q2,cb){

    fs.readdir(high_level_graph, (err, files) => {
	
	var type = "" // glede na to iscemo graf!
	var pq = new PriorityQueue()
	var initial = {}
	var add_info = {}
	
	files.forEach(file => {	   
	    var candidate = file.split("_")[0]
	    var path = high_level_graph+file
	    
	    if (candidate == q1){
		initial = {'distance' : 0,
	     		 'prev' : undefined,
	     		 'whole_path' : path,
			 'node' : candidate}	    	
	    }else{		
		pq.push({'distance' : 9999999,
	     		 'prev' : undefined,
	     		 'whole_path' : path,
			 'node' : candidate})
	    }	    	    
	})	

	// add the source node
	pq.push(initial)
	trigger = false
	while (pq.size() > 0) {	    

	    // do the neigh_search and relax the edges
	    var candidate = pq.pop()
	    var nrs = (function(pth){
		var data = fs.readFileSync(pth['whole_path'], 'utf8')
		var clist = data.split("\n")
		return clist
	    })(candidate)
	    
	    nrs.forEach((nr)=>{
		var cd = nr.split("_")[0]

		if (cd == q1){
		    cb(cd)
		    trigger =  true
		    // cb backtrace
		}
		
		try{
		    // this is linear time and is just a test!
		    if (candidate['distance']+1 < pq.node_mapping[cd]){ // +1 is for unweighted edges
			// update the cd's distance
			pq.decrease(cd,(candidate['distance']+1))

			// update backtrace 
		    }
		}catch(err){}
	    })	    
	}

	if (!trigger)
	    cb("test")
    })  
}

// get variant info
function query_pdb_genprobisdb(query_name,callback){

    read_json(query_name,(outjson)=>{
	var protein = {'snp_info' : [],
		       'basic_info' : {},
		       'ligands' : [],
		       'bresi' : [],
		       'disease_associations' : []}


	// this serves for mapping purposes..
	var snplist = []
	
	try{
	    outjson.forEach((item)=>{
		
		if (item['snp'] != undefined){
//		    protein['snp_info'].push(item['snp'])
		    snplist.push(item)
		}

		if (item['protein']['uniprot']['ac'] != null){
		    
		    protein['basic_info']['uniprot'] = item['protein']['uniprot']['ac']
		    protein['basic_info']['gene'] = item['protein']['uniprot']['gene_name']
		    protein['basic_info']['species'] = item['protein']['uniprot']['organism']
		    protein['basic_info']['pdb_id'] = item['protein']['pdb']['pdb_id']
		    protein['basic_info']['pdb_chain'] = item['protein']['pdb']['chain_id']
		    
		}
	    })	    
	}catch(err){}
//	console.log(protein)
	var fname = dgn_folder+protein['basic_info']['gene']+".json"
	try{
	    protein['disease_associations'] = fs.readFileSync(fname, 'utf8');
	}catch(err){
	    protein['disease_associations'] = []
	}
	
	// zmapiraj snpje na strukture..	
	var bresi_file = "./data/genprobisdb/bresi/bresi_"+query_name.split("/")[4]
	read_json(bresi_file,(outjson)=>{

	    if (outjson == null){
		callback(null,protein)
	    }else{
		// map SNPs here..
		var updated_snps = (function(snplist,outjson){

		    var out = []
		    for(var x = 0; x < snplist.length;x++){
			var full_entry = snplist[x]
			var pdb_info_resi = full_entry['protein']['pdb']['resi']
			var snp_entry = full_entry['snp']
			snp_entry['binding_site'] = []
			for(var k = 0; k < outjson.length;k++){

			    // match, add key, bresi num.		    
			    var tmp_bresi = outjson[k] //{ bs_id: 4, ltype: 'ion', resi: '162' }			
			    if (tmp_bresi['resi'] == pdb_info_resi){
				snp_entry['binding_site'].push(tmp_bresi)	    
			    }
			}
			
			snp_entry['pdb'] = full_entry['protein']['pdb']
			out.push(snp_entry)
		    }

		    return out
		})(snplist,outjson)

		// assign
		protein['snp_info'] = updated_snps
		protein['bresi'] = outjson

		// pass on 
		callback(null,protein)
	    }
	})
    })
}

// get bs info
function query_pdb_probisdb(query_name,tmpresult,callback){

    read_json(query_name,(outjson)=>{

	// first, screen ligand types	
	var ligands = {}
	
	if (outjson == null){
	    
	    callback("ERROR")
	    
	}else{

	    var source_bs = tmpresult['bresi']
	    var bs_map = {}

	    // map source bs info to individual keys
	    source_bs.forEach((bs)=>{
		var key = bs['ltype']+"_"+bs['bs_id']
		if (!bs_map.hasOwnProperty(key)){
		    bs_map[key] = []
		}
		bs_map[key].push(bs['resi'])
	    })

	    // loop through individual ligands
	    outjson.forEach((item)=>{

		var lid = item['lig_id']
		var components = lid.split("_")
		var type = components[0]
		var zscore = item['z_score']
		var pdbid = item['pdb_id']
		var pdbid_chain = item['bs_chain_id']
		var binding_site = item['bs_id']
		var pdbid_host = pdbid+pdbid_chain
		var lname = components[3]
		var tmpLig = components[1].toUpperCase()+"_"+components[2]

		// find AAs, associated with this binding site..
		var bs_mk = type+"_"+binding_site
		var bsite_aa = bs_map[bs_mk]
		if (bsite_aa == undefined)
		    bsite_aa = []
		if (type == "protein"){
		    if (nrdset.has(tmpLig)){
			lid = "ligand_"+lid
			if (!ligands.hasOwnProperty(lid)) {
			    ligands[lid] = {'zscore' : zscore,'superimposed_from' : pdbid_host,
					    'bsid' : binding_site,'ligand_type' : type,
					    'ligand_name' : lname, 'interface_aa' : []}
			}					
			ligands[lid]['interface_aa'] = bsite_aa;
		    }
		    
		}else{

		    if (!ligands.hasOwnProperty(lname)) {
			ligands[lname] = {'ligand_name': lname, 'ligand_type': type, zscore: 0, 'core_info': []}
		    }

		    ligands[lname].zscore = Math.max(zscore, ligands[lname].zscore);
		    ligands[lname].core_info.push({'zscore' : zscore,
						   'superimposed_from' : pdbid_host,
						   'interface_aa' : bsite_aa,
						   'bsid' : binding_site,
						   'ligand_type' : type,
						   'ligand_name' : lname,
						   'ligand_string' : lid})
		}
	    })

	    // containers
	    var jobs = []
	    var lmap = {}

	    // this adds the jobs for the first recursive step
	    for (var lig in ligands){
		if (ligands[lig]['ligand_type'] == "protein"){
		    var components = lig.split("_")
		    var query = components[2]+components[3]
		    var qname = jsondb+query+".json.gz"
		    lmap[qname] = lig
		    var tmpLig = components[2].toUpperCase()+"_"+components[3]
		    if (nrdset.has(tmpLig)){
			jobs.push(qname)
		    }
		}
	    }

	    // reduce bresi?
	    async.map(jobs,query_pdb_genprobisdb,(e,r)=>{
		for(var job =0;job<jobs.length;job++){
		    if (Object.keys(r[job]['basic_info']).length > 0){
			ligands[lmap[jobs[job]]]['snp_info'] = r[job]['snp_info']
			ligands[lmap[jobs[job]]]['basic_info'] = r[job]['basic_info']
			ligands[lmap[jobs[job]]]['ligands'] = r[job]['ligands']
			ligands[lmap[jobs[job]]]['bresi'] = r[job]['bresi']
		    }
		}	

		tmpresult['ligands'] = ligands
		callback(tmpresult)
		
	    })
	}
    })    
}

function query_single_entry_tsv(query_path,callback){

    // for each SNP, add it!

    var lineReader = require('readline').createInterface({
	input: fs.createReadStream(query_path).on('error',()=>{
	    callback(null,"NODATA")
	}).pipe(zlib.createGunzip())
    });

    var tmpdict = {
	'receptor_id' : "",
	'receptor_properties' : {},
	'ligands' : {}
    }

    var processed_ligands = []

    lineReader.on('error', function (line) {

	callback(null,"NODATA")
    })

    lineReader.on('line', function (line) {

	parts = line.split("\t")

	//definitions
	var evolution = parts[0] //*
	var pdb_receptor =parts[17]+parts[15] //*
	var ligand_string = parts[6] //*
	var interaction_ligand_type = parts[10]	//*
	var ligand_receptor = "" //*
	var species = parts[22] //*
	var phenotype_pubmed = parts[36] //*
	var uniprot_symbol = parts[20] //*
	var gene_symbol = parts[21] //*
	var clinvar = parts[24] //*
	var lig_rec_distance = parts[2] //*
	var snp_description = parts[24] // * this too
	var snp_id = parts[37] //* this is to mark
	var snp_disease_source = parts[36] //*
	var snp_disease_type = parts[35] //*
	var snp_predicted_significance = parts[39] //*
	var writeTrigger = 0 // *

	if (interaction_ligand_type == "protein"){
	    ligand_receptor = ligand_string.split("_").slice(2,4)

	    //get representative ligands here!
	    tmpLig = ligand_receptor[0].toUpperCase()+"_"+ligand_receptor[1]
	    if (nrdset.has(tmpLig)){
	    	ligand_receptor = ligand_receptor.join("")
	    	writeTrigger = 1
	    }

	}else if (interaction_ligand_type == "compound"){
	    ligand_receptor = ligand_string.split("_")[4]
	    writeTrigger = 1
	}else if (interaction_ligand_type == "nucleic"){
	    ligand_receptor = ligand_string.split("_")[4]+"_"+ligand_string.split("_")[6]
	    writeTrigger = 1
	}else if (interaction_ligand_type == "ion"){
	    ligand_receptor = ligand_string.split("_")[4]
	    writeTrigger = 1
	}else{} // additional ligands, such as H2O come here..

	if (writeTrigger){

	    var ligand_property = {
		'type' : interaction_ligand_type,
		'ligname' : ligand_receptor,
		'evolution' : evolution,
		'snp_id' : snp_id,
		'snp_disease_source' : snp_disease_source,
		'snp_disease_type' : snp_disease_type,
		'snp_predicted_significance' : snp_predicted_significance
	    }

	    tmpdict['receptor_id'] = pdb_receptor
	    tmpdict['receptor_properties']['gene'] = gene_symbol
	    tmpdict['receptor_properties']['uniprot'] = uniprot_symbol
	    tmpdict['receptor_properties']['species'] = species

	    var lig_id = ligand_receptor
	    if (lig_id in tmpdict['ligands']){


	    }else{

		tmpdict['ligands'][lig_id] = []
	    }

	    if (processed_ligands.indexOf(ligand_receptor) != -1){
	    }else{
		processed_ligands.push(ligand_receptor)
		if(tmpdict['ligands'][lig_id].indexOf(ligand_property) == -1){
		    tmpdict['ligands'][lig_id].push(ligand_property)
		}
	    }
	}
    })
    ,
    lineReader.on('close', function (line) {
	callback(null,tmpdict)
    });
}


function read_json(query_path,callback){

    var lineReader = require('readline').createInterface({
	input: fs.createReadStream(query_path).on('error',()=>{
	    callback(null,"NODATA")
	}).pipe(zlib.createGunzip())
    });

    lineReader.on('error', function (line) {

	callback(null,"NODATA")
    })

    var data = ""

    lineReader.on('line', function (line) {

	data+=line
    })

    lineReader.on('close',()=>{

	datafile = JSON.parse(data)

	callback(datafile)
    })
}

function read_tf_csv(query,query_path,cb){

    console.log(query_path)
    var lineReader = require('readline').createInterface({
	input: fs.createReadStream(query_path).on('error',()=>{
	    cb(null,"NODATA")
	}).pipe(zlib.createGunzip())
    });

    lineReader.on('error', function (line) {
	
	cb(null,"NODATA")
    })


    var entries = []

    lineReader.on('line', function (line) {

	parts = line.split(",")
	var g1 = parts[0]
	var g2 = parts[1]
	if (g1 == query || g2 == query){

	    entries.push({'first_partner' : g1,
			  'second_partner' : g2,
			  'PMID' : parts[2],
			  'method':parts[3],
			  'pathways':parts[4]})

	}
	
    })

    lineReader.on('close',()=>{
	cb(null,entries)
    })

}

