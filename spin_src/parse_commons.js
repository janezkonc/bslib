// this script generates JSON files, corresponding to pathway commons

// this script takes curated disgenet and transforms it into a json file.

var fs = require('fs');
var zlib = require('zlib');


if (process.argv[2].indexOf('.gz') != -1){
    var LR = require('readline').createInterface({
	input: fs.createReadStream(process.argv[2]).on('error',()=>{	    
	    console.log("Error occured for file:",process.argv[2])
	}).pipe(zlib.createGunzip())
    });


}else{
    var LR = require('readline').createInterface({
	input: require('fs').createReadStream(process.argv[2])
    });           
}




LR.on('error', function (line) {    

    console.log("Error occured for file:",process.argv[2])    

})

var entries = {}

LR.on('line', function (line) {
    
    var parts = line.split(/\s+/)
    if (!entries.hasOwnProperty(parts[0]))
	entries[parts[0]] = []
    entries[parts[0]].push({'interaction_type': parts[1],'interaction_partner' : parts[2]})
    
})

LR.on('close', function (line) {	

    var dbentry = []
    for (var k in entries){	
    	//fs.writeFileSync(process.argv[3]+"/"+k+".json", JSON.stringify(entries[k]))
	entries[k].forEach((entry)=>{
	    dbentry.push(entry['interaction_partner']+"\t"+entry['interaction_type']+"\n")
	})
    }

    var path = process.argv[4]+"/PC.txt"
    console.log("Constructing the enrichment database: Commons",path)  
    var dbset = new Set(dbentry);
    var outarray = Array.from(dbset).join("")
    fs.appendFileSync(path, outarray);  
});


