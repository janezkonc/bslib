

def read_prediction_output_file(fname):

    with open(fname) as fn:
        for line in fn:
            line = line.strip().split()
            pobject = {}

            try:
                pobject['partner_first'] = line[1]
                pobject['partner_second'] = line[2]
                pobject['eval_1'] = float(line[5])
                
                pobject['seq_1'] = float(line[6])
                pobject['cov_1'] = float(line[8][:-1])
                
                pobject['eval_2'] = float(line[11])
                pobject['seq_2'] = float(line[12])
                pobject['cov_2'] = float(line[14][:-1])

                pobject['old_first'] = line[15]
                pobject['old_second'] = line[16]

                name_first = pobject['partner_first'].split("_")[0]
                name_second = pobject['partner_second'].split("_")[0]

                name_old_first = pobject['old_first'].split("_")[0]
                name_old_second = pobject['old_second'].split("_")[0]

                if name_first == name_old_first:
                    pobject['partner_first'] = pobject['old_first']

                if name_second == name_old_second:
                    pobject['partner_second'] = pobject['old_second']

                #print(pobject['partner_first'],pobject['seq_1'],pobject['seq_2'],pobject['old_first'])
                
            except Exception as err:
                print(err)
                yield False
                

if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("--input_predictions",default="default")
    parser.add_argument("--output_folder",default="default")
    args = parser.parse_args()    

    
    for prediction_object in read_prediction_output_file(args.input_predictions):
        
        pass

    pass
