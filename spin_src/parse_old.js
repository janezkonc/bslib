// this script parses whole uniprot database into individual JSON-s, used on the frontend
// individual entries are saved based on gene symbols.
// tmpIndividualRecords/entry100098.txt
// node parse_uniprot2_json.js testtp53.txt tmp

var fs = require('fs');
var zlib = require('zlib');
var ggraphFolder = "../gene_graph_individual/"
var outFolder = process.argv[3]

if (process.argv[2].indexOf('.gz') != -1){
    var LR = require('readline').createInterface({
	input: fs.createReadStream(process.argv[2]).on('error',()=>{	    
	    console.log("Error occured for file:",process.argv[2])
	}).pipe(zlib.createGunzip())
    });


}else{
    var LR = require('readline').createInterface({
	input: require('fs').createReadStream(process.argv[2])
    });           
}


LR.on('error', function (line) {    

    console.log("Error occured for file:",process.argv[2])

})

var entryCounter = 0
var idTrigger = false // this is to recognize the beginnings
var writeTrigger = false // this is to write
var currentGeneName = "" // gene name
var currentSpecies = "" // species
var geneDescription = {} // dict of terms
var currentTerm = ""
var pdbDict = {} // dictionary of PDB ligands with corresponding information
var expInt = []
var uniID = []
var rxTrigger = false
var currentExperimentalPartner = ""

LR.on('line', function (line) {
    
    var lParts = line.split(/\s+/)
    if (lParts[0] == "ID"){
	idTrigger = true
	entryCounter++
	if (entryCounter % 1000 == 0)
	    console.log("Parsed: ",entryCounter,"proteins..")
	
    }
    
    if (idTrigger == true){

	if (lParts[0] == "AC"){
	    lParts.forEach((part)=>{
		if (part != "AC"){
		    var px = part.replace(";","")
		    uniID.push(px)
		}
	    })
	}
	
	if (lParts[0] == "OS")
	    currentSpecies = lParts.slice(1,lParts.length).splice(0,2).join("_")
	
	if(lParts[0] == "GN"){
	    currentGeneName = lParts[1].split("=")[1]
	    try{currentGeneName = currentGeneName.replace(";","")}catch(err){}
	}

	if (lParts[0] == "CC"){
	    if (lParts[1] == "-!-"){
		currentTerm = ((term_array)=>{
		    if (term_array[2].indexOf(":") != -1){
			return term_array[2].replace(":","")
		    }else{
			return term_array[2]+" "+term_array[3].replace(":","")
		    }		    				   
		})(lParts)
		if (geneDescription[currentTerm] == undefined)
		    geneDescription[currentTerm] = ""		
		geneDescription[currentTerm] += lParts.slice(3,lParts.length).join(" ")		
	    }else{	    
		geneDescription[currentTerm] += lParts.slice(1,lParts.length).join(" ")
	    }
	}
	
	
	if (lParts[0] == "DR" && lParts[1] == "PDB;"){
	    
	    var pdbChain = lParts[2].replace(";","")+lParts[5].replace(";","").split("=")[0]
	    var method = lParts[3].replace(";","")
	    var resolution = lParts[4]
	    var range = lParts[6]
	    pdbDict[pdbChain] = {'detection_method' : method,
				 'detection_resolution' : resolution,
				 'chain_range' : range}

	    
	}

	if (lParts[0] == "RP" && lParts[1] == "INTERACTION"){

	    rxTrigger = true	    
	    var partner = ""
	    if (lParts[3].indexOf('PHOSPH') != -1){
		partner = lParts[3]+" "+lParts[4].replace(".","")
	    }else{
		partner = lParts[3].replace(".","")
	    }

	    if (partner != ""){
		if(partner.indexOf(";") != -1){
		    partner.replace(";","")
		}

		if(partner.indexOf(",") != -1){
		    partner.replace(",","")
		}		
//		expInt.push(partner)
		currentExperimentalPartner = partner.replace(",","")
	    }
	}

	if (rxTrigger){	    
	    if (lParts[0] == "RX"){		
		expInt.push({'pmid' : lParts[1],
			     'partner_name': currentExperimentalPartner})
		console.log(expInt)
		rxTrigger=false
	    }
	}

	if (lParts.indexOf("//") != -1){


	    uniID.forEach((uniprotID)=>{
		var outJson = new Object()
		outJson['gene_name'] = currentGeneName
		outJson['uniprot_name'] = uniprotID
		outJson['species'] = currentSpecies
		outJson['description'] = geneDescription 
		outJson['associated_pdb_chains'] = pdbDict
		outJson['experimental_partners'] = expInt
		
		zlib.gzip(JSON.stringify(outJson), function (error, result) {
	    	    if (error){
	    		console.log(error)
	    	    }else{
			try{
	    		    if (currentGeneName.indexOf("/") != -1)
	    			currentGeneName.replace("/","_")
	    		    if (currentGeneName.indexOf(",") != -1)
	    			currentGeneName.replace(",","")
			}catch(err){
			    console.log(err, currentGeneName,uniprotID)
			}
			
	    		fs.writeFile(outFolder+"/"+uniprotID+".json.gz",result, function(err) {
	    	    	    if(err) {
	    	    		return console.log(err,"random");
	    	    	    }
	    		});
	    	    }
		})
		
	    });

	    // reset triggers
	    
	    idTrigger = false
	    uniID=[]
	    
	}
    }        
})

LR.on('close', function (line) {	

    
    
});

