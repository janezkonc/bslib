## set of calls to node2vec for network embedding-based visualization.
import argparse
import os
from py3plex.core import multinet
from py3plex.wrappers import train_node2vec_embedding
from py3plex.visualization import embedding_tools

#import json

def compute_embedded_coordinates(edgelist,output_file,n2v_binary=""):
    
    #os.remove("tmp.emb")

    encoded_network = "./tmp_encoded_network.edgelist"
    
    ## empty data structure
    multilayer_network = multinet.multi_layer_network().load_network(input_file=edgelist,directed=False,input_type="edgelist_spin")

    multilayer_network.save_network(encoded_network)
    
    ## print some stats
    multilayer_network.monitor(multilayer_network.basic_stats())
    
    multilayer_network.monitor("Creating embedding")    
    out_embedding = "tmp.emb" ## tmp placeholder
    ## call a specific embedding binary --- this is not limited to n2v
    train_node2vec_embedding.call_node2vec_binary(encoded_network,out_embedding,binary=n2v_binary,weighted=False,dimension=500)

    ## preprocess and check embedding
    multilayer_network.load_embedding("tmp.emb")

    ## output embedded coordinates as JSON
    multilayer_network.monitor("Creating the json file!")
    output_json = embedding_tools.get_2d_coordinates_tsne(multilayer_network,output_format="json")

    multilayer_network.monitor("Writing to file!")
    
    ## write to file
    with open(output_file, 'w') as outfile:
        outfile.write(output_json)
    
    multilayer_network.monitor("Finished with network embedding")
    os.remove(encoded_network)
    os.remove(out_embedding)

if __name__ == "__main__":
        
    parser = argparse.ArgumentParser()
    parser.add_argument("--edgelist_file",default=None)
    parser.add_argument("--output_json",default="./test.json")
    parser.add_argument("--n2v_binary",default="./node2vec")
    args = parser.parse_args()

    ## call to main
    compute_embedded_coordinates(args.edgelist_file,args.output_json,n2v_binary=args.n2v_binary)
