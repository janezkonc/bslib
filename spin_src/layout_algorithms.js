// compute node layout algorithms using custom approaches

var exec = require('child_process').exec;
var fs = require('fs');

module.exports = {
    default_force_node: function (graph,cb){

	var iterations = 4000;
    	var physicsSettings = {
	    springLength: 1200,
	    springCoeff: 0.0001,
	    gravity: -150,
	    theta: 0.45,
	    dragCoeff: 1.2,
	    timeStep: 150
	};

	// tukaj dodaj binarne!
	var layout = require('ngraph.forcelayout')(graph,physicsSettings);
	for (var i = 0; i < iterations; ++i) {
	    if (i % 30 == 0)
		console.log("Layout",(i*100/iterations)+"% complete.")
	    layout.step();
	}

	var final_data = [];
	console.log("Layout post-processing");
	var num_it = 10000;
	var displacement = 5;
	var distance_threshold = 10;
	
	// idea: go through neighbors of each node and adjust for overlaps.
	for (var j =1;j < num_it;j++){
	    if (num_it %10 == 0){
		console.log("Adaptation complete:",j*100/num_it,"%")
	    }
	    graph.forEachNode(function(node1){
		graph.forEachLinkedNode(node1.id, function(node2, link){

		    var pos1 = layout.getNodePosition(node1.id);
		    var pos2 = layout.getNodePosition(node2.id);
		    
		    //euclidean distance in the embedding plane
		    var distance = Math.sqrt(Math.pow(pos2.x-pos1.x, 2) + Math.pow(pos2.y-pos1.y, 2));
		    if (distance < distance_threshold){
			
			// randomly perturb the input space..
			if (rnum()){

			    // move the first node
			    if (pos1.x > pos2.x && pos1.y < pos2.y){
				layout.setNodePosition(node1.id, pos1.x+displacement, pos1.y-displacement);
			    }else if (pos1.x > pos2.x && pos1.y > pos2.y){
				layout.setNodePosition(node1.id, pos1.x+displacement, pos1.y+displacement);
			    }else if (pos1.x < pos2.x && pos1.y > pos2.y){
				layout.setNodePosition(node1.id, pos1.x-displacement, pos1.y+displacement);
			    }else if (pos1.x < pos2.x && pos1.y < pos2.y){
				layout.setNodePosition(node1.id, pos1.x-displacement, pos1.y-displacement);
			    }
			    
			}else{

			    if (pos1.x > pos2.x && pos1.y < pos2.y){
				layout.setNodePosition(node2.id, pos2.x-displacement, pos2.y+displacement);
			    }else if (pos1.x > pos2.x && pos1.y > pos2.y){
				layout.setNodePosition(node2.id, pos2.x-displacement, pos2.y-displacement);
			    }else if (pos1.x < pos2.x && pos1.y > pos2.y){
				layout.setNodePosition(node2.id, pos2.x+displacement, pos2.y-displacement);
			    }else if (pos1.x < pos2.x && pos1.y < pos2.y){
				layout.setNodePosition(node2.id, pos2.x+displacement, pos2.y+displacement);
			    }
		
			};
		    };
		});
	    });
	};
	
	console.log("Finished with layout processing..")

	var rect = layout.getGraphRect();
	var x_size = (Math.abs(rect.x1)+rect.x2)*1.02;
	var y_size = (Math.abs(rect.y1)+rect.y2)*1.02;
	cb({"rect":rect,"x_size":x_size,"y_size":y_size,"layout":layout});

    },
    
    external_binary_layout: function (graph, embedding_json, cb){

	// initialize via embeddings..

	var iterations;
    	var physicsSettings = {
	    springLength: 1000,
	    springCoeff: 0.0001,
	    gravity: -130,
	    theta: 0.25,
	    dragCoeff: 1.2,
	    timeStep: 150
	};

	var layout = require('ngraph.forcelayout')(graph,physicsSettings);
	var obj = JSON.parse(fs.readFileSync(embedding_json, 'utf8'));
	var iterations = 5000;

	var obj = JSON.parse(fs.readFileSync(embedding_json, 'utf8'));
	var unmathched = 0;
	for (var k in obj){
	    var value = obj[k];
	    try{
	    	layout.setNodePosition(value.node_names, value.dim1, value.dim2);
	    }catch(err){
		unmathched++;
	    }
	}

	console.log("unmatched nodes -- this will be random",unmathched);

	// layout.setNodePosition(node2.id, pos2.x+displacement, pos2.y+displacement);
	
	// minimize the embeddings..
	for (var i = 0; i < iterations; ++i) {
	    if (i % 10 == 0)
		console.log("Layout",(i*100/iterations)+"% complete.");
	    layout.step();
	};

	var rect = layout.getGraphRect();
	var x_size = (Math.abs(rect.x1)+rect.x2)*1.02;
	var y_size = (Math.abs(rect.y1)+rect.y2)*1.02;
	cb({"rect":rect,"x_size":x_size,"y_size":y_size,"layout":layout});

    }
}

function rnum() {
    return (Math.floor(Math.random() * 2) == 0) ? 1 : 0;
}

function os_func() {
    this.execCommand = function(cmd, callback) {
        exec(cmd, (error, stdout, stderr) => {
            if (error) {
                console.error(`exec error: ${error}`);
                return;
            }

            callback(stdout);
        });
    }
}
