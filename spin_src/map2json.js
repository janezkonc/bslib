// this node script converts a uniprot mapping to a json file.

var lineReader = require('readline').createInterface({
  input: require('fs').createReadStream(process.argv[2])
});

var tmp_dict = {}

lineReader.on('line', function (line) {
    var items = line.split("\t")
    var uni = items[0]
    var to = items[2]
    if (process.argv[3] == "--invert"){
	tmp_dict[to] = uni
    }else{
	tmp_dict[uni] = to
    }    
});

lineReader.on('close', function (line) {
    require('fs').writeFile(process.argv[4], JSON.stringify(tmp_dict), 'utf8', function (err) {
	if (err) {
            return console.log(err);
	}
	console.log("The file was saved!");
    }); 
});
