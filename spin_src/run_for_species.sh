
mkdir ../filtered_predictions;rm -rf ../filtered_predictions/*
mkdir ../results;rm -rf ../results/*

while read p; do
  echo $p;
  python3 prepare_datasets.py --query_network /servers/insilab.org/files/spin/whole_proteome_graph/reduced_proteome.edgelist --query_species $p --spin_database /servers/insilab.org/files/spin --description_folder /servers/insilab.org/files/spin/geneDescriptionJson >> ../results/$p"_predictions.txt"
done <representative_species_list.txt

#ls ../results/ | mawk '{print "python3 filter_edges.py --input_file","../results/"$1,"--output_file","../filtered_predictions/"$1}' | sh
