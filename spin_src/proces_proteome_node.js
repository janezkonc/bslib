// node variation of the graph processor

var fs = require('fs');
var zlib = require('zlib');
var createGraph = require('ngraph.graph');
const layout_module = require('./layout_algorithms');

var gene_index_file = process.argv[8]
var embedding_json = process.argv[9]

var taxonomy_mapping = (function(x){
    var taxMap = {}
    var mappings_lines = fs.readFileSync(x).toString().split("\n");
    mappings_lines.forEach((ex)=>{
	var pts = ex.split("\t")
	if(!taxMap.hasOwnProperty(pts[0]))
	    taxMap[pts[0]] = pts[1];
    });
    return taxMap

})(process.argv[7]);

function read_disease_associations(path,cb){
    console.log("reading the file..")
    var d = '';
    fs.createReadStream(path)
	.pipe(zlib.createGunzip())
	.on('data', function (data){
	    d += data.toString()
	})
	.on('end', function (){	    
	    cb(JSON.parse(d))
	})    
}

function onlyUnique(value, index, self) { 
    return self.indexOf(value) === index;
}

function prepare_network(disease_associations){

    //console.log(disease_json)
    if (process.argv[2].indexOf('.gz') != -1){
	var LR = require('readline').createInterface({
	    input: fs.createReadStream(process.argv[2]).on('error',()=>{	    
		console.log("Error occured for file:",process.argv[2])
	    }).pipe(zlib.createGunzip())
	});

    }else{
	var LR = require('readline').createInterface({
	    input: require('fs').createReadStream(process.argv[2])
	});           
    }

    LR.on('error', function (line) {    
	console.log("Error occured for file:",process.argv[2])    
    })

    var entries = {};
    var graph = createGraph();
    var iterations = 2000;
    var idcounter = -1;
    var processed_nodes = {};
    var tag_object = {};

    LR.on('line', function (line) {
	
	nodes = line.split(" ")
	if (!processed_nodes.hasOwnProperty(nodes[0])){
	    
	    var species_whole = nodes[0].split("_")[1];
	    try{
		var species_real = (taxonomy_mapping[species_whole]) ? (taxonomy_mapping[species_whole]).split(" ").join("_") : undefined;
		if (species_real != undefined){
		    var uniprot = nodes[0].split("_")[3];
		    var gname = nodes[0].split("_")[0];
		    var whole_entry = gname+"_"+species_real+"_"+uniprot;
		    //nodes[0] = whole_entry;
		    idcounter++;
		    processed_nodes[nodes[0]] = idcounter;
		    graph.addNode(nodes[0],processed_nodes[nodes[0]]);
		}else{
		    nodes[0] = undefined;
		}
		
	    }catch(err){
		console.log(species_whole)
	    }
	}

	if (!processed_nodes.hasOwnProperty(nodes[1])){
	    
	    try{
	    
		var species_whole2 = nodes[1].split("_")[1];	    
		var species_real = (taxonomy_mapping[species_whole2]) ? (taxonomy_mapping[species_whole2]).split(" ").join("_") : undefined;
		if(species_real != undefined){
		    var uniprot = nodes[1].split("_")[3];
		    var gname = nodes[1].split("_")[0];
		    var whole_entry = gname+"_"+species_real+"_"+uniprot;
		    //nodes[1] = whole_entry;
		    idcounter++;
		    processed_nodes[nodes[1]] = idcounter;
		    graph.addNode(nodes[1],processed_nodes[nodes[1]]);
		}else{
		    nodes[1] = undefined;
		}
	    }catch(err){
		console.log(species_whole2);
	    }
	}
	
	if (nodes[0] != nodes[1] && nodes[0] != undefined && nodes[1] != undefined){
	   
	    if (nodes[2] == "TAG_PRL" || nodes[2] == "TAG_PRL_EXP"){

		graph.addLink(nodes[0], nodes[1],{'type_of_interaction' : nodes[2],'zscore_confidence':nodes[3]||undefined});
		graph.addLink(nodes[1], nodes[0],{'type_of_interaction' : nodes[2],'zscore_confidence' : nodes[3]||undefined});

	    }else if (nodes[2] == "TAG_BIN"){

		graph.addLink(nodes[0], nodes[1],{'type_of_interaction' : nodes[2],'binary_confidence':nodes[3]||undefined});
		graph.addLink(nodes[1], nodes[0],{'type_of_interaction' : nodes[2],'binary_confidence' : nodes[3]||undefined});		
	    }else if (nodes[2] == "TAG_BLAST"){

		graph.addLink(nodes[0], nodes[1],{'type_of_interaction' : nodes[2],'blast_confidence':nodes[3]||undefined});
		graph.addLink(nodes[1], nodes[0],{'type_of_interaction' : nodes[2],'blast_confidence' : nodes[3]||undefined});

	    }
	    
	};
    });

    LR.on('close', function (line) {
		
	console.log("Calculating the graph layout..");

	let layout_object;
	layout_module.external_binary_layout(graph, embedding_json,(lo)=>{
	    layout_object = lo;
	});
	  
	var rect = layout_object.rect;
	var x_size = layout_object.x_size;
	var y_size = layout_object.y_size;
	var layout = layout_object.layout;
	var final_data = [];
	var disease_id_mapping = {};

	graph.forEachNode(function(node) {

	    // re-assign node.id to 
	    var nname = node.id
	    var species_whole = nname.split("_")[1];

	    try{
		var species_real = (taxonomy_mapping[species_whole]) ? (taxonomy_mapping[species_whole]).split(" ").join("_") : undefined;

		if (species_real != undefined){
		    var uniprot = nname.split("_")[3];
		    var gname = nname.split("_")[0];
		    var whole_entry = gname+"_"+species_real+"_"+uniprot;
		    node.id = whole_entry;
		}else{
		    nodes[0] = undefined;
		}
		
	    }catch(err){
		console.log("Couldn't map:",species_whole)
	    }
	    
	    try{ // not all nodes are here!
		var pos = layout.getNodePosition(nname);
		var new_x = (pos.x-rect.x1)/x_size;
		var new_y = (pos.y-rect.y1)/y_size;
		var gname = node.id.split("_")[0];
		var spec = node.id.split("_")[1];
	    }catch(err){
		console.log("Artificial node",node.id)
		var new_x = (0.8-rect.x1)/x_size;
		var new_y = (0.8-rect.y1)/y_size;
		var gname = node.id.split("_")[0];
		var spec = node.id.split("_")[1];
	    }
	    
		if (spec == 9606 || spec == "Homo"){
		    var associations = disease_associations[gname];
		}else{
		    var associations = undefined;
		}

		try{

		    for (var k in associations){
			if (!(disease_id_mapping.hasOwnProperty(k))){
			    disease_id_mapping[k] = []
			}				
			disease_id_mapping[k].push(node.data)
		    }
		    
		}catch(err){}

		fs.appendFileSync(gene_index_file, node.id+"\n");
		var nentry = {
		    group: "nodes",
		    data: {
			id: node.data,
			label: node.id,
			weight: 0.2,
			disease: associations
			
		    },
		    position: {
			x : new_x,
			y : new_y
		    }
		}
    		final_data.push(nentry);	   
	});
	
	zlib.gzip(JSON.stringify(disease_id_mapping), function (error, result) {
    	    if (error){
    		console.log(error)
    	    }else{
    		fs.writeFile(process.argv[6],result, function(err) {
    	    	    if(err) {
    	    		return console.log(err);
    	    	    }
    		});
    	    };
	});

	// classes -- binary tag
	var tag_object = {};
	
	graph.forEachLink(function(link) {
	    var int_type = link.data.type_of_interaction;
	    var id_edge = "e"+link.fromId+"_"+link.toId;
	    if (!tag_object.hasOwnProperty(id_edge)){
		tag_object[id_edge] = [];
	    }
	    
	    tag_object[id_edge].push(int_type);
	});

	var link_map = {};

	graph.forEachLink(function(link) {
	    var int_type = link.data.type_of_interaction;
	    var int_confidence = link.data.binary_confidence;
	    var int_prl_confidence = link.data.zscore_confidence;
	    var int_prl_blast_confidence = link.data.blast_confidence || undefined;
	    var id_edge = "e"+link.fromId+"_"+link.toId;
	    var id_edge_reverse = "e"+link.toId+"_"+link.fromId;
	    var classes = tag_object[id_edge];
	    var unique = classes.filter( onlyUnique );	    
	    
	    if (unique.indexOf("TAG_PRL_EXP") != -1){
		unique.push("TAG_PRL");
		unique.push("TAG_EXP");
		var index = unique.indexOf("TAG_PRL_EXP");
		if (index > -1) {
		    unique.splice(index, 1);
		};
	    };

	    if (unique.indexOf("TAG_BIN") != -1){
		unique.push("TAG_EXP");
	    };
	    

	    // construct classes
	    classes = unique.join(" ");

	    if (!link_map.hasOwnProperty(id_edge) && 
		!link_map.hasOwnProperty(id_edge_reverse)){
    		final_data.push({"group" : "edges",
    				 "data" : {"id" : id_edge,
    			     		   "source" : processed_nodes[link.fromId],
    			     		   "target" : processed_nodes[link.toId],
					   "binary_confidence":int_confidence,
					   "zscore_confidence":int_prl_confidence,
					   "blast_confidence":int_prl_blast_confidence},
					   "classes": classes});

		link_map[id_edge] = 1;
		link_map[id_edge_reverse] = 1;
		
		};
	});	
	

	zlib.gzip(JSON.stringify(final_data), function (error, result) {
    	    if (error){
    		console.log(error)
    	    }else{
    		fs.writeFile(process.argv[3],result, function(err) {
    	    	    if(err) {
    	    		return console.log(err);
    	    	    }
    		});
    	    };
	});

	// split according to a grid and write to separate files..
	var gridJson = {nodes : [], edges: []};
	var nodePositions = {};
	
	var partition = 50;
	var subpartition = 1/partition;
	
	final_data.forEach((entry)=>{
            if (entry.group == "nodes"){
		var position = entry.position;
		nodePositions[entry.data.id] = position;

		var part_x = Math.floor(position.x / subpartition);
		var part_y = Math.floor(position.y / subpartition);
		
		entry.data.partitionx = part_x;
		entry.data.partitiony = part_y;

		// if (!(gridJson.nodes.hasOwnProperty(part_x))) {
                //     gridJson.nodes[part_x] = {};
		// }

		// if (!(gridJson.nodes[part_x].hasOwnProperty(part_y))) {
                //     gridJson.nodes[part_x][part_y] = [];
		// }

		gridJson.nodes.push(entry)
            }
            else if (entry.group == "edges") {
		var source_position = nodePositions[entry.data.source];
		var target_position = nodePositions[entry.data.target];
		
		var source_part_x = Math.floor(source_position.x / subpartition);
		var source_part_y = Math.floor(source_position.y / subpartition);
		
		var target_part_x = Math.floor(target_position.x / subpartition);
		var target_part_y = Math.floor(target_position.y / subpartition);
		
		if (target_part_x < source_part_x || (target_part_x == source_part_x && target_part_y < source_part_y)) {
                    var fromName = target_part_x+"_"+target_part_y;
                    var toName = source_part_x+"_"+source_part_y;
		}
		else {
                    var fromName = source_part_x+"_"+source_part_y;
                    var toName = target_part_x+"_"+target_part_y;
		}
		
		// if (!(gridJson.edges.hasOwnProperty(fromName))) {
                //     gridJson.edges[fromName] = {};
		// }
		
		// if (!(gridJson.edges[fromName].hasOwnProperty(toName))) {
                //     gridJson.edges[fromName][toName] = [];
		// }
		
		gridJson.edges.push(entry);
            }
            
	});
	console.log("writing files")
	zlib.gzip(JSON.stringify(gridJson), function (error, result) {
    	    if (error){
    		console.log(error)
    	    }else{
    		fs.writeFile(process.argv[4],result, function(err) {
    	    	    if(err) {
    	    		return console.log(err);
    	    	    };
    		});
    	    };
	});	
    });
};


read_disease_associations(process.argv[5],(associations)=>{
    prepare_network(associations);
});
