## count the PL database instances
import json
import gzip
import glob
from collections import defaultdict

if __name__ == "__main__":
    
    ligand_store = defaultdict(set)
    ligands_path = "/servers/insilab.org/files/GenProBiS/probisdb/ligands/*"
    processed_files = 0
    for ligands_file in glob.glob(ligands_path):
        processed_files+=1
        if processed_files % 1000 == 0:
            print("Processed {}% files".format(processed_files*100/305000))
            for k,v in ligand_store.items():
                print(k,len(v))
        with gzip.open(ligands_file) as f:
            try:
                net = json.load(f)
                for entry in net:
                    ltype = entry['lig_id'].split("_")[0]
                    ligand_store[ltype].add(entry['lig_id'])
            except:
                pass
            
    for k,v in ligand_store.items():
        print(k,len(v))
