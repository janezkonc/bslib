## This script loads edgefile and filters according to desired criterion

import pandas as pd
import argparse

def parse_input_file(fname):
    header_names = ["edge_type","node_first","node_second","x1","uniprot_first","escore_first","coverage_v11","coverage_v12","seq_identity_first","x2","uniprot_second","escore_second","coverage_v21","coverage_v22","seq_identity_second","homolog_node_first","homolog_node_second"]
    df = pd.read_csv(fname, sep="\t", names=header_names)
    identities_first = [float(x.replace("'","")) for x in df['seq_identity_first'].tolist()]
    identities_second = [float(x.replace("'","")) for x  in df['seq_identity_second'].tolist()]
    df['seq_identity_first'] = identities_first
    df['seq_identity_second'] = identities_second
    return df


def filter_by_id_e(dataset,id_threshold,e_threshold):
    dataset = dataset[dataset['escore_first'] < e_threshold]
    dataset = dataset[dataset['escore_second'] < e_threshold]
    dataset = dataset[dataset['seq_identity_first'] > id_threshold]
    dataset = dataset[dataset['seq_identity_second'] > id_threshold]

    return dataset

if __name__ == "__main__":
    
    parser = argparse.ArgumentParser()
    parser.add_argument("--identity_filter",default=50)
    parser.add_argument("--escore_filter",default=0.000001)
    parser.add_argument("--input_file",default=None)
    parser.add_argument("--output_file",default="./text.tsv")
    args = parser.parse_args()
    loaded_file = parse_input_file(args.input_file)
    filtered_file = filter_by_id_e(loaded_file,args.identity_filter,args.escore_filter)
    filtered_file.to_csv(args.output_file,sep="\t")
