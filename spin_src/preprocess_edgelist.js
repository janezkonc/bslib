// Set of functions to preprocess the graph edgelist
// Here, gene and species are used to have mapped uniprot entries. Only a single uniprot entry must me kept.
// Which entry? How do we choose it?

// open and save SIFTS mappings

var fs = require('fs');
var zlib = require('zlib');

function filter_network(infile,mappings,cb){

    if (infile.indexOf('.gz') != -1){
	var LR2 = require('readline').createInterface({
	    input: fs.createReadStream(infile).on('error',()=>{    
		console.log("Error occured for file:",infile)
		    }).pipe(zlib.createGunzip())
	    });

    }else{
	var LR2 = require('readline').createInterface({
	        input: require('fs').createReadStream(infile)
	    });           
    }

    LR.on('error', function (line) {    
	console.log("Error occured for file:",infile)
    })

    var result_graph = []
    LR2.on('line', function (line) {
	var parts = line.split("\t");
	if (parts.length >= 3){
	        if (parts[2].indexOf("TAG_") != -1){

		    // get the uniprot identifiers
		    var l1 =  parts[0].split("_");
		    var l2 =  parts[1].split("_");

		    if (l1.length == 4 && l2.length == 4){
			    
			    var uniFirst = parts[0].split("_").splice(-1)[0];
			    var uniSecond = parts[1].split("_").splice(-1)[0];

			    // get the species identifiers
			    var geneSpeciesFirst = parts[0].split("_").splice(0,3).join("_").replace(")","").replace(".","");
			    var geneSpeciesSecond = parts[1].split("_").splice(0,3).join("_").replace(")","").replace(".","");

			    if (uniFirst == mappings[geneSpeciesFirst] && uniSecond == mappings[geneSpeciesSecond])
				result_graph.push([geneSpeciesFirst+"_"+uniFirst,geneSpeciesSecond+"_"+uniSecond,parts[2]]);
			}
		        }
	    }
    });

    LR2.on('close', function (line) {
	//console.log("Finished graph prunning.")
	cb(result_graph)
    });    
}

if (process.argv[2].indexOf('.gz') != -1){
    var LR = require('readline').createInterface({
	input: fs.createReadStream(process.argv[2]).on('error',()=>{    
	        console.log("Error occured for file:",process.argv[2])
	    }).pipe(zlib.createGunzip())
    });

}else{
    var LR = require('readline').createInterface({
	input: require('fs').createReadStream(process.argv[2])
    });           
}

LR.on('error', function (line) {    
    console.log("Error occured for file:",process.argv[2]) 
})

var entries = {};
LR.on('line', function (line) {    
    var parts = line.split("\t");
    if (parts.length >= 2){

	// get the uniprot identifiers
	var uniFirst= parts[0].split("_").splice(-1)[0];
	var uniSecond= parts[1].split("_").splice(-1)[0];
	
	// get the species identifiers
	var geneSpeciesFirst = parts[0].split("_").splice(0,3).join("_");
	var geneSpeciesSecond = parts[1].split("_").splice(0,3).join("_");

	if (!entries.hasOwnProperty(geneSpeciesFirst))
	        entries[geneSpeciesFirst] = [];
	
	entries[geneSpeciesFirst].push(uniFirst);
	
	if (!entries.hasOwnProperty(geneSpeciesSecond))
	    entries[geneSpeciesSecond] = [];
	
	entries[geneSpeciesSecond].push(uniSecond);
    }
});

LR.on('close', function (line) {
    
    for (var k in entries){
	var set = new Set(entries[k]);
	var it = set.values();
	entries[k] = it.next().value
    }
    
    var json = JSON.stringify(entries);
    fs.writeFile(process.argv[3], json, 'utf8');
    
    // filter_network(process.argv[2],entries,(result_graph)=>{
    // 	result_graph.forEach((en)=>{
    // 	        var edge = en[0]+" "+en[1]+" "+en[2]
    // 	        console.log(edge)
    // 	    });
    // });
});
