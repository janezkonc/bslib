### this is a bash script for distributed graph generation..
## this bash script distributes the jobs to slave computers for further use..
TSVBASE="../genprobisdb/tsv/"

if [ "$1" == "--copyFiles" ]

then
    
    cat slaves | mawk '{print "rsync -ar ~/NProBiS ",$2":~"}' | parallel

elif [ "$1" == "--single" ]
then
    ls $TSVBASE | mawk '{print "cd ~/NProBiS/data/algorithms;node emit_edges.js","../genprobisdb/tsv/"$1,"--gene_gene"}' | parallel --progress --jobs 4  > ../genegraph/gene2gene.txt
    

elif [ "$1" == "--runClusterJob" ]

then
    rm -rvf ../genegraph
    mkdir ../genegraph
    
    ls $TSVBASE | mawk '{print "cd ~/NProBiS/data/algorithms; /home/blazs/.nvm/versions/node/v7.9.0/bin/./node emit_edges.js","../genprobisdb/tsv/"$1,"--gene_gene"}' | parallel --progress --load=60% --sshloginfile slaves > ../genegraph/gene2gene.txt

elif [ "$1" == "--installNode" ]

then

    wget -qO- https://raw.githubusercontent.com/creationix/nvm/v0.33.1/install.sh | bash

    export NVM_DIR="$HOME/.nvm"
    [ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh" # This loads nvm


    nvm install node
    nvm use 7.9

elif [ "$1" == "--distributeNode" ]

then

    cat slaves | mawk '{print "ssh",$2,"\" cd NetProBiS/data/parsers_second;bash distribute_jobs.sh --installNode  \""}' | parallel

elif [ "$1" == "--options"  ]

then

    echo "--distributeNode, --copyFiles, --runClusterJob"

elif [ "$1" == "--example"  ]

then

    echo "--distributeNode,--copyFiles,--runClusterJob"
        
fi

