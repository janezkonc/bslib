// this function removes all structures form individual ligands..

var fs = require('fs');
var zlib = require('zlib');
var outFolder = process.argv[3]

if (process.argv[2].indexOf('.gz') != -1){
    var LR = require('readline').createInterface({
	input: fs.createReadStream(process.argv[2]).on('error',()=>{	    
	    console.log("Error occured for file:",process.argv[2])
	}).pipe(zlib.createGunzip())
    });

}else{
    var LR = require('readline').createInterface({
	input: require('fs').createReadStream(process.argv[2])
    });           
}


LR.on('error', function (line) {    

    console.log("Error occured for file:",process.argv[2])

})

var buffer = ""
LR.on('line', function (line) {
    buffer += line;    
})

LR.on('close', function (line) {
    
    var buff = JSON.parse(buffer);
    var processed = ((raw_buffer)=>{
	raw_buffer.forEach((ent)=>{
	    delete ent.pdb_file;
	    delete ent.trans;
	    if (ent.ltype == "water" && ent.cons < 0.86)
		delete ent
	})
	return raw_buffer;
    })(buff)

    zlib.gzip(JSON.stringify(processed), function (error, result) {
	if (error){
	    console.log(error)
	}else{
	    var outfile = outFolder+"/"+process.argv[2].split("/").splice(-1)
	    fs.writeFile(outfile, result, function(err) {
		if(err) {
		    return console.log(err,"random");
		}
	    });
	}
    })
});


